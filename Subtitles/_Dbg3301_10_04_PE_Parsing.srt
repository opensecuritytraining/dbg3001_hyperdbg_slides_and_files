1
00:00:00,000 --> 00:00:05,580
let's see other commands and other

2
00:00:03,000 --> 00:00:08,700
commands for hybrid which is it also

3
00:00:05,580 --> 00:00:11,160
makes you able to parse P files or

4
00:00:08,700 --> 00:00:14,400
portable executable files

5
00:00:11,160 --> 00:00:18,539
if you used that PE command then and you

6
00:00:14,400 --> 00:00:21,480
will you can dump a different headers

7
00:00:18,539 --> 00:00:24,660
for the PE files

8
00:00:21,480 --> 00:00:27,060
it's also possible to get the details of

9
00:00:24,660 --> 00:00:29,300
different sections for example you could

10
00:00:27,060 --> 00:00:32,940
use dot piece

11
00:00:29,300 --> 00:00:35,640
section and then you can specify the

12
00:00:32,940 --> 00:00:38,760
sync the section that you want to 

13
00:00:35,640 --> 00:00:42,300
that you want to see it's the headers

14
00:00:38,760 --> 00:00:46,079
for example in this example I see that

15
00:00:42,300 --> 00:00:48,620
that text section of this executable

16
00:00:46,079 --> 00:00:48,620
file

