1
00:00:00,000 --> 00:00:06,359
hi everyone welcome to the next part of

2
00:00:02,879 --> 00:00:08,099
the tutorial reversing with HyperDbg in

3
00:00:06,359 --> 00:00:10,080
the previous part we talked about third

4
00:00:08,099 --> 00:00:12,599
specific and processing specific

5
00:00:10,080 --> 00:00:16,080
debugging in this tutorial we're gonna

6
00:00:12,599 --> 00:00:20,400
see how we can use the script engine as

7
00:00:16,080 --> 00:00:22,980
this debugger just a quick outline

8
00:00:20,400 --> 00:00:26,100
of what we're going to talk in this part

9
00:00:22,980 --> 00:00:30,240
we're gonna talk about basic principles

10
00:00:26,100 --> 00:00:33,719
of the script engine and different

11
00:00:30,240 --> 00:00:36,840
diagrams also we're gonna see why

12
00:00:33,719 --> 00:00:37,860
it's so fast compared to the wind

13
00:00:36,840 --> 00:00:41,280
energy

14
00:00:37,860 --> 00:00:44,280
and then we're gonna go to some period

15
00:00:41,280 --> 00:00:47,840
 preliminaries and like keywords

16
00:00:44,280 --> 00:00:52,320
operators and how we can access or

17
00:00:47,840 --> 00:00:54,480
modify registers and our flag you're

18
00:00:52,320 --> 00:00:58,140
gonna see two registers and number

19
00:00:54,480 --> 00:01:02,340
prefixes in the script engine and we

20
00:00:58,140 --> 00:01:04,379
will also see how we can use comments in

21
00:01:02,340 --> 00:01:07,320
a script engine another important thing

22
00:01:04,379 --> 00:01:09,600
is different printing commands that

23
00:01:07,320 --> 00:01:12,720
are implemented which is used for

24
00:01:09,600 --> 00:01:16,500
lagging creating logs from the kernel

25
00:01:12,720 --> 00:01:19,740
and also we have evaluating and commands

26
00:01:16,500 --> 00:01:23,340
arguments variables and assignments and

27
00:01:19,740 --> 00:01:27,180
conditional statements loops and these

28
00:01:23,340 --> 00:01:29,700
are the keywords or the basic statements

29
00:01:27,180 --> 00:01:32,340
that are used in different programming

30
00:01:29,700 --> 00:01:34,979
languages and hybrid which is not an

31
00:01:32,340 --> 00:01:37,380
exception I also talk about how we can

32
00:01:34,979 --> 00:01:38,240
share the resources between different

33
00:01:37,380 --> 00:01:42,840
core

34
00:01:38,240 --> 00:01:45,240
and also so we will look at some of the

35
00:01:42,840 --> 00:01:48,360
script engine functions like halting

36
00:01:45,240 --> 00:01:51,960
functions event functions interlocked

37
00:01:48,360 --> 00:01:55,560
and memory functions at last we will

38
00:01:51,960 --> 00:01:58,680
have a summary okay let's see how how

39
00:01:55,560 --> 00:02:02,220
the script engine is designed well it's

40
00:01:58,680 --> 00:02:04,500
the by itself it's really challenging to

41
00:02:02,220 --> 00:02:07,700
create an S script engine that that is

42
00:02:04,500 --> 00:02:11,459
able to run in the kernel mode

43
00:02:07,700 --> 00:02:15,900
whenever it comes to the VM extrude mode

44
00:02:11,459 --> 00:02:19,739
then it's a more complicated and so most

45
00:02:15,900 --> 00:02:23,099
of the modules are and most of the

46
00:02:19,739 --> 00:02:25,800
commands or extension commands of hyper

47
00:02:23,099 --> 00:02:29,840
DG are already running in remix root

48
00:02:25,800 --> 00:02:33,420
mode so the script engine should be

49
00:02:29,840 --> 00:02:36,360
should support me in full mode and the

50
00:02:33,420 --> 00:02:39,420
thing is that there there are some 

51
00:02:36,360 --> 00:02:41,340
principles that hybrid images script

52
00:02:39,420 --> 00:02:42,739
engine is designed based on these

53
00:02:41,340 --> 00:02:45,239
principles

54
00:02:42,739 --> 00:02:48,780
 the first thing is that everything

55
00:02:45,239 --> 00:02:51,420
should be available in both user mode

56
00:02:48,780 --> 00:02:54,060
and kernel mode and of course we mix

57
00:02:51,420 --> 00:02:56,400
root mode because we're gonna use a

58
00:02:54,060 --> 00:02:59,220
script engine in user mode and kernel

59
00:02:56,400 --> 00:03:02,459
mode 2 we have some commands that are

60
00:02:59,220 --> 00:03:04,379
running in kernel mode but not in DMX

61
00:03:02,459 --> 00:03:07,260
root mode we will talk about them later

62
00:03:04,379 --> 00:03:10,440
and the thing is that the execution

63
00:03:07,260 --> 00:03:12,659
version of the S group in each core is

64
00:03:10,440 --> 00:03:15,120
independent from other course and this

65
00:03:12,659 --> 00:03:17,519
is a really important consideration that

66
00:03:15,120 --> 00:03:19,319
you should have when you're using hyper

67
00:03:17,519 --> 00:03:20,959
division script engine

68
00:03:19,319 --> 00:03:24,800
so

69
00:03:20,959 --> 00:03:27,000
escapes are able to run on every core

70
00:03:24,800 --> 00:03:29,040
simultaneously that's why we have

71
00:03:27,000 --> 00:03:32,599
something like resource clearing because

72
00:03:29,040 --> 00:03:35,819
each core is running independently and

73
00:03:32,599 --> 00:03:38,519
another challenging part of the script

74
00:03:35,819 --> 00:03:40,680
engine is that every memory access

75
00:03:38,519 --> 00:03:42,659
should go through it through the safe

76
00:03:40,680 --> 00:03:45,060
memory access routines to avoid

77
00:03:42,659 --> 00:03:47,879
unhandled behavior especially in remix

78
00:03:45,060 --> 00:03:51,420
Footwear the problem here is that and

79
00:03:47,879 --> 00:03:54,000
you cannot even though in terms of Intel

80
00:03:51,420 --> 00:03:57,900
 terminologies for the ring even

81
00:03:54,000 --> 00:04:00,900
though the hypervisor or the VM extrude

82
00:03:57,900 --> 00:04:03,060
mode is more privileged than the user

83
00:04:00,900 --> 00:04:06,239
and whatever you're not able to easily

84
00:04:03,060 --> 00:04:07,159
access a user mode from the VMS root

85
00:04:06,239 --> 00:04:12,780
mode

86
00:04:07,159 --> 00:04:15,360
so we're gonna map the memory addresses

87
00:04:12,780 --> 00:04:18,479
of the user mode to the kernel mode and

88
00:04:15,360 --> 00:04:20,699
then we try to access those memory from

89
00:04:18,479 --> 00:04:22,560
directly from the kernel but not in the

90
00:04:20,699 --> 00:04:25,860
user mode we will talk about them later

91
00:04:22,560 --> 00:04:28,560
it's it's a little bit tough using the

92
00:04:25,860 --> 00:04:31,320
fourth principle is that HyperDbg

93
00:04:28,560 --> 00:04:33,600
is a script engine is a CSI language

94
00:04:31,320 --> 00:04:37,500
which is combined with some masks

95
00:04:33,600 --> 00:04:40,500
syntaxes and it's a little bit like

96
00:04:37,500 --> 00:04:43,380
WinDbg you are already familiar with

97
00:04:40,500 --> 00:04:45,660
 how how to create a script in windy

98
00:04:43,380 --> 00:04:48,060
machine you probably it won't be hard

99
00:04:45,660 --> 00:04:50,940
for you to learn the script engine of

100
00:04:48,060 --> 00:04:55,560
the HyperDbg and the another thing is

101
00:04:50,940 --> 00:04:58,139
that the output for the script engine

102
00:04:55,560 --> 00:05:00,660
should be transferred to the user mode

103
00:04:58,139 --> 00:05:03,479
through a custom data transferring

104
00:05:00,660 --> 00:05:06,900
functions there are a lot of ways that

105
00:05:03,479 --> 00:05:09,600
hybrid imagery tries to send the

106
00:05:06,900 --> 00:05:11,639
messages to the user mode it's not

107
00:05:09,600 --> 00:05:15,120
really easily possible by simple

108
00:05:11,639 --> 00:05:17,400
routines because actually it's a little

109
00:05:15,120 --> 00:05:20,220
bit complicated to handle data in remix

110
00:05:17,400 --> 00:05:23,759
root mode but you should use the hybrid

111
00:05:20,220 --> 00:05:27,419
images routine which safely transfer the

112
00:05:23,759 --> 00:05:29,400
buffer or the messages from the from

113
00:05:27,419 --> 00:05:33,780
the vmix root mode and the kernel mode

114
00:05:29,400 --> 00:05:37,259
to the user mode so that the user can

115
00:05:33,780 --> 00:05:40,199
see the results and decide about those

116
00:05:37,259 --> 00:05:43,860
results based on these principles it's

117
00:05:40,199 --> 00:05:47,400
guaranteed to have a standalone and a

118
00:05:43,860 --> 00:05:49,919
fast script engine so why this is

119
00:05:47,400 --> 00:05:52,680
important because I've been fast brings

120
00:05:49,919 --> 00:05:56,639
us with thousands of many functions

121
00:05:52,680 --> 00:06:00,060
which many options that helps us on

122
00:05:56,639 --> 00:06:02,639
debugging the operating system and can

123
00:06:00,060 --> 00:06:05,160
solve a lot of challenges because just

124
00:06:02,639 --> 00:06:08,419
because the script engine is fast for

125
00:06:05,160 --> 00:06:11,820
example imagine that you can that

126
00:06:08,419 --> 00:06:14,820
hundreds of conditioner breakpoints 

127
00:06:11,820 --> 00:06:16,680
or I mean tens of on tens of functions

128
00:06:14,820 --> 00:06:19,320
all of them with a high rate of

129
00:06:16,680 --> 00:06:21,960
execution and the systems are still

130
00:06:19,320 --> 00:06:24,240
working normally so this is a really

131
00:06:21,960 --> 00:06:27,419
good you you have

132
00:06:24,240 --> 00:06:29,520
an excellent chance of debugging your

133
00:06:27,419 --> 00:06:32,460
target this is simply not true about

134
00:06:29,520 --> 00:06:35,520
green Luigi for example if you put just

135
00:06:32,460 --> 00:06:37,740
one breakpoint on a function with a high

136
00:06:35,520 --> 00:06:40,340
rate of execution then you definitely

137
00:06:37,740 --> 00:06:43,800
end up with the system being

138
00:06:40,340 --> 00:06:46,319
unresponsive so this is why it's

139
00:06:43,800 --> 00:06:48,840
important to have a fast script engine

140
00:06:46,319 --> 00:06:52,039
okay let's see how it works internally

141
00:06:48,840 --> 00:06:56,160
actually I produce is a script engine

142
00:06:52,039 --> 00:06:59,639
are running into different modes the

143
00:06:56,160 --> 00:07:02,520
first thing is that if you just pause

144
00:06:59,639 --> 00:07:04,860
the debugger and everything is passed

145
00:07:02,520 --> 00:07:08,220
and you're running an S script on just

146
00:07:04,860 --> 00:07:11,880
one core just one single core so we're

147
00:07:08,220 --> 00:07:12,860
gonna use an S grips like let me show it

148
00:07:11,880 --> 00:07:16,919
here

149
00:07:12,860 --> 00:07:19,560
you print use the printf function and

150
00:07:16,919 --> 00:07:22,560
 simply showing the message of just

151
00:07:19,560 --> 00:07:25,380
creating a lot from the one of the

152
00:07:22,560 --> 00:07:28,979
register like in this exam so it's RX

153
00:07:25,380 --> 00:07:31,979
and then it goes through the Elixir the

154
00:07:28,979 --> 00:07:35,639
parser and then converts that in

155
00:07:31,979 --> 00:07:38,759
intermediate format so we'll send this

156
00:07:35,639 --> 00:07:42,660
IR format code buffer over the serial

157
00:07:38,759 --> 00:07:45,780
over the TCP to the target debugging and

158
00:07:42,660 --> 00:07:49,380
the target debugger tries to evaluate

159
00:07:45,780 --> 00:07:54,000
and run the script and then send the

160
00:07:49,380 --> 00:07:57,960
results in a safe way back to the

161
00:07:54,000 --> 00:08:00,840
user mode debugger over the serial and

162
00:07:57,960 --> 00:08:03,419
the husk if you are running a HyperDbg

163
00:08:00,840 --> 00:08:06,000
on a virtual machine and I understand

164
00:08:03,419 --> 00:08:09,120
this is the simple scenario where we are

165
00:08:06,000 --> 00:08:11,940
simply just pausing everything

166
00:08:09,120 --> 00:08:15,660
 we'll have some examples to clarify

167
00:08:11,940 --> 00:08:19,020
what I mean later probably next parts

168
00:08:15,660 --> 00:08:21,900
of these tutorials but for now just just

169
00:08:19,020 --> 00:08:25,440
consider that we have two options for

170
00:08:21,900 --> 00:08:29,340
adding a script the second option here

171
00:08:25,440 --> 00:08:32,459
is that whenever an event is executed

172
00:08:29,340 --> 00:08:36,240
and in HyperDbg we have a chance to

173
00:08:32,459 --> 00:08:39,599
execute our scripts as you can see from

174
00:08:36,240 --> 00:08:42,360
the previous parts as you saw in the

175
00:08:39,599 --> 00:08:45,000
previous parts there is a keyword script

176
00:08:42,360 --> 00:08:48,540
on each event which means that whenever

177
00:08:45,000 --> 00:08:50,480
the event triggered for example a syscall

178
00:08:48,540 --> 00:08:55,740
is triggered then

179
00:08:50,480 --> 00:08:58,640
your action will start executing the

180
00:08:55,740 --> 00:09:01,860
action actually can be a break which

181
00:08:58,640 --> 00:09:04,620
eventually just breaks and notifies the

182
00:09:01,860 --> 00:09:06,480
debugger that something has happened in

183
00:09:04,620 --> 00:09:09,420
other words on debugger mode and another

184
00:09:06,480 --> 00:09:11,720
thing is that you might run the car last

185
00:09:09,420 --> 00:09:15,180
long code which is a simple assembly

186
00:09:11,720 --> 00:09:17,880
function or assembly codes and an

187
00:09:15,180 --> 00:09:21,779
important function or the most important

188
00:09:17,880 --> 00:09:28,380
function is the script engine which it

189
00:09:21,779 --> 00:09:30,899
lets you to run the the scripts you can

190
00:09:28,380 --> 00:09:33,180
then send the results back to the

191
00:09:30,899 --> 00:09:36,300
debugger mode or you can just simply

192
00:09:33,180 --> 00:09:40,080
continue without any without housing

193
00:09:36,300 --> 00:09:42,540
assistance so the thing is I explained

194
00:09:40,080 --> 00:09:45,899
it in the previous slide but let's just

195
00:09:42,540 --> 00:09:49,200
have a fast review of why that script

196
00:09:45,899 --> 00:09:51,600
engine is so fast compared to WinDbg

197
00:09:49,200 --> 00:09:55,019
and the thing is that it's fully kernel

198
00:09:51,600 --> 00:09:57,480
slide implemented so everything is done

199
00:09:55,019 --> 00:09:59,940
in post kernel mode and remix root mode

200
00:09:57,480 --> 00:10:02,580
and there is no user mode interaction

201
00:09:59,940 --> 00:10:04,800
which means that whenever your script

202
00:10:02,580 --> 00:10:06,839
get a chance to get executed in the

203
00:10:04,800 --> 00:10:10,260
kernel mode then everything is executed

204
00:10:06,839 --> 00:10:12,360
in a kernel this is different from the

205
00:10:10,260 --> 00:10:15,360
way that WinDbg implements the

206
00:10:12,360 --> 00:10:17,640
script engine in the vdbg everything is

207
00:10:15,360 --> 00:10:19,860
passed the debugger which might be a

208
00:10:17,640 --> 00:10:22,140
user mode debugger and you might pass

209
00:10:19,860 --> 00:10:24,839
everything for example through a Serial

210
00:10:22,140 --> 00:10:29,100
port and the escape these are running

211
00:10:24,839 --> 00:10:33,000
completely on the user mode part of the

212
00:10:29,100 --> 00:10:37,560
house if you're dividing a VM and after

213
00:10:33,000 --> 00:10:40,380
that you will ask or query for for

214
00:10:37,560 --> 00:10:42,180
the things that that that are needed by

215
00:10:40,380 --> 00:10:46,200
the script to be executed for example

216
00:10:42,180 --> 00:10:48,959
the script might ask for a register or

217
00:10:46,200 --> 00:10:52,560
might ask for a memory address then

218
00:10:48,959 --> 00:10:55,860
there's an interaction here between the

219
00:10:52,560 --> 00:10:58,920
US and the guests or the debugging and

220
00:10:55,860 --> 00:11:02,240
the debugger and after that it tries to

221
00:10:58,920 --> 00:11:04,860
execute the scripts in the user mode

222
00:11:02,240 --> 00:11:08,760
part of the debugger

223
00:11:04,860 --> 00:11:11,220
so this interaction is just to be

224
00:11:08,760 --> 00:11:13,980
completely removed this interaction and

225
00:11:11,220 --> 00:11:17,519
everything is running on the kernel's

226
00:11:13,980 --> 00:11:21,000
line another thing is that there is no

227
00:11:17,519 --> 00:11:23,279
house while executing a script which

228
00:11:21,000 --> 00:11:25,680
means that as we don't have any

229
00:11:23,279 --> 00:11:28,680
interaction with the debugger we can

230
00:11:25,680 --> 00:11:30,959
just simply continue the execution you

231
00:11:28,680 --> 00:11:34,740
might need to just check some of the

232
00:11:30,959 --> 00:11:38,160
parameters and the orgs create some logs

233
00:11:34,740 --> 00:11:41,459
there's no need to have alt the entire

234
00:11:38,160 --> 00:11:44,279
system and easily continue without any

235
00:11:41,459 --> 00:11:47,700
extra interaction with the debugger

236
00:11:44,279 --> 00:11:50,640
another thing is as I mentioned before

237
00:11:47,700 --> 00:11:53,579
each core executes the scripts

238
00:11:50,640 --> 00:11:56,880
independently and it doesn't matter 

239
00:11:53,579 --> 00:12:00,200
if the two cores are running escaped or

240
00:11:56,880 --> 00:12:03,779
maybe more for example eight cores are

241
00:12:00,200 --> 00:12:06,300
simultaneously executing a simple s

242
00:12:03,779 --> 00:12:08,160
group and there is a nothing because

243
00:12:06,300 --> 00:12:11,519
they are independent they're nothing

244
00:12:08,160 --> 00:12:14,839
between them so you can easily execute

245
00:12:11,519 --> 00:12:18,300
scripts in all course simultaneously

246
00:12:14,839 --> 00:12:21,600
also there is an efficient buffering

247
00:12:18,300 --> 00:12:23,100
method used in the script engine you

248
00:12:21,600 --> 00:12:24,500
might take a look at the source code

249
00:12:23,100 --> 00:12:27,660
actually

250
00:12:24,500 --> 00:12:30,740
it just removes every just it's just a

251
00:12:27,660 --> 00:12:34,860
simple buffering method that tries to

252
00:12:30,740 --> 00:12:38,040
accumulate the messages around the

253
00:12:34,860 --> 00:12:41,640
debugger and then send them to the user

254
00:12:38,040 --> 00:12:44,279
mode or might send them immediately to

255
00:12:41,640 --> 00:12:46,019
the debugger these are these are the

256
00:12:44,279 --> 00:12:48,000
things that are implemented in the

257
00:12:46,019 --> 00:12:49,920
script engine after hyper Division and

258
00:12:48,000 --> 00:12:53,160
you simply use it for example when you

259
00:12:49,920 --> 00:12:56,880
use a function simple function like a

260
00:12:53,160 --> 00:12:58,860
print s then you definitely use this

261
00:12:56,880 --> 00:13:02,100
efficient buffering

262
00:12:58,860 --> 00:13:05,100
okay now the next question is how much

263
00:13:02,100 --> 00:13:08,579
is faster generally based on the

264
00:13:05,100 --> 00:13:12,320
results that we published in in an

265
00:13:08,579 --> 00:13:19,519
academic paper of individually

266
00:13:12,320 --> 00:13:19,519
checked on average like 6

267
00:13:20,000 --> 00:13:28,740
941 conditions and meanwhile HyperDbg

268
00:13:24,300 --> 00:13:33,120
checked over 9 million conditions in in

269
00:13:28,740 --> 00:13:36,180
five minutes so on average it's it's

270
00:13:33,120 --> 00:13:38,300
substantially faster than WinDbg it's

271
00:13:36,180 --> 00:13:42,959
about

272
00:13:38,300 --> 00:13:47,639
99.9 percent faster than the VIN dbg one

273
00:13:42,959 --> 00:13:51,779
is added here so it's just a simple 

274
00:13:47,639 --> 00:13:54,860
compression comparison between the

275
00:13:51,779 --> 00:14:00,360
HyperDbg of individges almost

276
00:13:54,860 --> 00:14:02,639
300 times faster in EPT hook two this

277
00:14:00,360 --> 00:14:07,019
will be discussed later and it's also

278
00:14:02,639 --> 00:14:08,880
it's 3 000 times faster and it's 1 000

279
00:14:07,019 --> 00:14:12,420
times more than one thousand times

280
00:14:08,880 --> 00:14:15,660
faster in EPT hook and if we want to

281
00:14:12,420 --> 00:14:17,519
just simply hook system calls it's

282
00:14:15,660 --> 00:14:21,240
almost 2

283
00:14:17,519 --> 00:14:23,940
000 times faster than WinDbg okay

284
00:14:21,240 --> 00:14:27,060
let's see some of the basic concepts

285
00:14:23,940 --> 00:14:30,779
that that you should know before 

286
00:14:27,060 --> 00:14:35,519
using this here is some of the keyboard

287
00:14:30,779 --> 00:14:38,540
keyboards that HyperDbg supports as

288
00:14:35,519 --> 00:14:42,000
you can see it supports POI or

289
00:14:38,540 --> 00:14:45,600
differencing a pointer or accessing the

290
00:14:42,000 --> 00:14:49,800
memory only pointed by a pointer the ref

291
00:14:45,600 --> 00:14:52,920
which is like

292
00:14:49,800 --> 00:14:55,560
ampersand like and

293
00:14:52,920 --> 00:14:57,720
song for the referencing for getting

294
00:14:55,560 --> 00:15:00,240
their reference address of the specific

295
00:14:57,720 --> 00:15:04,560
variable getting the pointer to a

296
00:15:00,240 --> 00:15:11,540
variable and it also supports DB which

297
00:15:04,560 --> 00:15:17,639
gets the low 8 Bits of a variable or

298
00:15:11,540 --> 00:15:25,380
value and then it has high which refers

299
00:15:17,639 --> 00:15:30,420
to high 16 bits of value low which

300
00:15:25,380 --> 00:15:34,680
represents the lower 16 bits DW which

301
00:15:30,420 --> 00:15:39,240
represents DW or define more distribute

302
00:15:34,680 --> 00:15:43,620
which represents low 16 bits TD or

303
00:15:39,240 --> 00:15:48,779
define DWORD which refers to 32 bits

304
00:15:43,620 --> 00:15:53,160
and DQ or defined quad which refers

305
00:15:48,779 --> 00:15:57,980
to 64 bits and not which is which flips

306
00:15:53,160 --> 00:16:01,500
each piece each and every bit and energy

307
00:15:57,980 --> 00:16:05,040
which is a true false logic Philippine

308
00:16:01,500 --> 00:16:06,920
and as you can see most of them are same

309
00:16:05,040 --> 00:16:10,440
as the

310
00:16:06,920 --> 00:16:15,000
keyboards that are used in WinDbg

311
00:16:10,440 --> 00:16:18,779
except this ref that is that it's

312
00:16:15,000 --> 00:16:22,019
more like referencing a variable 

313
00:16:18,779 --> 00:16:24,720
like getting the pointer to a variable

314
00:16:22,019 --> 00:16:28,800
and it's used mainly in functions like

315
00:16:24,720 --> 00:16:34,079
it's block and other functions and here

316
00:16:28,800 --> 00:16:37,620
the operators are supported by HyperDbg

317
00:16:34,079 --> 00:16:40,320
and it's just based on the Precedence of

318
00:16:37,620 --> 00:16:43,220
this operators you can use these

319
00:16:40,320 --> 00:16:46,320
operators in your scripts it's pretty

320
00:16:43,220 --> 00:16:49,740
self-explanatory I don't need to speak

321
00:16:46,320 --> 00:16:54,180
too much so now let's go to another

322
00:16:49,740 --> 00:16:57,300
slide here are the registers that are

323
00:16:54,180 --> 00:17:00,600
supported by HyperDbg somehow like a

324
00:16:57,300 --> 00:17:03,240
vindiviji currently hybridges supports

325
00:17:00,600 --> 00:17:06,720
these registers and as you can see

326
00:17:03,240 --> 00:17:12,780
almost all of the registers are starting

327
00:17:06,720 --> 00:17:15,059
with an at sign mark in HyperDbg so 

328
00:17:12,780 --> 00:17:16,339
if you want to use the general 

329
00:17:15,059 --> 00:17:19,380
purpose

330
00:17:16,339 --> 00:17:22,040
registers you can just put the name of

331
00:17:19,380 --> 00:17:25,799
them or if you there are also other

332
00:17:22,040 --> 00:17:30,360
64-bit mode resistors and stack pointers

333
00:17:25,799 --> 00:17:34,020
you can also access the rip or the

334
00:17:30,360 --> 00:17:37,620
program counter instructions or flags

335
00:17:34,020 --> 00:17:41,640
and each of the bits of the rflags are

336
00:17:37,620 --> 00:17:44,580
also separately you can put a value on

337
00:17:41,640 --> 00:17:48,299
each beat of the r Flex data segments

338
00:17:44,580 --> 00:17:52,500
and other segment registers are also

339
00:17:48,299 --> 00:17:55,200
available and the control registers

340
00:17:52,500 --> 00:17:57,780
memory management resistors interrupt

341
00:17:55,200 --> 00:18:00,240
descriptor registers and also debug

342
00:17:57,780 --> 00:18:02,760
resistors are also available in the

343
00:18:00,240 --> 00:18:05,520
script engine of the hyperactivity

344
00:18:02,760 --> 00:18:08,059
you can also use the model specific

345
00:18:05,520 --> 00:18:10,260
registers however

346
00:18:08,059 --> 00:18:13,020
using these models with speaker

347
00:18:10,260 --> 00:18:16,500
registers needs other commands like RJ

348
00:18:13,020 --> 00:18:18,600
master or wdr and stars and also as I

349
00:18:16,500 --> 00:18:21,600
told you before our flag bits are

350
00:18:18,600 --> 00:18:24,120
separately have their abbreviations in

351
00:18:21,600 --> 00:18:26,580
HyperDbg for example if you want to put

352
00:18:24,120 --> 00:18:30,419
the value on the carry flag or the

353
00:18:26,580 --> 00:18:33,419
parity flag you can just use the and you

354
00:18:30,419 --> 00:18:35,760
can just use them just like registers

355
00:18:33,419 --> 00:18:38,100
you can put the value or read them just

356
00:18:35,760 --> 00:18:41,160
like a registers there are also some

357
00:18:38,100 --> 00:18:45,900
suder registers this might whenever you

358
00:18:41,160 --> 00:18:48,419
are listening to this tutorial there

359
00:18:45,900 --> 00:18:50,880
might be an updated version of these two

360
00:18:48,419 --> 00:18:53,580
resistors that you can go to the

361
00:18:50,880 --> 00:18:56,600
document version and see the updates but

362
00:18:53,580 --> 00:18:59,760
for now it supports it's a dollar sign

363
00:18:56,600 --> 00:19:02,760
before each so the register has a dollar

364
00:18:59,760 --> 00:19:07,320
sign before it starts like dollar sign p

365
00:19:02,760 --> 00:19:09,179
ID which means that the process ID the

366
00:19:07,320 --> 00:19:11,160
current process ID that apparently

367
00:19:09,179 --> 00:19:15,179
executing or

368
00:19:11,160 --> 00:19:17,280
p r o C or prac which is the address of

369
00:19:15,179 --> 00:19:18,720
the current process or the EPROCESS of

370
00:19:17,280 --> 00:19:21,419
the current process

371
00:19:18,720 --> 00:19:24,059
there's also other important which is

372
00:19:21,419 --> 00:19:26,940
there's super registers like a pine

373
00:19:24,059 --> 00:19:31,380
which is a pointer to a character I have

374
00:19:26,940 --> 00:19:34,320
a 16 byte wide character array of the

375
00:19:31,380 --> 00:19:36,799
process name this is something really

376
00:19:34,320 --> 00:19:39,360
useful when you are trying to use the

377
00:19:36,799 --> 00:19:42,419
script engine for example you want to

378
00:19:39,360 --> 00:19:44,460
check for the name of a specific process

379
00:19:42,419 --> 00:19:48,240
that that are currently triggering an

380
00:19:44,460 --> 00:19:50,520
event and there are also some specific I

381
00:19:48,240 --> 00:19:52,860
don't want to talk about this 

382
00:19:50,520 --> 00:19:55,620
register because it's pretty clear but

383
00:19:52,860 --> 00:19:58,260
about these tools to the registers

384
00:19:55,620 --> 00:20:00,780
dollar buffer it's a pre-allocated

385
00:19:58,260 --> 00:20:03,480
buffer I will explain it later

386
00:20:00,780 --> 00:20:07,200
or we can access the pre-allocated

387
00:20:03,480 --> 00:20:11,160
buffer if we if we just try to read

388
00:20:07,200 --> 00:20:12,260
or modify these students there and also

389
00:20:11,160 --> 00:20:15,780
another

390
00:20:12,260 --> 00:20:19,860
important to register is dollar context

391
00:20:15,780 --> 00:20:23,700
the each and as I told you before each

392
00:20:19,860 --> 00:20:25,860
event has a has a context you can see

393
00:20:23,700 --> 00:20:29,820
the documentation for each event for

394
00:20:25,860 --> 00:20:32,820
example this the context for the system

395
00:20:29,820 --> 00:20:36,179
called events are the system call ID

396
00:20:32,820 --> 00:20:39,120
which is available on Rex register so

397
00:20:36,179 --> 00:20:41,760
you can just try to read the cons the

398
00:20:39,120 --> 00:20:44,820
context and decide what you're going to

399
00:20:41,760 --> 00:20:47,340
do in your experience and if you want to

400
00:20:44,820 --> 00:20:50,760
use by default for everything in high

401
00:20:47,340 --> 00:20:53,660
produces in hex format so if you want to

402
00:20:50,760 --> 00:20:58,220
just use other formats other than

403
00:20:53,660 --> 00:21:02,160
hexadecimal you can use these prefixes

404
00:20:58,220 --> 00:21:04,080
of course if you just put 0 X then it

405
00:21:02,160 --> 00:21:06,960
means the hexadecimal but if you don't

406
00:21:04,080 --> 00:21:09,360
put anything it's still interpreted as

407
00:21:06,960 --> 00:21:12,720
hexadecimal and there's nothing like

408
00:21:09,360 --> 00:21:17,160
decimal in HyperDbg if you want to

409
00:21:12,720 --> 00:21:19,559
use the a value in a decimal format in

410
00:21:17,160 --> 00:21:22,980
the script engine or in the entire

411
00:21:19,559 --> 00:21:25,440
debugger you can use zero and prefix

412
00:21:22,980 --> 00:21:28,380
which shows that the current address is

413
00:21:25,440 --> 00:21:30,799
the decimal address or zero T which

414
00:21:28,380 --> 00:21:35,059
stands for octal

415
00:21:30,799 --> 00:21:39,000
0 Y which which shows that

416
00:21:35,059 --> 00:21:42,240
that the value should be interpreted as

417
00:21:39,000 --> 00:21:45,200
a binary value for example here it is

418
00:21:42,240 --> 00:21:50,100
 one two three four five it's 18

419
00:21:45,200 --> 00:21:55,740
interpreted as X and then we have 0 y 1

420
00:21:50,100 --> 00:22:01,919
0 1 and 0 1 0 which is interpreted as

421
00:21:55,740 --> 00:22:06,659
binary and 0 and 85 5 1 2 which is

422
00:22:01,919 --> 00:22:11,820
interpreted in the decimal format 0 t 1

423
00:22:06,659 --> 00:22:14,820
5 7 1 6 which is interpreted as OCTA and

424
00:22:11,820 --> 00:22:17,159
the comments in the hybrid which is the

425
00:22:14,820 --> 00:22:21,480
script engine is pretty similar to the

426
00:22:17,159 --> 00:22:23,179
 C the to the regular C commands like

427
00:22:21,480 --> 00:22:26,520


428
00:22:23,179 --> 00:22:29,820
designs to create comments just like the

429
00:22:26,520 --> 00:22:32,640
C language okay now let's see how we can

430
00:22:29,820 --> 00:22:36,000
print a simple value in hybrid which is

431
00:22:32,640 --> 00:22:37,919
a script engine basically we can use

432
00:22:36,000 --> 00:22:40,380
the print command to print 

433
00:22:37,919 --> 00:22:43,380
expressions if you if we want to

434
00:22:40,380 --> 00:22:46,440
evaluate an X version we can use the

435
00:22:43,380 --> 00:22:49,620
print on however this command is not

436
00:22:46,440 --> 00:22:53,100
able to print the variable or

437
00:22:49,620 --> 00:22:55,080
messages but because it just tries to

438
00:22:53,100 --> 00:22:57,120
evaluate a simple command it's not

439
00:22:55,080 --> 00:22:59,760
designed to show the messages and

440
00:22:57,120 --> 00:23:02,220
instead of just if you want to show a

441
00:22:59,760 --> 00:23:05,340
message then you can use the printf

442
00:23:02,220 --> 00:23:08,760
function let's some examples the printf

443
00:23:05,340 --> 00:23:12,000
and the print command are the same for

444
00:23:08,760 --> 00:23:15,840
you can use the print as a command but

445
00:23:12,000 --> 00:23:18,260
if you want to use the print in the

446
00:23:15,840 --> 00:23:21,360
escape engine you can just put some

447
00:23:18,260 --> 00:23:23,700
parentheses between your between the

448
00:23:21,360 --> 00:23:26,880
start and end of your expert version on

449
00:23:23,700 --> 00:23:30,919
radio kit so if you want to just show

450
00:23:26,880 --> 00:23:34,620
all the use it as a command then print

451
00:23:30,919 --> 00:23:35,940
ra X means that we should print the RX

452
00:23:34,620 --> 00:23:39,960
which is there

453
00:23:35,940 --> 00:23:42,240
printing the dollar PID means that we

454
00:23:39,960 --> 00:23:45,539
should print the process ID of the

455
00:23:42,240 --> 00:23:49,980
current process also we can combine them

456
00:23:45,539 --> 00:23:54,900
in some expressions adding them or

457
00:23:49,980 --> 00:23:57,299
also we can add keywords like POI ecx

458
00:23:54,900 --> 00:24:00,659
which means that you should reference or

459
00:23:57,299 --> 00:24:02,400
get the value of the ecx and show the

460
00:24:00,659 --> 00:24:05,640
results it's exactly the same as green

461
00:24:02,400 --> 00:24:10,919
dvg also other things if you want to

462
00:24:05,640 --> 00:24:15,240
just get the data in a 64-bit format you

463
00:24:10,919 --> 00:24:18,539
can use the DQ keyword then also the

464
00:24:15,240 --> 00:24:22,260
thing is that if you set the symbols or

465
00:24:18,539 --> 00:24:24,240
the symbol you can use the the name

466
00:24:22,260 --> 00:24:29,159
of the symbols for example if you want

467
00:24:24,240 --> 00:24:31,980
to simply create a lot from ndkd default

468
00:24:29,159 --> 00:24:34,860
mask which is a variable in the Windows

469
00:24:31,980 --> 00:24:37,860
acting the kernel debugger mask of

470
00:24:34,860 --> 00:24:40,440
creating logs you can just put the name

471
00:24:37,860 --> 00:24:43,620
of the variable or the symbol name

472
00:24:40,440 --> 00:24:47,100
directly and use them between this

473
00:24:43,620 --> 00:24:51,000
keyboard or within the expressions also

474
00:24:47,100 --> 00:24:57,320
there are other examples that are here

475
00:24:51,000 --> 00:25:00,419
for example DQ POI r8 plus 10 plus 0x8

476
00:24:57,320 --> 00:25:05,760
it should be noted that 10 is the still

477
00:25:00,419 --> 00:25:09,419
like 0x10 which is a interpreted as X so

478
00:25:05,760 --> 00:25:16,140
if I want to simply explain it it adds 0

479
00:25:09,419 --> 00:25:18,600
x 10 to RH register then finds the value

480
00:25:16,140 --> 00:25:21,840
pointed by the result of this addition

481
00:25:18,600 --> 00:25:25,100
then add 0x to this result and

482
00:25:21,840 --> 00:25:29,840
eventually shows the value value in the

483
00:25:25,100 --> 00:25:29,840
address that that is computed here

