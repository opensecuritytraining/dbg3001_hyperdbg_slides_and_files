1
00:00:00,000 --> 00:00:06,120
the first section here would be

2
00:00:02,720 --> 00:00:08,519
debugging a multi-core systems here we

3
00:00:06,120 --> 00:00:12,240
want to see how we can switch to

4
00:00:08,519 --> 00:00:15,240
different cores in HyperDbg there is a

5
00:00:12,240 --> 00:00:17,699
command this is the tilde command that

6
00:00:15,240 --> 00:00:22,080
can be used to switch between different

7
00:00:17,699 --> 00:00:24,240
cores and also if you use this command

8
00:00:22,080 --> 00:00:26,220
without any number then it shows the

9
00:00:24,240 --> 00:00:28,439
current core this is a really good

10
00:00:26,220 --> 00:00:31,679
example if you have noticed that there

11
00:00:28,439 --> 00:00:34,860
is a number before the K HyperDbg here

12
00:00:31,679 --> 00:00:37,200
and it shows the current or that hybrid

13
00:00:34,860 --> 00:00:38,880
which is currently operating on it so if

14
00:00:37,200 --> 00:00:41,760
we want to just change it to another

15
00:00:38,880 --> 00:00:45,600
core for example we want to switch to

16
00:00:41,760 --> 00:00:48,960
the zero core then distill that command

17
00:00:45,600 --> 00:00:51,539
along with the number zero then the

18
00:00:48,960 --> 00:00:55,199
operating core will be changed to zero

19
00:00:51,539 --> 00:00:57,600
also if we want to change to other

20
00:00:55,199 --> 00:01:00,719
course then we will change the number

21
00:00:57,600 --> 00:01:04,379
and everything will be switched as you

22
00:01:00,719 --> 00:01:07,020
can see here at first we were the third

23
00:01:04,379 --> 00:01:10,380
core and after that we'll we switched to

24
00:01:07,020 --> 00:01:12,960
the zero score but as you can see the

25
00:01:10,380 --> 00:01:15,659
zeroth core was executing a user mode

26
00:01:12,960 --> 00:01:18,180
application because this address is it

27
00:01:15,659 --> 00:01:22,259
belongs to the user mode while other

28
00:01:18,180 --> 00:01:25,020
cores are debugging in a debugging

29
00:01:22,259 --> 00:01:27,659
kernel mode routines yeah here as you

30
00:01:25,020 --> 00:01:30,240
can see our other cores were trying to

31
00:01:27,659 --> 00:01:33,479
debug kernel mode routine because these

32
00:01:30,240 --> 00:01:36,420
addresses are kernel addresses of course

33
00:01:33,479 --> 00:01:39,299
this command can only be used in K hyper

34
00:01:36,420 --> 00:01:42,799
dvg in the kernel mode debugging part of

35
00:01:39,299 --> 00:01:42,799
the hybrid WG divider

