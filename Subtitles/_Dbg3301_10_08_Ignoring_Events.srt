1
00:00:00,000 --> 00:00:06,540
another important

2
00:00:02,480 --> 00:00:08,940
parts of the HyperDbg is ignoring event

3
00:00:06,540 --> 00:00:11,280
it's a new feature that is added

4
00:00:08,940 --> 00:00:13,799
recently in the HyperDbg

5
00:00:11,280 --> 00:00:16,859
which is called ignoring event there are

6
00:00:13,799 --> 00:00:20,400
some cases when messages just overflow

7
00:00:16,859 --> 00:00:22,800
like the HyperDbg just produces so much

8
00:00:20,400 --> 00:00:26,880
events or so much lacks from the

9
00:00:22,800 --> 00:00:30,599
execution that and then it cannot just

10
00:00:26,880 --> 00:00:33,360
gather all of them so for example in

11
00:00:30,599 --> 00:00:36,120
system calls there are thousands of

12
00:00:33,360 --> 00:00:38,579
system calls that are executed in each

13
00:00:36,120 --> 00:00:42,000
each time in Windows and if we want to

14
00:00:38,579 --> 00:00:44,340
save all of them then tons of messages

15
00:00:42,000 --> 00:00:47,219
are generated and it's not possible to

16
00:00:44,340 --> 00:00:51,719
just save all of them because it will

17
00:00:47,219 --> 00:00:54,420
fill or fully fill the RAM memory and

18
00:00:51,719 --> 00:00:58,199
HyperDbg will eventually is not able to

19
00:00:54,420 --> 00:01:00,960
deliver all of them to the debuggers so

20
00:00:58,199 --> 00:01:04,019
should like use some conditional

21
00:01:00,960 --> 00:01:06,299
statements like if else if and else just

22
00:01:04,019 --> 00:01:08,640
to filter the messages and instead of

23
00:01:06,299 --> 00:01:10,820
saving all of them we should we just

24
00:01:08,640 --> 00:01:12,720
need a portion of them so instead of

25
00:01:10,820 --> 00:01:15,060
saving all of them we use this

26
00:01:12,720 --> 00:01:19,080
conditional statements

27
00:01:15,060 --> 00:01:22,380
but in case a lot of messages are

28
00:01:19,080 --> 00:01:24,680
delivered and we want to just

29
00:01:22,380 --> 00:01:27,780
immediately

30
00:01:24,680 --> 00:01:31,860
ignore all of them produce the flash

31
00:01:27,780 --> 00:01:34,799
command it's clear that if a message

32
00:01:31,860 --> 00:01:36,840
overflow occurs in high productivity the

33
00:01:34,799 --> 00:01:39,720
newer messages will be replaced by

34
00:01:36,840 --> 00:01:42,659
previously not delivered messages so or

35
00:01:39,720 --> 00:01:45,240
in these cases we will lose some of the

36
00:01:42,659 --> 00:01:48,180
messages and for example in case of

37
00:01:45,240 --> 00:01:50,939
event forwarding if the output of the

38
00:01:48,180 --> 00:01:54,240
messages might be replaced by newer

39
00:01:50,939 --> 00:01:56,880
messages if the buffer is already full

40
00:01:54,240 --> 00:01:59,640
but there are also other ways for

41
00:01:56,880 --> 00:02:01,979
example if you're connecting other

42
00:01:59,640 --> 00:02:05,159
modes of the debugging this message

43
00:02:01,979 --> 00:02:08,039
overflow will never occur because in

44
00:02:05,159 --> 00:02:11,220
case of debugger mode every message is

45
00:02:08,039 --> 00:02:14,760
is delivered immediately none of them

46
00:02:11,220 --> 00:02:17,940
are buffered but if you are in a VMI

47
00:02:14,760 --> 00:02:20,220
mode then the messages will be buffered

48
00:02:17,940 --> 00:02:21,920
and this is a really important note that

49
00:02:20,220 --> 00:02:27,180
we should keep in mind

50
00:02:21,920 --> 00:02:29,700
a solution for the message overflow is

51
00:02:27,180 --> 00:02:32,340
flash command repurprisedly saw the

52
00:02:29,700 --> 00:02:35,220
flash command the exact same

53
00:02:32,340 --> 00:02:37,440
functionality is also revealed or is

54
00:02:35,220 --> 00:02:40,200
also available in the script engine if

55
00:02:37,440 --> 00:02:43,200
you want to use some s-script to flush

56
00:02:40,200 --> 00:02:46,500
the buffers we could use flash function

57
00:02:43,200 --> 00:02:49,080
in the descript engine and by using this

58
00:02:46,500 --> 00:02:51,540
flash command this the squash

59
00:02:49,080 --> 00:02:53,519
function in the script engine all of the

60
00:02:51,540 --> 00:02:57,300
currently buffered and not delivered

61
00:02:53,519 --> 00:03:01,560
messages will be flushed and ignored

62
00:02:57,300 --> 00:03:05,420
so here is a simple example in which I

63
00:03:01,560 --> 00:03:10,560
set an epd who can this address and if

64
00:03:05,420 --> 00:03:13,680
Rex is equal to 55 and RDS is equal to

65
00:03:10,560 --> 00:03:17,459
this then we will print the target

66
00:03:13,680 --> 00:03:20,700
functions call but if rcx register is

67
00:03:17,459 --> 00:03:22,620
equal to 75 then the event will be

68
00:03:20,700 --> 00:03:25,260
disabled the second event will be

69
00:03:22,620 --> 00:03:27,900
disabled and a flash function will be

70
00:03:25,260 --> 00:03:31,080
called there are also other mechanisms

71
00:03:27,900 --> 00:03:33,959
for ignoring events and the ignoring

72
00:03:31,080 --> 00:03:37,080
events is a powerful feature of the high

73
00:03:33,959 --> 00:03:39,239
produces the script engine and by using

74
00:03:37,080 --> 00:03:41,580
event influence there are some of the

75
00:03:39,239 --> 00:03:44,099
commands in HyperDbg that support event

76
00:03:41,580 --> 00:03:46,860
ignorance for example the system call

77
00:03:44,099 --> 00:03:49,560
command for example the IO in an Iowa

78
00:03:46,860 --> 00:03:52,140
out command or MSR read or MSR and also

79
00:03:49,560 --> 00:03:55,260
other commands these commands can be

80
00:03:52,140 --> 00:03:57,000
ignored which means that the effects of

81
00:03:55,260 --> 00:03:59,280
these commands will be ignored in the

82
00:03:57,000 --> 00:04:02,400
case if we want to manage it for some

83
00:03:59,280 --> 00:04:03,900
conditional for some conditions in the

84
00:04:02,400 --> 00:04:06,780
script engine

85
00:04:03,900 --> 00:04:09,780
because HyperDbg generally emulates the

86
00:04:06,780 --> 00:04:12,239
behavior of events for example HyperDbg

87
00:04:09,780 --> 00:04:13,640
emulates the behavior of syscall

88
00:04:12,239 --> 00:04:17,160
instruction

89
00:04:13,640 --> 00:04:19,440
after the VM exits so if the user just

90
00:04:17,160 --> 00:04:22,380
wants to ignore the emulation of some

91
00:04:19,440 --> 00:04:25,860
events then it's possible to simply use

92
00:04:22,380 --> 00:04:29,280
simple event a simple function which is

93
00:04:25,860 --> 00:04:31,500
called even underline a C which stands

94
00:04:29,280 --> 00:04:34,320
for event underline short circuiting

95
00:04:31,500 --> 00:04:37,320
this is a mechanism of HyperDbg and

96
00:04:34,320 --> 00:04:41,820
then we could specify one or zero or

97
00:04:37,320 --> 00:04:44,100
true or false as its arguments so if

98
00:04:41,820 --> 00:04:47,040
it's one then the high pressure will

99
00:04:44,100 --> 00:04:49,680
ignore the execution of that event or

100
00:04:47,040 --> 00:04:52,139
ignore the emulation of that event

101
00:04:49,680 --> 00:04:55,139
using this we could just use a fake

102
00:04:52,139 --> 00:04:57,120
results for example we could ignore some

103
00:04:55,139 --> 00:05:02,160
system calls or for example we could

104
00:04:57,120 --> 00:05:05,160
ignore some input and outputs and just

105
00:05:02,160 --> 00:05:07,620
add some fake results as a result and

106
00:05:05,160 --> 00:05:10,199
the operating system and the target

107
00:05:07,620 --> 00:05:12,960
debug you will never know that this

108
00:05:10,199 --> 00:05:16,139
system call is actually never executed

109
00:05:12,960 --> 00:05:18,479
only fake results are available so for

110
00:05:16,139 --> 00:05:21,240
example if you could ignore some reads

111
00:05:18,479 --> 00:05:24,720
or ignore some rights into the files

112
00:05:21,240 --> 00:05:27,539
this is a really powerful feature and

113
00:05:24,720 --> 00:05:29,759
if you want to or not all of the events

114
00:05:27,539 --> 00:05:32,280
in hybrid will just support these event

115
00:05:29,759 --> 00:05:35,100
ignorance because some of them because

116
00:05:32,280 --> 00:05:37,800
it's just not makes any sense in case of

117
00:05:35,100 --> 00:05:41,039
some events so if you want to see

118
00:05:37,800 --> 00:05:43,199
whether the target event supports event

119
00:05:41,039 --> 00:05:45,479
ignorance or not or even sure short

120
00:05:43,199 --> 00:05:49,080
circuiting or not you could just see the

121
00:05:45,479 --> 00:05:53,639
documentation for this command let's see

122
00:05:49,080 --> 00:05:55,800
an example there is a command for there

123
00:05:53,639 --> 00:05:58,919
is a short a short circuiting example

124
00:05:55,800 --> 00:06:03,419
here and we want to check if the system

125
00:05:58,919 --> 00:06:06,720
and call number for NtCreateFile the

126
00:06:03,419 --> 00:06:09,479
only check whether the system call is

127
00:06:06,720 --> 00:06:11,580
for NtCreateFile and if because in

128
00:06:09,479 --> 00:06:14,820
case of system calls the system call

129
00:06:11,580 --> 00:06:17,580
number is located in RX register and in

130
00:06:14,820 --> 00:06:20,820
current version of the Windows 0x55

131
00:06:17,580 --> 00:06:24,000
corresponds to integrate file and then

132
00:06:20,820 --> 00:06:27,780
we will check its second argument

133
00:06:24,000 --> 00:06:31,259
because in fast call the in system calls

134
00:06:27,780 --> 00:06:37,500
in fascal the parameters are passed in

135
00:06:31,259 --> 00:06:41,479
rcx rdx r8 and r9 then a stack so the

136
00:06:37,500 --> 00:06:46,100
second arguments passed in rdx

137
00:06:41,479 --> 00:06:49,500
we only modify the result to return 0x

138
00:06:46,100 --> 00:06:52,199
this code which corresponds to aesthetic

139
00:06:49,500 --> 00:06:54,720
status access denied and instead of

140
00:06:52,199 --> 00:06:58,199
running the system call to delete to

141
00:06:54,720 --> 00:07:02,940
perform delete a special file we simply

142
00:06:58,199 --> 00:07:05,699
use event underline SC1 to ignore the

143
00:07:02,940 --> 00:07:07,800
emulation or ignore the execution of the

144
00:07:05,699 --> 00:07:09,360
system call so the system call will be

145
00:07:07,800 --> 00:07:14,780
ignored

146
00:07:09,360 --> 00:07:14,780
see how we can use this command

147
00:07:15,560 --> 00:07:22,819
I try to on a notepad file

148
00:07:27,620 --> 00:07:36,120
s are open and I will print I will

149
00:07:33,180 --> 00:07:39,199
save HyperDbg to

150
00:07:36,120 --> 00:07:39,199
this file

151
00:07:42,660 --> 00:07:45,660
test.txt

152
00:07:47,759 --> 00:07:54,199
now I want to just open this file by

153
00:07:51,660 --> 00:07:54,199
using

154
00:07:54,319 --> 00:08:03,080
this notepad this instant of notepad so

155
00:07:58,560 --> 00:08:03,080
let's just try to run the HyperDbg

156
00:08:08,220 --> 00:08:11,220
foreign

157
00:08:15,979 --> 00:08:22,020
after that I try to

158
00:08:20,280 --> 00:08:25,259
run

159
00:08:22,020 --> 00:08:27,419
task manager to see the process ID of

160
00:08:25,259 --> 00:08:30,419
this notepad

161
00:08:27,419 --> 00:08:30,419
process

162
00:08:33,200 --> 00:08:43,279
so we should also convert it to

163
00:08:39,979 --> 00:08:43,279
hexadecimal

164
00:08:50,580 --> 00:08:56,640
so this is

165
00:08:52,320 --> 00:09:01,760
the hexadecimal of the process ID so

166
00:08:56,640 --> 00:09:05,420
I use a syscall command

167
00:09:01,760 --> 00:09:08,399
on this target

168
00:09:05,420 --> 00:09:12,420
PID process ID

169
00:09:08,399 --> 00:09:15,440
after that I will use a script and I

170
00:09:12,420 --> 00:09:18,660
know that if if notepad wants to open

171
00:09:15,440 --> 00:09:22,560
this special file then

172
00:09:18,660 --> 00:09:24,899
it should it should open it by using NV

173
00:09:22,560 --> 00:09:27,920
create file and the Antichrist create

174
00:09:24,899 --> 00:09:32,580
file system call number is

175
00:09:27,920 --> 00:09:36,060
0x55 so I will check if at sign RX or

176
00:09:32,580 --> 00:09:40,399
the system call number equals to zero

177
00:09:36,060 --> 00:09:40,399
it's 55 and if it's

178
00:09:40,800 --> 00:09:46,880
an NP open five then I simply

179
00:09:44,880 --> 00:09:51,540
set the

180
00:09:46,880 --> 00:09:54,180
RX to an axis denied I I don't know the

181
00:09:51,540 --> 00:09:58,940
exact code for access denied

182
00:09:54,180 --> 00:09:58,940
yeah here it is I will try to copy

183
00:10:02,519 --> 00:10:09,540
and instead of executing the system call

184
00:10:06,000 --> 00:10:16,279
I will ignore it so it's like nothing

185
00:10:09,540 --> 00:10:16,279
happens here like system call is ignore

186
00:10:17,399 --> 00:10:23,580
and the notepad is not able to run any

187
00:10:21,120 --> 00:10:27,019
NP open file

188
00:10:23,580 --> 00:10:27,019
system call

189
00:10:28,920 --> 00:10:33,260
so here was the 

190
00:10:33,420 --> 00:10:38,820
script now let's try to just simply open

191
00:10:36,420 --> 00:10:41,940
this test file

192
00:10:38,820 --> 00:10:44,339
as and as you can see that no notepads

193
00:10:41,940 --> 00:10:47,579
shows some errors like the handle is

194
00:10:44,339 --> 00:10:50,339
invalid and we could not open this one

195
00:10:47,579 --> 00:10:54,240
and you can also see if if you ever want

196
00:10:50,339 --> 00:10:56,940
to open other files some some rare

197
00:10:54,240 --> 00:11:00,660
thing happens here the notepad cannot

198
00:10:56,940 --> 00:11:04,140
simply use this system call and because

199
00:11:00,660 --> 00:11:08,899
of the script that we

200
00:11:04,140 --> 00:11:12,000
used so let's just try to

201
00:11:08,899 --> 00:11:14,579
clear it

202
00:11:12,000 --> 00:11:17,760
and

203
00:11:14,579 --> 00:11:20,880
if I try to open it again

204
00:11:17,760 --> 00:11:24,660
no we are able to open this hybrid video

205
00:11:20,880 --> 00:11:27,240
file okay know that we see an example

206
00:11:24,660 --> 00:11:29,940
of event short circuiting or event

207
00:11:27,240 --> 00:11:33,839
ignorance for the syscall command you can

208
00:11:29,940 --> 00:11:37,380
also we can also see another example

209
00:11:33,839 --> 00:11:38,660
about event ignorance for the monitor

210
00:11:37,380 --> 00:11:43,560
command

211
00:11:38,660 --> 00:11:46,140
 the only thing about muncher

212
00:11:43,560 --> 00:11:48,480
command is that eventual circuiting or

213
00:11:46,140 --> 00:11:51,660
event ignorance have two distinct

214
00:11:48,480 --> 00:11:53,839
behaviors for reason rights and for the

215
00:11:51,660 --> 00:11:57,360
execution

216
00:11:53,839 --> 00:12:00,779
 the short circuiting mechanism for

217
00:11:57,360 --> 00:12:03,720
reason right is like disregarding or

218
00:12:00,779 --> 00:12:07,680
ignoring the execution of 

219
00:12:03,720 --> 00:12:09,839
instructions that read or write 

220
00:12:07,680 --> 00:12:12,060
from the memory

221
00:12:09,839 --> 00:12:15,240
or to the memory such as move

222
00:12:12,060 --> 00:12:18,360
instructions for example it's like

223
00:12:15,240 --> 00:12:22,500
these instructions were never executed

224
00:12:18,360 --> 00:12:25,079
and reads are never performed or rights

225
00:12:22,500 --> 00:12:28,380
are never performed

226
00:12:25,079 --> 00:12:30,660
but for the execution if we use the

227
00:12:28,380 --> 00:12:33,360
short circuiting mechanism is like

228
00:12:30,660 --> 00:12:35,180
blocking the execution at the target

229
00:12:33,360 --> 00:12:38,100
address range

230
00:12:35,180 --> 00:12:40,380
and it prevents the execution of the

231
00:12:38,100 --> 00:12:43,740
specific page prevents the execution of

232
00:12:40,380 --> 00:12:46,560
a specific thread or a specific process

233
00:12:43,740 --> 00:12:47,660
will pause that process if that makes

234
00:12:46,560 --> 00:12:51,600
sense

235
00:12:47,660 --> 00:12:54,180
 once that page wants to get executed

236
00:12:51,600 --> 00:12:56,880
HyperDbg will block the execution in

237
00:12:54,180 --> 00:12:59,519
that page again the the page tries again

238
00:12:56,880 --> 00:13:03,000
to execute again high producing blocks

239
00:12:59,519 --> 00:13:07,440
and after some times the Windows comes

240
00:13:03,000 --> 00:13:09,899
and context switch the process so 

241
00:13:07,440 --> 00:13:13,680
once the Windows returns to this process

242
00:13:09,899 --> 00:13:15,839
again it wants to execute the target

243
00:13:13,680 --> 00:13:19,200
address and again hypervisual blockage

244
00:13:15,839 --> 00:13:22,320
so it's like pausing that process or

245
00:13:19,200 --> 00:13:24,360
pausing that thread once you use the

246
00:13:22,320 --> 00:13:26,760
short circuiting don't worry about it we

247
00:13:24,360 --> 00:13:32,339
have some examples about it

248
00:13:26,760 --> 00:13:37,920
I will show you later but in case once

249
00:13:32,339 --> 00:13:41,399
we don't use this event SC or once we

250
00:13:37,920 --> 00:13:44,339
not use this event short circuiting

251
00:13:41,399 --> 00:13:47,220
mechanism it's like stepping through the

252
00:13:44,339 --> 00:13:51,540
instructions in that page so it means

253
00:13:47,220 --> 00:13:55,139
once the the target page 

254
00:13:51,540 --> 00:13:58,440
triggers one event one monitor event and

255
00:13:55,139 --> 00:14:01,680
we we don't use events C so it continues

256
00:13:58,440 --> 00:14:04,500
normally executes one instruction and

257
00:14:01,680 --> 00:14:09,000
again our next instruction against

258
00:14:04,500 --> 00:14:11,420
trigger the events so it's like a

259
00:14:09,000 --> 00:14:11,420
stepping

260
00:14:11,579 --> 00:14:18,480
 here are some examples in the

261
00:14:15,360 --> 00:14:21,060
first example we intercept the execution

262
00:14:18,480 --> 00:14:23,700
starting from this address to this

263
00:14:21,060 --> 00:14:27,079
address and this specific process we

264
00:14:23,700 --> 00:14:30,540
blocked the execution once a threat

265
00:14:27,079 --> 00:14:35,040
comes to this address is trapped and

266
00:14:30,540 --> 00:14:38,360
cannot execution anymore without 

267
00:14:35,040 --> 00:14:41,639
without any further actions from the

268
00:14:38,360 --> 00:14:45,440
debugger and

269
00:14:41,639 --> 00:14:49,560
and this monitor command 

270
00:14:45,440 --> 00:14:51,360
intercepts any rights starting from this

271
00:14:49,560 --> 00:14:54,660
address to this address and this

272
00:14:51,360 --> 00:14:58,380
specific process and it ignores the

273
00:14:54,660 --> 00:15:02,639
right for example we don't want we

274
00:14:58,380 --> 00:15:04,260
want to prevent any memory right and a

275
00:15:02,639 --> 00:15:07,860
special variable

276
00:15:04,260 --> 00:15:10,940
so we use these for we use the

277
00:15:07,860 --> 00:15:14,940
monitor like this and 

278
00:15:10,940 --> 00:15:17,880
prevent or ignore the memory right by

279
00:15:14,940 --> 00:15:21,899
using events C or again if we want to

280
00:15:17,880 --> 00:15:26,040
ignore reads from this address we can

281
00:15:21,899 --> 00:15:27,180
also use event SC and set the registers

282
00:15:26,040 --> 00:15:30,560
manual

283
00:15:27,180 --> 00:15:36,440
now let's see the examples

284
00:15:30,560 --> 00:15:36,440
for reads and writes I made a

285
00:15:36,480 --> 00:15:45,779
 one simple program this

286
00:15:42,180 --> 00:15:47,639
program consists of volatile integer

287
00:15:45,779 --> 00:15:51,959


288
00:15:47,639 --> 00:15:54,360
the integer variable named test

289
00:15:51,959 --> 00:15:58,079
we

290
00:15:54,360 --> 00:16:00,720
print the process ID and the address of

291
00:15:58,079 --> 00:16:04,320
the test and after that try to increment

292
00:16:00,720 --> 00:16:06,899
it each two seconds test this income

293
00:16:04,320 --> 00:16:11,279
incremented by one and we will show it

294
00:16:06,899 --> 00:16:13,199
here now let's come back to the 

295
00:16:11,279 --> 00:16:18,480
divided

296
00:16:13,199 --> 00:16:21,959
I will continue it and run right

297
00:16:18,480 --> 00:16:25,560
ignore you can see that this is the

298
00:16:21,959 --> 00:16:30,360
process and this variable is

299
00:16:25,560 --> 00:16:34,980
incremented each time so I try to get

300
00:16:30,360 --> 00:16:39,259
its memory address go back to debugger

301
00:16:34,980 --> 00:16:39,259
write a manager command

302
00:16:40,380 --> 00:16:49,380
 start for the rights starting from

303
00:16:45,240 --> 00:16:52,880
this address and ending from this

304
00:16:49,380 --> 00:16:57,980
address plus four because it's land into

305
00:16:52,880 --> 00:17:03,360
integer 32 bit

306
00:16:57,980 --> 00:17:09,120
 long or four byte lung variable

307
00:17:03,360 --> 00:17:12,240
and in the S script I I will write 

308
00:17:09,120 --> 00:17:16,880
a printf

309
00:17:12,240 --> 00:17:21,480
stating that memory

310
00:17:16,880 --> 00:17:24,059
right is ignored

311
00:17:21,480 --> 00:17:27,199
at

312
00:17:24,059 --> 00:17:27,199
this address

313
00:17:29,640 --> 00:17:36,660
context sudo register and then I write

314
00:17:33,660 --> 00:17:36,660
events

315
00:17:37,559 --> 00:17:40,559
SC1

316
00:17:41,039 --> 00:17:47,160
and I will run as you can see it's

317
00:17:43,500 --> 00:17:51,600
incremented here one two three four five

318
00:17:47,160 --> 00:17:56,100
 five six seven and now that we

319
00:17:51,600 --> 00:17:59,240
executed we didn't specify the 

320
00:17:56,100 --> 00:17:59,240
process ID

321
00:18:00,240 --> 00:18:03,620
let's again

322
00:18:20,179 --> 00:18:24,980
what's this it's a script

323
00:18:26,280 --> 00:18:30,799
oh I have a typo here

324
00:18:40,380 --> 00:18:48,320
okay now we return to the debugging and

325
00:18:44,100 --> 00:18:52,440
as you can see it detects that

326
00:18:48,320 --> 00:18:53,480
 each each time this process tries to

327
00:18:52,440 --> 00:18:58,740
write

328
00:18:53,480 --> 00:19:02,160
 into this address it's ignored and 

329
00:18:58,740 --> 00:19:05,400
it just shows eight and we can detect

330
00:19:02,160 --> 00:19:09,120
the execution or we can intercept it

331
00:19:05,400 --> 00:19:14,280
here in this 

332
00:19:09,120 --> 00:19:18,000
a window and once we clear the event you

333
00:19:14,280 --> 00:19:20,460
can see that it normally increments to 9

334
00:19:18,000 --> 00:19:23,460
10 and 11.

335
00:19:20,460 --> 00:19:23,460


336
00:19:23,700 --> 00:19:27,080
no 

337
00:19:28,160 --> 00:19:32,960
let's go to another example

338
00:19:33,740 --> 00:19:43,740
 for the second example I made a

339
00:19:39,480 --> 00:19:46,020
simple hello world program  we

340
00:19:43,740 --> 00:19:47,460
previously have this example in the

341
00:19:46,020 --> 00:19:50,280
previous part

342
00:19:47,460 --> 00:19:52,799
 but I think it's just they're really

343
00:19:50,280 --> 00:19:55,620
good to be used in this example for

344
00:19:52,799 --> 00:19:58,140
blocking the execution and to show how

345
00:19:55,620 --> 00:20:02,520
we can block the execution in this

346
00:19:58,140 --> 00:20:05,760
module so you can compile it and

347
00:20:02,520 --> 00:20:08,760
after that you can 

348
00:20:05,760 --> 00:20:12,059
come back to the debugger run it we can

349
00:20:08,760 --> 00:20:15,179
we can use 

350
00:20:12,059 --> 00:20:17,820
 data start command to

351
00:20:15,179 --> 00:20:22,559
start this process

352
00:20:17,820 --> 00:20:25,380
and once I press G we come to its entry

353
00:20:22,559 --> 00:20:29,039
point again I press G to run it as you

354
00:20:25,380 --> 00:20:32,160
can see this is a normal hello world

355
00:20:29,039 --> 00:20:35,240
program that shows hello world each two

356
00:20:32,160 --> 00:20:40,559
seconds in an infinite loop

357
00:20:35,240 --> 00:20:44,580
so I go back to the debugger again I

358
00:20:40,559 --> 00:20:48,960
don't know the process ID so

359
00:20:44,580 --> 00:20:52,620
let's see the list of processes and 

360
00:20:48,960 --> 00:20:57,780
all over this here 3D

361
00:20:52,620 --> 00:21:01,140
C so I I just want to see the modules of

362
00:20:57,780 --> 00:21:01,140
this 

363
00:21:02,640 --> 00:21:07,340
process so I use LM command

364
00:21:08,580 --> 00:21:13,100
to see the user mode modules as you can

365
00:21:10,919 --> 00:21:16,080
see it's here

366
00:21:13,100 --> 00:21:19,620
 the entry point is that this address

367
00:21:16,080 --> 00:21:23,400
and this module starts from this address

368
00:21:19,620 --> 00:21:26,340
we can intercept the execution for the

369
00:21:23,400 --> 00:21:29,960
entire module for example we can use it

370
00:21:26,340 --> 00:21:33,480
from the very first 

371
00:21:29,960 --> 00:21:37,559
address but we can also use a page that

372
00:21:33,480 --> 00:21:40,020
we know that it will be executed so I

373
00:21:37,559 --> 00:21:42,299
just create one monitor command for the

374
00:21:40,020 --> 00:21:44,840
execution starting from this address

375
00:21:42,299 --> 00:21:44,840
let's

376
00:21:46,280 --> 00:21:52,679
the execution blocking on an entire page

377
00:21:49,980 --> 00:21:56,820
where the entry point is also located

378
00:21:52,679 --> 00:21:59,720
there so I use this address again

379
00:21:56,820 --> 00:21:59,720


380
00:22:00,059 --> 00:22:05,039
is it like this because I want to

381
00:22:02,280 --> 00:22:08,640
intercept it on one page you can extend

382
00:22:05,039 --> 00:22:10,380
 you can add some additional page if

383
00:22:08,640 --> 00:22:13,740
you think that this module will be

384
00:22:10,380 --> 00:22:15,840
executed on other pages but I think this

385
00:22:13,740 --> 00:22:19,380
is enough because it's a really small

386
00:22:15,840 --> 00:22:21,480
program with probably small

387
00:22:19,380 --> 00:22:24,059
instructions a small number of

388
00:22:21,480 --> 00:22:26,400
instructions

389
00:22:24,059 --> 00:22:27,679
 or short number of instructions

390
00:22:26,400 --> 00:22:32,520
actually

391
00:22:27,679 --> 00:22:35,400
so I write the process ID don't

392
00:22:32,520 --> 00:22:38,580
forget the process ID because we are not

393
00:22:35,400 --> 00:22:42,260
in the context of hybrid in the context

394
00:22:38,580 --> 00:22:46,860
of this process so I write an script

395
00:22:42,260 --> 00:22:49,860
and I use event

396
00:22:46,860 --> 00:22:49,860
SC1

397
00:22:50,539 --> 00:22:57,500
and show a printer showing that 

398
00:22:58,320 --> 00:23:03,240
instruction

399
00:23:00,020 --> 00:23:05,940
which is blocked

400
00:23:03,240 --> 00:23:08,960
at

401
00:23:05,940 --> 00:23:08,960
this address

402
00:23:12,780 --> 00:23:17,400
yeah that's enough no let's run it and

403
00:23:16,200 --> 00:23:20,640
as you can see we're running

404
00:23:17,400 --> 00:23:22,980
successfully now as you can see here a

405
00:23:20,640 --> 00:23:25,440
high produce is flooded with a lot of

406
00:23:22,980 --> 00:23:27,960
messages that say the instruction

407
00:23:25,440 --> 00:23:30,720
execution is blocked it's because each

408
00:23:27,960 --> 00:23:32,900
time Windows tries to context switch and

409
00:23:30,720 --> 00:23:36,179
run some instructions in this process

410
00:23:32,900 --> 00:23:38,100
HyperDbg will show this message so it's

411
00:23:36,179 --> 00:23:41,220
probably in this case it's probably

412
00:23:38,100 --> 00:23:44,580
better not to show any message to just

413
00:23:41,220 --> 00:23:47,159
not make the system a slow we can we

414
00:23:44,580 --> 00:23:51,720
could use it without 

415
00:23:47,159 --> 00:23:55,919
without any extra messages but once

416
00:23:51,720 --> 00:24:00,419
we continue this target debuggee

417
00:23:55,919 --> 00:24:04,440
you can see that it again start to show

418
00:24:00,419 --> 00:24:08,900
a hello world messages and it starts to

419
00:24:04,440 --> 00:24:08,900
execute its normal execution

420
00:24:09,679 --> 00:24:18,559
now let's see what happens if we run the

421
00:24:13,799 --> 00:24:18,559
previous example without any message

422
00:24:19,320 --> 00:24:24,020
so

423
00:24:21,419 --> 00:24:24,020
yeah

424
00:24:25,980 --> 00:24:32,340
as you can see again it's blocked and 

425
00:24:29,460 --> 00:24:34,260
there's no printf so this is not a

426
00:24:32,340 --> 00:24:38,900
really heavy task of sending messages

427
00:24:34,260 --> 00:24:38,900
here and it works perfect

428
00:24:40,799 --> 00:24:49,980
again if I clear the the events you

429
00:24:46,260 --> 00:24:53,880
can see that this process start to

430
00:24:49,980 --> 00:24:56,480
execute normally without blocking its

431
00:24:53,880 --> 00:24:56,480
instructions

