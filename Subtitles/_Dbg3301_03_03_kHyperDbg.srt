1
00:00:00,060 --> 00:00:08,160
the next thing about HyperDbg is its

2
00:00:04,020 --> 00:00:12,360
kernel debugger we can use the kernel

3
00:00:08,160 --> 00:00:16,080
debugger to set up a high produces it's

4
00:00:12,360 --> 00:00:18,300
called K HyperDbg you need to have

5
00:00:16,080 --> 00:00:21,960
you need to set up a physical serial

6
00:00:18,300 --> 00:00:24,240
port or virtual stereo device namely or

7
00:00:21,960 --> 00:00:25,980
something like a name pipe hyper

8
00:00:24,240 --> 00:00:29,699
division debugging needs to support

9
00:00:25,980 --> 00:00:32,780
Intel VMS and Intel Equity but we can

10
00:00:29,699 --> 00:00:35,520
but it also is able

11
00:00:32,780 --> 00:00:38,840
to run an enlisted virtualization

12
00:00:35,520 --> 00:00:44,420
environment like member workstation

13
00:00:38,840 --> 00:00:44,420
let's see a demo

14
00:00:46,800 --> 00:00:53,280
first of all I have to create a

15
00:00:50,000 --> 00:00:55,199
completely 

16
00:00:53,280 --> 00:00:58,500
you

17
00:00:55,199 --> 00:01:01,020
serial device you have to turn off your

18
00:00:58,500 --> 00:01:04,820
VM

19
00:01:01,020 --> 00:01:04,820
and then let me just

20
00:01:05,880 --> 00:01:13,979
to create a new one I will go to the ad

21
00:01:09,060 --> 00:01:17,939
and serial for I specify a Serial port

22
00:01:13,979 --> 00:01:20,700
then I use a named pipe

23
00:01:17,939 --> 00:01:24,299
and we can also with it to a physical

24
00:01:20,700 --> 00:01:28,619
stereo for the port is also possible to

25
00:01:24,299 --> 00:01:32,220
use this method but for no I prefer

26
00:01:28,619 --> 00:01:36,960
to use nameplate because it's easier

27
00:01:32,220 --> 00:01:39,900
I choose the name HyperDbg pipe

28
00:01:36,960 --> 00:01:43,140
you have to specify this string first

29
00:01:39,900 --> 00:01:46,140
before your name is like

30
00:01:43,140 --> 00:01:46,140
slash

31
00:01:49,380 --> 00:01:54,540
this is the end this is the end service

32
00:01:51,899 --> 00:01:57,240
yes and the other end is the virtual

33
00:01:54,540 --> 00:01:59,399
machine and also we should yield a CPU

34
00:01:57,240 --> 00:02:02,640
and pull

35
00:01:59,399 --> 00:02:05,399
so I will create it and it will be

36
00:02:02,640 --> 00:02:08,420
connected at the power on we want to

37
00:02:05,399 --> 00:02:12,180
again see this

38
00:02:08,420 --> 00:02:14,220
device it's also added here but for now

39
00:02:12,180 --> 00:02:17,300
I will return to my previous estate

40
00:02:14,220 --> 00:02:20,060
which I already made the

41
00:02:17,300 --> 00:02:24,300
debugging cereal

42
00:02:20,060 --> 00:02:25,980
cable here but it's also okay you can

43
00:02:24,300 --> 00:02:30,319
start it

44
00:02:25,980 --> 00:02:33,780
and it would probably be fine

45
00:02:30,319 --> 00:02:36,060
in my current this is the way that I

46
00:02:33,780 --> 00:02:38,420
used to debug hybrid VGA will create

47
00:02:36,060 --> 00:02:42,800
some 

48
00:02:38,420 --> 00:02:45,959
snapshots from the state of my VM and

49
00:02:42,800 --> 00:02:48,780
after that because everything is ready

50
00:02:45,959 --> 00:02:51,379
and tools are ready and it's easier to

51
00:02:48,780 --> 00:02:55,080
start the

52
00:02:51,379 --> 00:02:58,680
virtual machine and after that I try to

53
00:02:55,080 --> 00:03:01,940
attach a vindi video to it like I

54
00:02:58,680 --> 00:03:07,340
used to make a

55
00:03:01,940 --> 00:03:07,340
a script for it you can see

56
00:03:08,700 --> 00:03:15,840
that I just started individually with my

57
00:03:11,879 --> 00:03:17,060
key and my port address and after

58
00:03:15,840 --> 00:03:19,920
that

59
00:03:17,060 --> 00:03:22,440
the individges tank that the reason why

60
00:03:19,920 --> 00:03:25,080
I always connect every individually

61
00:03:22,440 --> 00:03:29,040
before connecting HyperDbg is that I

62
00:03:25,080 --> 00:03:32,700
want as I explained previously I want a

63
00:03:29,040 --> 00:03:35,760
vindi BG to bypass the diversity

64
00:03:32,700 --> 00:03:37,739
Insurance enforcement for me so both of

65
00:03:35,760 --> 00:03:41,000
the whenever the individually is

66
00:03:37,739 --> 00:03:44,760
attached from the start of 

67
00:03:41,000 --> 00:03:47,760
system then the PatchGuard won't get a

68
00:03:44,760 --> 00:03:49,799
chance there won't be run or load this

69
00:03:47,760 --> 00:03:52,620
chance to get executed so we don't have

70
00:03:49,799 --> 00:03:54,060
PatchGuard and it's really easier to

71
00:03:52,620 --> 00:03:58,860
debug

72
00:03:54,060 --> 00:04:02,519
and also another thing is that it 

73
00:03:58,860 --> 00:04:05,760
it just stops IT the driver's signature

74
00:04:02,519 --> 00:04:08,099
enforcement and I can continue easily

75
00:04:05,760 --> 00:04:09,720
with that with that unsigned version of

76
00:04:08,099 --> 00:04:14,340
the hypertension

77
00:04:09,720 --> 00:04:17,040
so basically I have this do I get

78
00:04:14,340 --> 00:04:20,160
here no it's time we should return back

79
00:04:17,040 --> 00:04:22,380
to our past this application is

80
00:04:20,160 --> 00:04:26,340
executing in Haas should first let's

81
00:04:22,380 --> 00:04:28,919
start from the Huskies the for for

82
00:04:26,340 --> 00:04:31,740
the first step there's no need to the

83
00:04:28,919 --> 00:04:32,840
guests and I specific that that debug

84
00:04:31,740 --> 00:04:37,919
command

85
00:04:32,840 --> 00:04:41,280
debug command we specified that debug

86
00:04:37,919 --> 00:04:44,280
remote name pipe and we put the name of

87
00:04:41,280 --> 00:04:48,000
the name pipe that we previously added

88
00:04:44,280 --> 00:04:52,800
to the VMware here

89
00:04:48,000 --> 00:04:55,860
so I put it here and now it just Waits

90
00:04:52,800 --> 00:04:57,840
For the Key by key to connect to the US

91
00:04:55,860 --> 00:05:02,699
there are also other options for example

92
00:04:57,840 --> 00:05:05,759
if you want to connect to a remote

93
00:05:02,699 --> 00:05:09,720
serial port you can specify it's it's

94
00:05:05,759 --> 00:05:11,540
named for example com3 or come to based

95
00:05:09,720 --> 00:05:16,500
on your device

96
00:05:11,540 --> 00:05:20,840
your device com port in case and now

97
00:05:16,500 --> 00:05:20,840
it's time to go back to the

98
00:05:21,919 --> 00:05:29,400
as an administrator because it needs the

99
00:05:25,080 --> 00:05:31,560
privilege to install the driver again I

100
00:05:29,400 --> 00:05:34,380
use the debug command and instead of

101
00:05:31,560 --> 00:05:37,500
using remote as the first parameter

102
00:05:34,380 --> 00:05:40,800
after the the dot debug command I use

103
00:05:37,500 --> 00:05:44,120
the pre-brow command which shows that we

104
00:05:40,800 --> 00:05:47,400
want to prepare this 

105
00:05:44,120 --> 00:05:48,620
this computer to be debugged by a

106
00:05:47,400 --> 00:05:52,560
debugger

107
00:05:48,620 --> 00:05:55,800
I know that my comport is mapped to come

108
00:05:52,560 --> 00:05:59,460
to there might be also income one you

109
00:05:55,800 --> 00:06:03,479
should test it and see whether it works

110
00:05:59,460 --> 00:06:08,479
or not or you can directly see the tab

111
00:06:03,479 --> 00:06:13,940
the device manager and find the specific

112
00:06:08,479 --> 00:06:16,680
mapped com serial port device for the

113
00:06:13,940 --> 00:06:18,960
the device that we previously added to

114
00:06:16,680 --> 00:06:21,660
the VMware

115
00:06:18,960 --> 00:06:27,300
so I use this command

116
00:06:21,660 --> 00:06:29,759
and just wait till the HyperDbg is

117
00:06:27,300 --> 00:06:34,440
connected here actually if you see that

118
00:06:29,759 --> 00:06:38,639
 VMware tries to connect and whenever

119
00:06:34,440 --> 00:06:43,139
VMware tries successfully you'll see

120
00:06:38,639 --> 00:06:46,979
that it was just unsuccessful at our

121
00:06:43,139 --> 00:06:51,180
first step because the current the

122
00:06:46,979 --> 00:06:52,800
VMware was not connected to the the

123
00:06:51,180 --> 00:06:55,319
individual was not connected to the

124
00:06:52,800 --> 00:06:59,039
VMware but now it's connected and

125
00:06:55,319 --> 00:07:02,880
everything is fine if I execute it again

126
00:06:59,039 --> 00:07:07,620
you can see that you know the VMware is

127
00:07:02,880 --> 00:07:11,180
connected to the WinDbg and I'm as the

128
00:07:07,620 --> 00:07:14,400
driver's signature enforcement is not

129
00:07:11,180 --> 00:07:16,440
disabled in this stage we can run

130
00:07:14,400 --> 00:07:19,880
hybridges

131
00:07:16,440 --> 00:07:22,580
also in the 

132
00:07:19,880 --> 00:07:25,979
hostage state I have

133
00:07:22,580 --> 00:07:29,099
 HyperDbg which says that it

134
00:07:25,979 --> 00:07:31,639
basically running on a Windows 10 Pro

135
00:07:29,099 --> 00:07:34,680
it's actually Windows 11 but

136
00:07:31,639 --> 00:07:36,720
they didn't update in in the current

137
00:07:34,680 --> 00:07:38,880
version they didn't update the register

138
00:07:36,720 --> 00:07:41,400
file that shows the

139
00:07:38,880 --> 00:07:44,479
I mean Windows name so it's a problem

140
00:07:41,400 --> 00:07:47,039
for Windows network HyperDbg

141
00:07:44,479 --> 00:07:50,039
data it starts getting the symbol

142
00:07:47,039 --> 00:07:53,340
details from the debugging system and

143
00:07:50,039 --> 00:07:57,960
now the debug everything is interpreted

144
00:07:53,340 --> 00:08:00,240
and created so we can press Ctrl C and

145
00:07:57,960 --> 00:08:03,319
stop the debug as you can see that here

146
00:08:00,240 --> 00:08:07,860
the debugging is currently working and

147
00:08:03,319 --> 00:08:11,759
and is in a running state but if I press

148
00:08:07,860 --> 00:08:16,139
Ctrl pass C just everything it stops and

149
00:08:11,759 --> 00:08:18,479
I cannot do anything in the debugging

150
00:08:16,139 --> 00:08:21,720
system so everything is halted and the

151
00:08:18,479 --> 00:08:24,599
debugging is waiting for a commands so I

152
00:08:21,720 --> 00:08:29,000
just clear my string and press G is

153
00:08:24,599 --> 00:08:29,000
exactly like min Luigi to continue

154
00:08:33,320 --> 00:08:41,279
this is how we can set up the kernel

155
00:08:36,300 --> 00:08:44,279
modular year of hybrid which okay no 

156
00:08:41,279 --> 00:08:47,279
 we can use the data start or that

157
00:08:44,279 --> 00:08:51,420
restart command to create process or

158
00:08:47,279 --> 00:08:55,040
make another process or either things

159
00:08:51,420 --> 00:08:55,040
related to the process

