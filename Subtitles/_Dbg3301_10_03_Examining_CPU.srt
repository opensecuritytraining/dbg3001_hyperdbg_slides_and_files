1
00:00:00,000 --> 00:00:08,340
another command for HyperDbg is how it

2
00:00:04,920 --> 00:00:12,179
examines the CPU to enumerate processor

3
00:00:08,340 --> 00:00:17,880
features for example in this command I

4
00:00:12,179 --> 00:00:20,640
executed the CPU command which shows the

5
00:00:17,880 --> 00:00:24,660
different capabilities of my processors

6
00:00:20,640 --> 00:00:30,019
so we could also test it on the hybrid

7
00:00:24,660 --> 00:00:30,019
vg2 I use the source code directly here

8
00:00:31,340 --> 00:00:37,760
it shows the different features that are

9
00:00:34,380 --> 00:00:41,160
supported by my processor

10
00:00:37,760 --> 00:00:43,559
and you could you could use it to

11
00:00:41,160 --> 00:00:46,860
examine different features of the hyper

12
00:00:43,559 --> 00:00:50,660
both HyperDbg and for other

13
00:00:46,860 --> 00:00:50,660
applications if you want to use

