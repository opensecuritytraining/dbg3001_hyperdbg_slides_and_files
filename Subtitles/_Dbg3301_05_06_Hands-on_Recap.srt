1
00:00:00,000 --> 00:00:06,060
yeah we are in a hands-on section of

2
00:00:02,639 --> 00:00:08,820
this part so we're gonna see a practical

3
00:00:06,060 --> 00:00:12,059
examples of how we can combine the

4
00:00:08,820 --> 00:00:15,240
script engine and use it to change the

5
00:00:12,059 --> 00:00:18,660
control flow of a real world program and

6
00:00:15,240 --> 00:00:21,600
for this part of the tutorial I made

7
00:00:18,660 --> 00:00:25,320
a really simple C program that just

8
00:00:21,600 --> 00:00:28,740
keeps showing castles through each two

9
00:00:25,320 --> 00:00:32,579
seconds like because this test pool

10
00:00:28,740 --> 00:00:35,820
is true just keeps telling that the test

11
00:00:32,579 --> 00:00:39,600
was true so I will put this application

12
00:00:35,820 --> 00:00:40,760
to my virtual machine and meanwhile I

13
00:00:39,600 --> 00:00:45,059
bring

14
00:00:40,760 --> 00:00:49,020
x64dbg to show how we can use a hybrid

15
00:00:45,059 --> 00:00:51,420
vision and XC and x64dbg together and

16
00:00:49,020 --> 00:00:55,620
just try to run

17
00:00:51,420 --> 00:00:58,440
okay here we go just keep running the

18
00:00:55,620 --> 00:01:01,800
deal again you can see that this keeps

19
00:00:58,440 --> 00:01:05,339
running exit like test underline

20
00:01:01,800 --> 00:01:08,040
pool is true so we're gonna just put

21
00:01:05,339 --> 00:01:10,400
some debugging here some fast debugging

22
00:01:08,040 --> 00:01:10,400
here

23
00:01:11,880 --> 00:01:17,460
and to find where where is the guilty

24
00:01:15,600 --> 00:01:21,240
function that is

25
00:01:17,460 --> 00:01:23,700
thank you responsible for sitting

26
00:01:21,240 --> 00:01:26,220
or showing

27
00:01:23,700 --> 00:01:29,220
this message

28
00:01:26,220 --> 00:01:32,939
so yeah yeah he was here this code

29
00:01:29,220 --> 00:01:36,119
function will eventually lead to this 

30
00:01:32,939 --> 00:01:39,659
test underlying pool is true so what

31
00:01:36,119 --> 00:01:43,320
we're gonna do now is that we know that

32
00:01:39,659 --> 00:01:47,700
this jump or the this jne jump not

33
00:01:43,320 --> 00:01:50,340
equals will pass the this call and

34
00:01:47,700 --> 00:01:52,439
probably this is the guilty function or

35
00:01:50,340 --> 00:01:56,119
this is the conditional statement here

36
00:01:52,439 --> 00:01:56,119
so I just

37
00:01:59,159 --> 00:02:04,439
put

38
00:02:01,740 --> 00:02:07,860
and just copy the address from this

39
00:02:04,439 --> 00:02:10,739
function and try to manipulate the

40
00:02:07,860 --> 00:02:15,500
execution flow that's enough for this

41
00:02:10,739 --> 00:02:15,500
program I will run another program again

42
00:02:21,840 --> 00:02:24,140
yeah

43
00:02:24,239 --> 00:02:29,760
it's I just copy this address and

44
00:02:28,379 --> 00:02:34,040
this is the address that I want to

45
00:02:29,760 --> 00:02:39,080
modify no let's see what is the address

46
00:02:34,040 --> 00:02:39,080
what is the process ID of this process

47
00:02:39,480 --> 00:02:42,480
go

48
00:02:47,360 --> 00:02:55,379
it's a 966 because we should convert the

49
00:02:52,260 --> 00:02:59,340
process ID to the hexadecimal format

50
00:02:55,379 --> 00:03:03,300
so it's I hope everything is hybrid

51
00:02:59,340 --> 00:03:06,319
region is in xsma format so we're

52
00:03:03,300 --> 00:03:06,319
gonna convert it here

53
00:03:08,519 --> 00:03:15,300
yeah it's equal to this address so I'm

54
00:03:11,459 --> 00:03:17,159
gonna copy it bring it here and create a

55
00:03:15,300 --> 00:03:21,300
simple

56
00:03:17,159 --> 00:03:24,480
book or this command I put a simple 

57
00:03:21,300 --> 00:03:26,400
EPT hook which works on both user mode

58
00:03:24,480 --> 00:03:30,599
and carrier mode and this address is a

59
00:03:26,400 --> 00:03:34,680
user mode address and then I will put a

60
00:03:30,599 --> 00:03:38,000
script and write a simple script which

61
00:03:34,680 --> 00:03:40,980
just tries to put

62
00:03:38,000 --> 00:03:45,360
zero flag to

63
00:03:40,980 --> 00:03:47,459
zero change the execution of course so

64
00:03:45,360 --> 00:03:51,379
let's just connect the HyperDbg in

65
00:03:47,459 --> 00:03:51,379
the local kernel dividing

66
00:03:52,760 --> 00:03:58,440
including the VMM

67
00:03:55,319 --> 00:04:00,540
let's hope that it's connected to the

68
00:03:58,440 --> 00:04:03,959
windy regime

69
00:04:00,540 --> 00:04:06,360
I have to wait to see whether

70
00:04:03,959 --> 00:04:10,739
yeah it's not

71
00:04:06,360 --> 00:04:13,640
yeah it's not connected so the

72
00:04:10,739 --> 00:04:13,640
what happened

73
00:04:13,739 --> 00:04:18,440
yeah

74
00:04:16,019 --> 00:04:18,440
okay

75
00:04:18,479 --> 00:04:24,540
it's now connected and I wanna let's put

76
00:04:22,500 --> 00:04:26,220
it here to show that it's currently

77
00:04:24,540 --> 00:04:29,880
running

78
00:04:26,220 --> 00:04:33,479
I will use this test script to patch the

79
00:04:29,880 --> 00:04:36,660
Z or zero flag

80
00:04:33,479 --> 00:04:39,540
I need and as you can see that it's that

81
00:04:36,660 --> 00:04:42,780
in the target programs change to false

82
00:04:39,540 --> 00:04:45,479
everything here is a change of puzzle we

83
00:04:42,780 --> 00:04:48,840
patch the normal execution execution

84
00:04:45,479 --> 00:04:51,300
flow of this program and the thing is

85
00:04:48,840 --> 00:04:54,660
that you can also many manage difference

86
00:04:51,300 --> 00:04:57,780
like disabling these events the event ID

87
00:04:54,660 --> 00:05:00,500
zero so I want to disable zero and after

88
00:04:57,780 --> 00:05:04,259
disabling it you can see that it's again

89
00:05:00,500 --> 00:05:06,300
change to true the application won't be

90
00:05:04,259 --> 00:05:09,300
notified that it's changed because we

91
00:05:06,300 --> 00:05:12,600
are using the APT hose if I renable it

92
00:05:09,300 --> 00:05:13,979
again you can see that it changed to

93
00:05:12,600 --> 00:05:16,860
false

94
00:05:13,979 --> 00:05:19,259
here are the some innovative ways that

95
00:05:16,860 --> 00:05:21,960
we can use hybrid images to patch the

96
00:05:19,259 --> 00:05:27,860
normal execution flow of the programs

97
00:05:21,960 --> 00:05:27,860
and I think it's enough for this part

