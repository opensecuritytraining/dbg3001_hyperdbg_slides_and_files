1
00:00:00,000 --> 00:00:06,000
here we have some we talk about some

2
00:00:03,780 --> 00:00:08,639
memory considerations some of the things

3
00:00:06,000 --> 00:00:11,519
that you should know before using hyper

4
00:00:08,639 --> 00:00:15,839
dvg especially when whenever you want to

5
00:00:11,519 --> 00:00:17,880
use a script engine there are two terms

6
00:00:15,839 --> 00:00:20,160
that are used in the documentation of

7
00:00:17,880 --> 00:00:23,939
the high producing and the one is safe

8
00:00:20,160 --> 00:00:26,880
and another one is unsafe by safe we

9
00:00:23,939 --> 00:00:29,160
mean something that works all the time

10
00:00:26,880 --> 00:00:30,840
and won't cause the system crash or

11
00:00:29,160 --> 00:00:34,020
system hot

12
00:00:30,840 --> 00:00:37,500
because generally it's so tricky to

13
00:00:34,020 --> 00:00:40,620
manage codes in Vienna extrude mode 

14
00:00:37,500 --> 00:00:42,960
HyperDbg gives us a HyperDbg gives the

15
00:00:40,620 --> 00:00:45,600
user the ability to run the custom

16
00:00:42,960 --> 00:00:48,960
assembly codes in all of the execution

17
00:00:45,600 --> 00:00:51,840
execution modes including ramix root

18
00:00:48,960 --> 00:00:55,739
mode and remix now root mode but

19
00:00:51,840 --> 00:00:58,079
whenever the code is running in VMX root

20
00:00:55,739 --> 00:00:59,879
mode then you should avoid unsafe

21
00:00:58,079 --> 00:01:03,059
behavior because

22
00:00:59,879 --> 00:01:07,159
generally speaking the unsafe behavior

23
00:01:03,059 --> 00:01:10,680
will lead to system instability the

24
00:01:07,159 --> 00:01:14,280
general problem originates from the fact

25
00:01:10,680 --> 00:01:17,640
that whenever the hypervisor just

26
00:01:14,280 --> 00:01:21,180
switches to a VMS root mode then an

27
00:01:17,640 --> 00:01:25,560
interrupts or R flags if bits or

28
00:01:21,180 --> 00:01:28,680
interrupt flag is masked so no

29
00:01:25,560 --> 00:01:30,439
interrupts are allowed in remix root

30
00:01:28,680 --> 00:01:34,439
mode so

31
00:01:30,439 --> 00:01:36,960
because because of that then the

32
00:01:34,439 --> 00:01:41,460
HyperDbg needs some extra efforts

33
00:01:36,960 --> 00:01:44,820
 to handle the memory and there's no

34
00:01:41,460 --> 00:01:48,000
paging paging is not allowed in vme's

35
00:01:44,820 --> 00:01:52,799
throat mode in HyperDbg and in all of

36
00:01:48,000 --> 00:01:55,200
the widgets based softwares because

37
00:01:52,799 --> 00:02:00,479
of that you should not

38
00:01:55,200 --> 00:02:03,180
 access to a page table which is

39
00:02:00,479 --> 00:02:05,640
paged out because the paging is not

40
00:02:03,180 --> 00:02:09,179
allowed so there's nothing to just came

41
00:02:05,640 --> 00:02:13,379
and bring the page the page out page to

42
00:02:09,179 --> 00:02:16,500
the system so we should not access those

43
00:02:13,379 --> 00:02:20,040
pages that are already paged out and

44
00:02:16,500 --> 00:02:23,280
also you should not directly access a

45
00:02:20,040 --> 00:02:26,280
user mode location a user mode

46
00:02:23,280 --> 00:02:28,440
address all of them are should be safely

47
00:02:26,280 --> 00:02:31,080
accessed through some of their routines

48
00:02:28,440 --> 00:02:33,660
that are provided by the HyperDbg for

49
00:02:31,080 --> 00:02:37,680
example by HyperDbg commands or by

50
00:02:33,660 --> 00:02:39,660
the commands of by the by those

51
00:02:37,680 --> 00:02:41,239
functions that are provided in the S

52
00:02:39,660 --> 00:02:46,800
script engine

53
00:02:41,239 --> 00:02:49,680
so HyperDbg also offers a buffer a safe

54
00:02:46,800 --> 00:02:53,459
buffer to you that you might use it and

55
00:02:49,680 --> 00:02:57,060
you might use it as a as a place to save

56
00:02:53,459 --> 00:02:59,580
some ordinary things like some writing

57
00:02:57,060 --> 00:03:01,980
some bytes safely on it but in case if

58
00:02:59,580 --> 00:03:05,519
you want to access a memory you should

59
00:03:01,980 --> 00:03:07,739
 whether use hybrid usage commands or

60
00:03:05,519 --> 00:03:10,019
use some of the functions that are

61
00:03:07,739 --> 00:03:12,000
provided in the script engine this is

62
00:03:10,019 --> 00:03:14,940
the way that HyperDbg handles

63
00:03:12,000 --> 00:03:18,000
unsafe and safe memory buffers

64
00:03:14,940 --> 00:03:21,300
so just keep in mind do not access user

65
00:03:18,000 --> 00:03:25,920
mode memory from considerations need to

66
00:03:21,300 --> 00:03:27,659
be considered and do not access pages

67
00:03:25,920 --> 00:03:29,760
that are already paged out because

68
00:03:27,659 --> 00:03:32,879
paging are disabled paging out not

69
00:03:29,760 --> 00:03:35,700
allowed enough for that and other

70
00:03:32,879 --> 00:03:39,300
things that you might it might might be

71
00:03:35,700 --> 00:03:41,040
interesting to know is this these are

72
00:03:39,300 --> 00:03:43,860
the things that I already talked about

73
00:03:41,040 --> 00:03:46,500
because the whenever because generally

74
00:03:43,860 --> 00:03:49,140
speaking when the page is out it's not

75
00:03:46,500 --> 00:03:51,000
possible to modify or read the memory

76
00:03:49,140 --> 00:03:54,360
addresses there are also some

77
00:03:51,000 --> 00:03:57,599
optimizations in HyperDbg by using

78
00:03:54,360 --> 00:04:02,400
Intel transactional synchronization

79
00:03:57,599 --> 00:04:04,739
extension TSX for a HyperDbg before

80
00:04:02,400 --> 00:04:07,319
HyperDbg wants to access some

81
00:04:04,739 --> 00:04:11,099
memory it checks whether the address is

82
00:04:07,319 --> 00:04:13,739
valid and whether it's safe or not if

83
00:04:11,099 --> 00:04:15,659
the pages just already page out then

84
00:04:13,739 --> 00:04:19,560
HyperDbg won't access is because

85
00:04:15,659 --> 00:04:22,260
accessing that page with a result of a

86
00:04:19,560 --> 00:04:24,060
high pressure VG hub or a VM halt will

87
00:04:22,260 --> 00:04:26,820
happen so hypers which first check

88
00:04:24,060 --> 00:04:29,580
whether the address is valid or not and

89
00:04:26,820 --> 00:04:32,460
after that tries it tries to map it to a

90
00:04:29,580 --> 00:04:35,280
map that physical address to a kernel

91
00:04:32,460 --> 00:04:38,400
valid address which is also valid for

92
00:04:35,280 --> 00:04:42,240
the HyperDbg process to act to be

93
00:04:38,400 --> 00:04:44,940
accessed but we have two methods of

94
00:04:42,240 --> 00:04:48,300
of checking whether the address is valid

95
00:04:44,940 --> 00:04:50,100
or not the first method is by just

96
00:04:48,300 --> 00:04:52,620
traversing through the page table

97
00:04:50,100 --> 00:04:55,020
finding the corresponding page and

98
00:04:52,620 --> 00:04:57,240
checking whether the attributes of the

99
00:04:55,020 --> 00:05:00,120
page or the optional fields of the page

100
00:04:57,240 --> 00:05:02,699
shows whether the pages present or not

101
00:05:00,120 --> 00:05:06,300
this is the first way that hybrid would

102
00:05:02,699 --> 00:05:09,840
you use it uses this approach in most

103
00:05:06,300 --> 00:05:12,120
of the kernel or VMS root

104
00:05:09,840 --> 00:05:14,820
functionalities and also if you are

105
00:05:12,120 --> 00:05:18,419
checking the ad validity I've added

106
00:05:14,820 --> 00:05:21,780
others by using in in the user mode

107
00:05:18,419 --> 00:05:24,180
then HyperDbg uses the TSX because

108
00:05:21,780 --> 00:05:27,060
there are some transactions in the

109
00:05:24,180 --> 00:05:31,199
hybrid transactions for Intel TS6 that

110
00:05:27,060 --> 00:05:33,600
will fail if an address is invalid so we

111
00:05:31,199 --> 00:05:38,220
use the this optimization to make the

112
00:05:33,600 --> 00:05:41,340
memory checks a lot faster and Hyper

113
00:05:38,220 --> 00:05:43,740
if the TS6 is not available in your

114
00:05:41,340 --> 00:05:46,320
system then HyperDbg will automatically

115
00:05:43,740 --> 00:05:49,080
switch to the classic method of checking

116
00:05:46,320 --> 00:05:51,720
addresses which is the traversing

117
00:05:49,080 --> 00:05:55,800
through the page table entries another

118
00:05:51,720 --> 00:05:58,440
thing is pre-allocated pool it's not

119
00:05:55,800 --> 00:06:01,259
allowed because the paging is disabled

120
00:05:58,440 --> 00:06:04,500
it's not allowed or it's not possible to

121
00:06:01,259 --> 00:06:06,600
allocate memory in VMX root mode so you

122
00:06:04,500 --> 00:06:10,500
don't you are not able to allocate

123
00:06:06,600 --> 00:06:12,660
anything any new memory but some of the

124
00:06:10,500 --> 00:06:14,820
functionalities like EPT hooks money

125
00:06:12,660 --> 00:06:19,580
through hooks or if or even the user

126
00:06:14,820 --> 00:06:21,900
here might want some

127
00:06:19,580 --> 00:06:24,120
memories then there are some

128
00:06:21,900 --> 00:06:26,300
pre-allocated memories that are

129
00:06:24,120 --> 00:06:30,620
previously allocated by HyperDbg

130
00:06:26,300 --> 00:06:34,020
are used in the VMX root mode so

131
00:06:30,620 --> 00:06:38,460
pre-allocated pools are allocated and 

132
00:06:34,020 --> 00:06:41,759
if there is no usage for the previously

133
00:06:38,460 --> 00:06:47,280
allocated then they are deallocated and

134
00:06:41,759 --> 00:06:49,080
by defaults the pre-allocated pools

135
00:06:47,280 --> 00:06:51,240
are enough for the regular

136
00:06:49,080 --> 00:06:54,419
functionalities of the high probability

137
00:06:51,240 --> 00:06:56,520
but in case you might encounter some

138
00:06:54,419 --> 00:06:58,620
errors regarding the lack of memory

139
00:06:56,520 --> 00:07:01,340
which it shows you the error hybrid

140
00:06:58,620 --> 00:07:03,800
shows that I don't have enough

141
00:07:01,340 --> 00:07:07,139
pre-allocated memory

142
00:07:03,800 --> 00:07:10,380
in such cases

143
00:07:07,139 --> 00:07:12,960
 you might use period command this

144
00:07:10,380 --> 00:07:15,360
command pre-allocate some of the buffers

145
00:07:12,960 --> 00:07:18,720
whenever it's safe and after that you

146
00:07:15,360 --> 00:07:20,880
you can try again with that assets with

147
00:07:18,720 --> 00:07:24,539
that with that with the previous command

148
00:07:20,880 --> 00:07:27,900
and I know hybrid which you have more

149
00:07:24,539 --> 00:07:30,780
pre-allocated buffers to use it in VM

150
00:07:27,900 --> 00:07:35,460
its root mode in summary of this section

151
00:07:30,780 --> 00:07:37,160
we see that how what are the VT-x and we

152
00:07:35,460 --> 00:07:40,259
see some of the basic or primitive

153
00:07:37,160 --> 00:07:43,259
concepts about page tables and hardware

154
00:07:40,259 --> 00:07:46,680
page table translation then we see

155
00:07:43,259 --> 00:07:48,479
escalate or EPT and after that we saw

156
00:07:46,680 --> 00:07:51,120
some of the commands that are used for

157
00:07:48,479 --> 00:07:53,960
reading and the writings searching and

158
00:07:51,120 --> 00:07:58,020
disassembling physical memory

159
00:07:53,960 --> 00:08:00,360
and we also see how we can convert a

160
00:07:58,020 --> 00:08:02,699
page table from physical virtual and

161
00:08:00,360 --> 00:08:05,120
virtual physical and finally we learn

162
00:08:02,699 --> 00:08:08,220
some of the considerations

163
00:08:05,120 --> 00:08:12,840
that you should make whenever using the

164
00:08:08,220 --> 00:08:17,120
HyperDbg in safe and unsafe States

165
00:08:12,840 --> 00:08:21,180
so before that thanks for watching and

166
00:08:17,120 --> 00:08:23,639
make sure to just run the in source code

167
00:08:21,180 --> 00:08:26,099
the source code after the source code

168
00:08:23,639 --> 00:08:28,699
that are used in this tutorial is also

169
00:08:26,099 --> 00:08:33,800
available on the files

170
00:08:28,699 --> 00:08:33,800
so yeah that's it have a nice day

