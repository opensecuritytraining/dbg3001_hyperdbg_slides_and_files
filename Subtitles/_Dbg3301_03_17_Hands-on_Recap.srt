1
00:00:00,000 --> 00:00:05,940
currently we don't know that much about

2
00:00:02,399 --> 00:00:08,580
HyperDbg divider we just know some very

3
00:00:05,940 --> 00:00:10,860
basic details about the debugger we see

4
00:00:08,580 --> 00:00:12,900
some of the advanced sections 

5
00:00:10,860 --> 00:00:15,839
advanced debugging techniques in the

6
00:00:12,900 --> 00:00:19,740
next sections but based on our

7
00:00:15,839 --> 00:00:23,039
current knowledge about HyperDbg we

8
00:00:19,740 --> 00:00:25,980
could we could do some kind of

9
00:00:23,039 --> 00:00:28,500
examining the systems and and in and

10
00:00:25,980 --> 00:00:31,320
actually in this hands-on I I want to

11
00:00:28,500 --> 00:00:34,020
show you how you can start examining the

12
00:00:31,320 --> 00:00:36,300
targets by using HyperDbg how you can

13
00:00:34,020 --> 00:00:38,820
get some information about the target

14
00:00:36,300 --> 00:00:41,640
and from where you can actually start

15
00:00:38,820 --> 00:00:44,820
your debugging and reversing Journey I'm

16
00:00:41,640 --> 00:00:48,239
just we're gonna start at 32b to get

17
00:00:44,820 --> 00:00:50,820
application a simple network application

18
00:00:48,239 --> 00:00:53,820
then we will try to try Tracy from

19
00:00:50,820 --> 00:00:56,340
carry-on mode to user mode and also we

20
00:00:53,820 --> 00:01:00,000
will see some of the comments that we

21
00:00:56,340 --> 00:01:03,359
that we see in this part so the basic

22
00:01:00,000 --> 00:01:05,519
scenario is like starting from user mode

23
00:01:03,359 --> 00:01:08,760
will find the system column the system

24
00:01:05,519 --> 00:01:10,740
call numbers then we try to trace it 

25
00:01:08,760 --> 00:01:13,560
trace the circuit and the function the

26
00:01:10,740 --> 00:01:15,060
functions and calls and then we try to

27
00:01:13,560 --> 00:01:19,320
investigate and analyze the

28
00:01:15,060 --> 00:01:24,259
functionality so but again I have this

29
00:01:19,320 --> 00:01:24,259
 virtual machine here

30
00:01:25,560 --> 00:01:33,860
restored to my Snapshot and I will also

31
00:01:29,820 --> 00:01:33,860
open a HyperDbg

32
00:01:35,640 --> 00:01:40,939
no no need to

33
00:01:37,500 --> 00:01:40,939
just administrator

34
00:01:41,759 --> 00:01:48,360


35
00:01:42,900 --> 00:01:48,360
I use the debug command 

36
00:01:49,380 --> 00:01:55,619
try to connect to the debugger and of

37
00:01:52,380 --> 00:01:59,399
course again I bring my WinDbg attached

38
00:01:55,619 --> 00:02:01,500
to it because we want to make we want to

39
00:01:59,399 --> 00:02:05,659
run it in the test mode so the driver

40
00:02:01,500 --> 00:02:05,659
signature enforcement will be disabled

41
00:02:14,420 --> 00:02:17,720
that's right

42
00:02:28,319 --> 00:02:35,520
 currency the debugger is

43
00:02:30,959 --> 00:02:38,099
synchronizing the symbol information so

44
00:02:35,520 --> 00:02:40,580
 the first thing that we're gonna test

45
00:02:38,099 --> 00:02:40,580
here

46
00:02:43,680 --> 00:02:51,780
yeah for example currently we are on

47
00:02:48,060 --> 00:02:54,599
the zeroth core because signature shows

48
00:02:51,780 --> 00:02:57,540
that we are in the zero score so

49
00:02:54,599 --> 00:02:58,560
everything will be showed in the zeroed

50
00:02:57,540 --> 00:03:02,580
core

51
00:02:58,560 --> 00:03:03,900
now let's try to switch to the second

52
00:03:02,580 --> 00:03:06,780
core

53
00:03:03,900 --> 00:03:10,560
again here's the second chord it's

54
00:03:06,780 --> 00:03:14,599
operating in the kernel mode

55
00:03:10,560 --> 00:03:17,840
let's see if we can find any course in

56
00:03:14,599 --> 00:03:17,840
user mode

57
00:03:18,360 --> 00:03:24,959
again this core is in

58
00:03:22,620 --> 00:03:27,360
in the same

59
00:03:24,959 --> 00:03:29,940
location and these are in the same

60
00:03:27,360 --> 00:03:32,400
location because currently probably this

61
00:03:29,940 --> 00:03:35,360
course are idle

62
00:03:32,400 --> 00:03:35,360
so

63
00:03:37,680 --> 00:03:43,680
this way we could change the this

64
00:03:41,760 --> 00:03:45,480
could we could change the execution to

65
00:03:43,680 --> 00:03:51,980
other 

66
00:03:45,480 --> 00:03:51,980
course but let's return to the HyperDbg

67
00:03:52,440 --> 00:03:59,459
and continue the debugging

68
00:03:55,200 --> 00:04:02,459
again and I I try to use the batch

69
00:03:59,459 --> 00:04:04,860
process command here

70
00:04:02,459 --> 00:04:09,000
that process command shows the current

71
00:04:04,860 --> 00:04:13,200
 process we are currently in the first

72
00:04:09,000 --> 00:04:15,360
core and the current user mode

73
00:04:13,200 --> 00:04:19,260
process that is running into our system

74
00:04:15,360 --> 00:04:23,120
is HyperDbg CLI if we go to

75
00:04:19,260 --> 00:04:23,120
another core and see

76
00:04:25,020 --> 00:04:33,960
so that process now hybrid Imaging in

77
00:04:28,620 --> 00:04:39,060
the zero zero score is debugging this

78
00:04:33,960 --> 00:04:41,520
application again to another core

79
00:04:39,060 --> 00:04:43,800
yeah this one is currently running in

80
00:04:41,520 --> 00:04:46,100
the user mode because the address is a

81
00:04:43,800 --> 00:04:48,800
user mode address so

82
00:04:46,100 --> 00:04:54,740
it's running in a user mode application

83
00:04:48,800 --> 00:04:54,740
and it's a VM tools VM tools

84
00:04:56,280 --> 00:05:00,320
 foreign

85
00:05:11,340 --> 00:05:16,680
the symbol

86
00:05:13,860 --> 00:05:17,960
 five

87
00:05:16,680 --> 00:05:20,820
yeah

88
00:05:17,960 --> 00:05:24,979
because of that

89
00:05:20,820 --> 00:05:24,979
to use that same path

90
00:05:25,139 --> 00:05:31,860
I previously showed you how to

91
00:05:28,080 --> 00:05:37,639
set the symbols I don't want to explain

92
00:05:31,860 --> 00:05:37,639
it again just try to load them

93
00:05:43,919 --> 00:05:51,740
and now again try the same command to

94
00:05:48,060 --> 00:05:51,740
see the list of processes

95
00:05:51,860 --> 00:05:57,600
here at least that processes that are

96
00:05:55,080 --> 00:06:00,500
currently running on my system if I I

97
00:05:57,600 --> 00:06:04,500
want to switch to one of them

98
00:06:00,500 --> 00:06:07,620
I could easily use their process ID or

99
00:06:04,500 --> 00:06:12,840
their EPROCESS structure for example

100
00:06:07,620 --> 00:06:15,840
let's switch to 

101
00:06:12,840 --> 00:06:18,680
some process that we

102
00:06:15,840 --> 00:06:18,680
know

103
00:06:18,960 --> 00:06:22,919
yeah for example Explorer Explorer is

104
00:06:21,720 --> 00:06:26,060
the window

105
00:06:22,919 --> 00:06:29,100
it is the desktop or the window

106
00:06:26,060 --> 00:06:33,900
for the Windows so the process ID is

107
00:06:29,100 --> 00:06:37,100
this in hexadecimal format so I tried

108
00:06:33,900 --> 00:06:37,100
that 

109
00:06:42,919 --> 00:06:51,000
specified PID

110
00:06:46,699 --> 00:06:53,280
now whenever we press G the the debugger

111
00:06:51,000 --> 00:06:55,620
tries to continue its execution and when

112
00:06:53,280 --> 00:06:59,180
when it reaches to the target process

113
00:06:55,620 --> 00:07:03,120
then it shows but then it gives the

114
00:06:59,180 --> 00:07:04,380
control back to the debugger so if I try

115
00:07:03,120 --> 00:07:06,720
to use

116
00:07:04,380 --> 00:07:10,220
Dutch process command you you can see

117
00:07:06,720 --> 00:07:13,380
that we are currently running in the

118
00:07:10,220 --> 00:07:17,160
explorer.exe in the context of actually

119
00:07:13,380 --> 00:07:19,740
this process so that's how we could use

120
00:07:17,160 --> 00:07:22,259
it actually have options to use that

121
00:07:19,740 --> 00:07:25,500
thread command currency this Thread this

122
00:07:22,259 --> 00:07:29,099
e-tread is running on our system so if I

123
00:07:25,500 --> 00:07:31,639
might just want to see a list of threads

124
00:07:29,099 --> 00:07:31,639


125
00:07:32,039 --> 00:07:39,539
that thread at least for process this

126
00:07:37,319 --> 00:07:42,900
process this Explorer and let's see that

127
00:07:39,539 --> 00:07:46,979
transport this process

128
00:07:42,900 --> 00:07:51,120
yeah and here is a list of threads

129
00:07:46,979 --> 00:07:55,919
that are currently running for

130
00:07:51,120 --> 00:07:58,319
these stats thread for this the

131
00:07:55,919 --> 00:08:01,500
Explorer process

132
00:07:58,319 --> 00:08:03,240
and this this is a list and also hyper

133
00:08:01,500 --> 00:08:05,400
division guarantees that it won't

134
00:08:03,240 --> 00:08:07,860
continue the debugging like everything

135
00:08:05,400 --> 00:08:12,720
is executed in the

136
00:08:07,860 --> 00:08:14,340
 debugger nothing is continued and no

137
00:08:12,720 --> 00:08:16,860
processes get a chance to get it

138
00:08:14,340 --> 00:08:21,740
executed before this command is running

139
00:08:16,860 --> 00:08:25,440
that's it for that processor dot 

140
00:08:21,740 --> 00:08:28,800
and let's see some of the scenarios in

141
00:08:25,440 --> 00:08:30,660
which we we could see some

142
00:08:28,800 --> 00:08:33,899
searches for example if we want to

143
00:08:30,660 --> 00:08:36,300
search for a special pattern let's say

144
00:08:33,899 --> 00:08:40,380
for example this VM call this is the

145
00:08:36,300 --> 00:08:43,500
pattern for VM called zero F zero one

146
00:08:40,380 --> 00:08:45,839
C1 this is an indication of VM calling

147
00:08:43,500 --> 00:08:48,360
instruction there might be other

148
00:08:45,839 --> 00:08:49,880
instructions that start with this

149
00:08:48,360 --> 00:08:53,399
pattern or

150
00:08:49,880 --> 00:08:58,700
has this pattern inside them

151
00:08:53,399 --> 00:08:58,700
but just simply we use LM command

152
00:08:59,220 --> 00:09:08,060
 to create a list of 

153
00:09:03,200 --> 00:09:08,060
debugging create a list of

154
00:09:08,899 --> 00:09:15,060
drivers and the user mode applications

155
00:09:11,459 --> 00:09:20,420
that currently loaded and it just turns

156
00:09:15,060 --> 00:09:20,420
to black because I didn't 

157
00:09:20,820 --> 00:09:26,000
change it for a long time so let's find

158
00:09:26,760 --> 00:09:34,080
the HyperDbg itself this is the process

159
00:09:29,820 --> 00:09:38,120
of a HyperDbg and if I just want to

160
00:09:34,080 --> 00:09:42,779
make it bigger again use the LM command

161
00:09:38,120 --> 00:09:44,640
so I if I want to see the commands 

162
00:09:42,779 --> 00:09:48,240
the the list of

163
00:09:44,640 --> 00:09:51,779
 this list of drivers that start with

164
00:09:48,240 --> 00:09:52,860
high producing let me just use on kernel

165
00:09:51,779 --> 00:09:55,860
mode

166
00:09:52,860 --> 00:09:55,860
reverse

167
00:09:57,380 --> 00:10:06,540
and here we have a high produce HV

168
00:10:01,440 --> 00:10:13,320
aligned with a HyperDbg

169
00:10:06,540 --> 00:10:16,820
 KD let's just search for hybrid HV

170
00:10:13,320 --> 00:10:16,820
and this is the size

171
00:10:17,040 --> 00:10:23,940
 also the VM call pattern is 0f01

172
00:10:22,500 --> 00:10:28,100
C1

173
00:10:23,940 --> 00:10:30,839
so I added here and try to

174
00:10:28,100 --> 00:10:32,700
use the S speed

175
00:10:30,839 --> 00:10:34,560
yeah

176
00:10:32,700 --> 00:10:36,600
so

177
00:10:34,560 --> 00:10:41,540
we'll have some

178
00:10:36,600 --> 00:10:41,540
mistakes no command C Instagram

179
00:10:43,800 --> 00:10:47,700
C1

180
00:10:45,660 --> 00:10:48,779
y60

181
00:10:47,700 --> 00:10:53,339
yeah

182
00:10:48,779 --> 00:10:56,279
so it found two addresses and if I want

183
00:10:53,339 --> 00:10:59,279
to unassemble or disassemble this

184
00:10:56,279 --> 00:11:02,540
addresses we we see that we have these

185
00:10:59,279 --> 00:11:06,540
functionalities so we tries to search

186
00:11:02,540 --> 00:11:09,540
for this VM called instructions all

187
00:11:06,540 --> 00:11:14,060
the VM called instructions in or target

188
00:11:09,540 --> 00:11:14,060
driver these are the addresses

189
00:11:14,180 --> 00:11:20,700
 no let's I I just wrote a very simple

190
00:11:19,200 --> 00:11:24,420


191
00:11:20,700 --> 00:11:24,420
program 

192
00:11:28,100 --> 00:11:35,120
to just simply send some HTTP requests

193
00:11:32,459 --> 00:11:40,260
some simple circuit program

194
00:11:35,120 --> 00:11:42,600
nothing special I I also try I will put

195
00:11:40,260 --> 00:11:44,940
the source code for this program for you

196
00:11:42,600 --> 00:11:49,019
but currently if you want if you want to

197
00:11:44,940 --> 00:11:50,519
 just test it on on the debugger I try

198
00:11:49,019 --> 00:11:53,459
to

199
00:11:50,519 --> 00:11:56,420
move the executable file to the target

200
00:11:53,459 --> 00:11:56,420
VM

201
00:12:13,800 --> 00:12:19,200
okay

202
00:12:15,380 --> 00:12:22,560
 this this is a really simple

203
00:12:19,200 --> 00:12:24,320
function the simple last version of

204
00:12:22,560 --> 00:12:28,440
the circuit

205
00:12:24,320 --> 00:12:34,079
function in the Windows

206
00:12:28,440 --> 00:12:38,640
some scenarios here I use that I used

207
00:12:34,079 --> 00:12:41,700
a debug break here because I I just

208
00:12:38,640 --> 00:12:44,600
want to try to find some some ways of

209
00:12:41,700 --> 00:12:49,019
examining how receiving and

210
00:12:44,600 --> 00:12:52,440
over TCP works on Windows so I try to

211
00:12:49,019 --> 00:12:56,760
simply create this project

212
00:12:52,440 --> 00:12:59,579
 I specify one IP address that that is

213
00:12:56,760 --> 00:13:01,200
valid on my current system for example

214
00:12:59,579 --> 00:13:07,940
if

215
00:13:01,200 --> 00:13:07,940
you go on this target IP then

216
00:13:08,940 --> 00:13:12,139
we'll see

217
00:13:12,600 --> 00:13:17,579
there's a simple IIs server I installed

218
00:13:15,540 --> 00:13:19,680
on my system you can use whatever IP

219
00:13:17,579 --> 00:13:23,519
address that you want

220
00:13:19,680 --> 00:13:27,180
even a valid a web IP address is also

221
00:13:23,519 --> 00:13:30,000
okay the Haas or the header for the HTTP

222
00:13:27,180 --> 00:13:32,540
is also not important but the thing here

223
00:13:30,000 --> 00:13:36,720
is that I try to

224
00:13:32,540 --> 00:13:39,420
run this application and if there was

225
00:13:36,720 --> 00:13:42,899
some errors here we will try it we will

226
00:13:39,420 --> 00:13:44,880
try again we will just try to run two

227
00:13:42,899 --> 00:13:49,260
times I will tell you why you need to

228
00:13:44,880 --> 00:13:51,560
run it two times so

229
00:13:49,260 --> 00:13:55,139
let's just

230
00:13:51,560 --> 00:13:58,560
run this application while hybrid which

231
00:13:55,139 --> 00:14:01,940
is also running in the background

232
00:13:58,560 --> 00:14:04,800
I try to run it and when this process

233
00:14:01,940 --> 00:14:08,579
wants to send some data

234
00:14:04,800 --> 00:14:10,440
 to the system hyper individually

235
00:14:08,579 --> 00:14:13,339
intercepted because there's a break

236
00:14:10,440 --> 00:14:16,560
point here or a zero it's

237
00:14:13,339 --> 00:14:20,160
CC 

238
00:14:16,560 --> 00:14:24,120
instruction is located here

239
00:14:20,160 --> 00:14:28,139
so I will try to execute it to see how I

240
00:14:24,120 --> 00:14:30,300
want to just see how sample sending

241
00:14:28,139 --> 00:14:33,060
data and receiving data works and

242
00:14:30,300 --> 00:14:36,959
Windows so I opened it and as you can

243
00:14:33,060 --> 00:14:39,959
see here and the breakpoint is executed

244
00:14:36,959 --> 00:14:42,560
and we are here in the middle of this

245
00:14:39,959 --> 00:14:42,560
application

246
00:14:44,220 --> 00:14:47,540


247
00:14:45,240 --> 00:14:51,240
and if I try to just

248
00:14:47,540 --> 00:14:57,899
go through the instructions like I want

249
00:14:51,240 --> 00:15:01,019
to execute 100 of instructions

250
00:14:57,899 --> 00:15:02,959
 to see how it works actually you can

251
00:15:01,019 --> 00:15:07,160
see that

252
00:15:02,959 --> 00:15:07,160
something is happening here

253
00:15:09,560 --> 00:15:16,440
after executing some instructions in the

254
00:15:14,100 --> 00:15:20,459
user mode these are user mode addresses

255
00:15:16,440 --> 00:15:22,380
hybrid ug goes to the kernel mode but

256
00:15:20,459 --> 00:15:27,320


257
00:15:22,380 --> 00:15:27,320
he tries to handle some page faults

258
00:15:27,660 --> 00:15:37,199
so whenever we go to this function 

259
00:15:32,240 --> 00:15:39,899
WS2 underline 32 dll Dan hyperduity

260
00:15:37,199 --> 00:15:42,360
tries to go to the key for a key page

261
00:15:39,899 --> 00:15:45,300
fault function whenever you encounter

262
00:15:42,360 --> 00:15:49,380
these scenarios where HyperDbg will go

263
00:15:45,300 --> 00:15:51,420
to the page fault handler this is simply

264
00:15:49,380 --> 00:15:54,120
because of some some of the mechanisms

265
00:15:51,420 --> 00:15:58,320
in Windows for example a mechanism in

266
00:15:54,120 --> 00:16:01,139
Windows just tries to side load some

267
00:15:58,320 --> 00:16:04,740
deals some deals are not really loaded

268
00:16:01,139 --> 00:16:08,100
until someone tries to use them so

269
00:16:04,740 --> 00:16:10,019
Windows tries to handle it by invoking

270
00:16:08,100 --> 00:16:12,480
some page faults for example the

271
00:16:10,019 --> 00:16:15,839
whenever the first access is made to the

272
00:16:12,480 --> 00:16:18,360
target deal then a page file with will

273
00:16:15,839 --> 00:16:20,940
occur and

274
00:16:18,360 --> 00:16:23,820
and HyperDbg on the other hand will

275
00:16:20,940 --> 00:16:25,800
directly go to the page Vault handle but

276
00:16:23,820 --> 00:16:29,940
almost certainly we are not interested

277
00:16:25,800 --> 00:16:33,440
in the page fault handler so let's just

278
00:16:29,940 --> 00:16:39,500
try to continue or target system

279
00:16:33,440 --> 00:16:39,500
and another thing about these

280
00:16:39,899 --> 00:16:48,959
process or this program is that I try

281
00:16:45,120 --> 00:16:51,660
I try to write two times the first time

282
00:16:48,959 --> 00:16:56,220
and for the first time when when I run

283
00:16:51,660 --> 00:17:00,600
it it the Windows first tries to load

284
00:16:56,220 --> 00:17:06,720
the functionality or the dll file or the

285
00:17:00,600 --> 00:17:09,360
or ring the data for this sender to the

286
00:17:06,720 --> 00:17:12,120
actual process so some page fault will

287
00:17:09,360 --> 00:17:14,959
happen and after that I will try to run

288
00:17:12,120 --> 00:17:19,559
this

289
00:17:14,959 --> 00:17:24,480
function again two times and in the

290
00:17:19,559 --> 00:17:28,199
second time we won't see any page

291
00:17:24,480 --> 00:17:30,720
faults like no page fault happens

292
00:17:28,199 --> 00:17:32,280
because we previously handled the

293
00:17:30,720 --> 00:17:34,380
page fault so we will have two

294
00:17:32,280 --> 00:17:36,539
breakpoints this is a really important

295
00:17:34,380 --> 00:17:39,900
note to consider when you are using

296
00:17:36,539 --> 00:17:44,400
hyper Division and you're just

297
00:17:39,900 --> 00:17:47,039
running on a page fault handler files a

298
00:17:44,400 --> 00:17:49,640
page file handler function so I try to

299
00:17:47,039 --> 00:17:49,640
run it again

300
00:17:54,539 --> 00:17:59,820
and the first query point is not

301
00:17:57,419 --> 00:18:03,299
important for us because it will

302
00:17:59,820 --> 00:18:05,460
eventually go to page 1200 and the

303
00:18:03,299 --> 00:18:09,059
second breakpoint when when the second

304
00:18:05,460 --> 00:18:12,299
break point triggers then we could start

305
00:18:09,059 --> 00:18:15,480
 instrumenting instruction to see how

306
00:18:12,299 --> 00:18:18,620
how this sending mechanism actually

307
00:18:15,480 --> 00:18:18,620
works on Windows

308
00:18:19,140 --> 00:18:28,140
yeah here we see that it tries to run

309
00:18:23,460 --> 00:18:33,140
some functions like kernel 32 TLS get

310
00:18:28,140 --> 00:18:36,960
value or other functions like this WS

311
00:18:33,140 --> 00:18:38,059
232 send function and after that it will

312
00:18:36,960 --> 00:18:41,900
go

313
00:18:38,059 --> 00:18:45,840
to the kernels so

314
00:18:41,900 --> 00:18:49,860
 I don't know about the system call 

315
00:18:45,840 --> 00:18:51,440
that is designed for sending a

316
00:18:49,860 --> 00:18:54,900
requests to the

317
00:18:51,440 --> 00:18:58,380
terminals of sending a network request

318
00:18:54,900 --> 00:19:01,320
or sending TCP or UDP request so we will

319
00:18:58,380 --> 00:19:05,160
just try to investigate how it works

320
00:19:01,320 --> 00:19:09,660
but as we can see here it eventually

321
00:19:05,160 --> 00:19:13,860
calls this HDL function and this this is

322
00:19:09,660 --> 00:19:17,000
this is probably this is the function

323
00:19:13,860 --> 00:19:21,960
that that is responsible for sending

324
00:19:17,000 --> 00:19:26,280
data or over TCP or over other over the

325
00:19:21,960 --> 00:19:30,419
network so NT device IO control file

326
00:19:26,280 --> 00:19:34,500
will try to search it on internet and

327
00:19:30,419 --> 00:19:38,480
or as you can see here's the definition

328
00:19:34,500 --> 00:19:42,299
of the this functions actually

329
00:19:38,480 --> 00:19:45,780
deprecated but currency Windows you use

330
00:19:42,299 --> 00:19:48,419
it for sending data so I I just try to

331
00:19:45,780 --> 00:19:51,740
figure out what's the system call number

332
00:19:48,419 --> 00:19:51,740
for this

333
00:19:52,080 --> 00:19:57,720


334
00:19:54,080 --> 00:20:01,799
this system call so the system call

335
00:19:57,720 --> 00:20:04,740
numbers always in RX registers I try to

336
00:20:01,799 --> 00:20:08,240
just write a little bit before

337
00:20:04,740 --> 00:20:10,559
because whenever we reach to this 

338
00:20:08,240 --> 00:20:15,600
stage 

339
00:20:10,559 --> 00:20:18,299
r a e x register let let's see if RX

340
00:20:15,600 --> 00:20:21,440
register is modified or not

341
00:20:18,299 --> 00:20:25,700
RX contains the

342
00:20:21,440 --> 00:20:25,700
target system call number

343
00:20:25,980 --> 00:20:33,020
no it's not modified so yeah so if I

344
00:20:30,240 --> 00:20:36,780
just press R and see there

345
00:20:33,020 --> 00:20:40,320
registers we could easily see that 

346
00:20:36,780 --> 00:20:44,039
this is the system call number for NT

347
00:20:40,320 --> 00:20:47,640
device IO control file

348
00:20:44,039 --> 00:20:50,400
and also these are the parameters that

349
00:20:47,640 --> 00:20:55,220
are passed to this function let's try to

350
00:20:50,400 --> 00:20:55,220
run it again and instead of 

351
00:20:57,080 --> 00:21:04,020
 seeing the system call number we want

352
00:21:01,260 --> 00:21:07,760
to see the actual parameters that are

353
00:21:04,020 --> 00:21:10,740
passed to this target to this NT device

354
00:21:07,760 --> 00:21:16,440
control file

355
00:21:10,740 --> 00:21:17,960
i IO control file so I try to continue

356
00:21:16,440 --> 00:21:21,299
and

357
00:21:17,960 --> 00:21:23,820
we see that and we reach to the same

358
00:21:21,299 --> 00:21:25,380
breakpoint again and this time I will

359
00:21:23,820 --> 00:21:29,039
try to run

360
00:21:25,380 --> 00:21:31,640
like F0 this is in hexadecimal format so

361
00:21:29,039 --> 00:21:35,340
I try to run F0

362
00:21:31,640 --> 00:21:39,260
number of instructions

363
00:21:35,340 --> 00:21:43,799
yeah again we are still in the user mode

364
00:21:39,260 --> 00:21:46,860
 we are not in the kernel mode so I

365
00:21:43,799 --> 00:21:49,940
try to just run the instructions one by

366
00:21:46,860 --> 00:21:49,940
one to see

367
00:21:52,440 --> 00:21:55,860
yeah

368
00:21:53,580 --> 00:21:59,240
 currently I did the next instruction

369
00:21:55,860 --> 00:22:02,100
that will be executed is syscall

370
00:21:59,240 --> 00:22:02,880
instructions so at this point we should

371
00:22:02,100 --> 00:22:04,380
have

372
00:22:02,880 --> 00:22:08,159
parameters

373
00:22:04,380 --> 00:22:11,039
because the in system call the

374
00:22:08,159 --> 00:22:14,100
parameters are passed in fast call

375
00:22:11,039 --> 00:22:16,820
formats so the first parameter

376
00:22:14,100 --> 00:22:16,820
here

377
00:22:18,620 --> 00:22:26,700
 the first parameter is the the RX

378
00:22:24,419 --> 00:22:29,900
register campaigns that

379
00:22:26,700 --> 00:22:32,520
system call number

380
00:22:29,900 --> 00:22:35,580
the second

381
00:22:32,520 --> 00:22:38,100
the second the first parameter is passed

382
00:22:35,580 --> 00:22:40,140
in rcx register so this is the file

383
00:22:38,100 --> 00:22:43,500
handle the actual file handle that is

384
00:22:40,140 --> 00:22:47,460
used by this process for this

385
00:22:43,500 --> 00:22:51,659
function the second one is the event

386
00:22:47,460 --> 00:22:56,460
handle which is passed on RDS register

387
00:22:51,659 --> 00:22:58,640
and R H is also used for the APC routine

388
00:22:56,460 --> 00:23:01,559
or the third

389
00:22:58,640 --> 00:23:05,940
parameter and the fourth parameter is

390
00:23:01,559 --> 00:23:08,700
also zero r9 which is also APC context

391
00:23:05,940 --> 00:23:11,640
so this is and and other

392
00:23:08,700 --> 00:23:14,220
parameters are also passing a stack for

393
00:23:11,640 --> 00:23:17,940
example we could easily see the input

394
00:23:14,220 --> 00:23:20,039
buffer that this process prompts to send

395
00:23:17,940 --> 00:23:23,580
to the 

396
00:23:20,039 --> 00:23:27,240
target program for example if I just

397
00:23:23,580 --> 00:23:30,120
want to see the stack for this process

398
00:23:27,240 --> 00:23:33,059
let's just see the stack like I use the

399
00:23:30,120 --> 00:23:38,419
DQ instruction

400
00:23:33,059 --> 00:23:41,580
and use the rsp or the S10

401
00:23:38,419 --> 00:23:44,600
there's some 

402
00:23:41,580 --> 00:23:48,380
special stack pointers I think

403
00:23:44,600 --> 00:23:52,380
for fast complete it starts from

404
00:23:48,380 --> 00:23:56,539
28 inch hexadecimal format so just let's

405
00:23:52,380 --> 00:24:00,720
just try to see if we are in the target

406
00:23:56,539 --> 00:24:03,440
 find the target parameters this one

407
00:24:00,720 --> 00:24:06,080
is the U-Line

408
00:24:03,440 --> 00:24:09,000
this one's a Uline

409
00:24:06,080 --> 00:24:11,659
parameters so we could see that the

410
00:24:09,000 --> 00:24:18,980
actual 

411
00:24:11,659 --> 00:24:18,980
IOCTL or IOCTL IO control code that 

412
00:24:19,640 --> 00:24:24,860
this IO control code that passed to

413
00:24:23,220 --> 00:24:28,940
this

414
00:24:24,860 --> 00:24:32,299
anti-device IO control file is actually

415
00:24:28,940 --> 00:24:32,299
this value

416
00:24:34,700 --> 00:24:42,380
if we just want to see the input buffer

417
00:24:38,520 --> 00:24:42,380
then we have to add

418
00:24:42,780 --> 00:24:48,720
eight 

419
00:24:45,600 --> 00:24:50,340
bytes to this stack

420
00:24:48,720 --> 00:24:54,539
so

421
00:24:50,340 --> 00:24:58,260
we will get some addresses like this

422
00:24:54,539 --> 00:25:01,700
let's just try to like search a little

423
00:24:58,260 --> 00:25:04,980
bit about the target address and

424
00:25:01,700 --> 00:25:10,740
you might reach to some

425
00:25:04,980 --> 00:25:13,320
good hints of how to start like how

426
00:25:10,740 --> 00:25:16,080
we can start

427
00:25:13,320 --> 00:25:18,960
 examining the target that that that's

428
00:25:16,080 --> 00:25:22,200
enough let's just try to I think this

429
00:25:18,960 --> 00:25:24,179
is enough for understanding how to work

430
00:25:22,200 --> 00:25:28,200
with high producer and how you can start

431
00:25:24,179 --> 00:25:31,500
your debugging but another thing that

432
00:25:28,200 --> 00:25:34,140
I want to mention here is how we can use

433
00:25:31,500 --> 00:25:36,980
the tracking commands of the HyperDbg

434
00:25:34,140 --> 00:25:39,240
to just get an idea how this

435
00:25:36,980 --> 00:25:41,460
functionality actually works in the

436
00:25:39,240 --> 00:25:45,679
target operating system so I try to run

437
00:25:41,460 --> 00:25:50,539
the target symbol stand file again

438
00:25:45,679 --> 00:25:50,539
we got to the second

439
00:25:50,940 --> 00:25:58,320
break point and I will use that I I will

440
00:25:54,840 --> 00:26:02,100
use track command 

441
00:25:58,320 --> 00:26:05,539
as you can see this command tries to

442
00:26:02,100 --> 00:26:08,640
on the instructions and create a list of

443
00:26:05,539 --> 00:26:10,880
functions that are called during the

444
00:26:08,640 --> 00:26:15,360
execution you can always

445
00:26:10,880 --> 00:26:19,380
finish the execution or part or stop the

446
00:26:15,360 --> 00:26:22,020
execution by pressing Ctrl C but let it

447
00:26:19,380 --> 00:26:26,760
work for some times to create a list of

448
00:26:22,020 --> 00:26:29,640
functions that are called then this

449
00:26:26,760 --> 00:26:32,940
functionality or when sending

450
00:26:29,640 --> 00:26:35,360
some data the kernel

451
00:26:32,940 --> 00:26:39,840
is invoked from the user

452
00:26:35,360 --> 00:26:43,200
as you can see here first these two

453
00:26:39,840 --> 00:26:46,140
font this function is called and after

454
00:26:43,200 --> 00:26:48,299
that it returns in this address and I

455
00:26:46,140 --> 00:26:51,299
produce you don't know what are these

456
00:26:48,299 --> 00:26:54,900
addresses because probably HyperDbg

457
00:26:51,299 --> 00:26:56,340
doesn't have the symbol file for this

458
00:26:54,900 --> 00:26:59,640
address

459
00:26:56,340 --> 00:27:02,400
well and after that there are also other

460
00:26:59,640 --> 00:27:04,980
functions in this stage the HyperDbg

461
00:27:02,400 --> 00:27:07,919
transfer the execution from user mode to

462
00:27:04,980 --> 00:27:12,419
the kernel mode as we can see here first

463
00:27:07,919 --> 00:27:14,059
NP IOP X6 X control file is called

464
00:27:12,419 --> 00:27:18,480
and after that

465
00:27:14,059 --> 00:27:20,820
these functions are called

466
00:27:18,480 --> 00:27:23,880
 so

467
00:27:20,820 --> 00:27:27,020
again 

468
00:27:23,880 --> 00:27:32,760
in in this special function

469
00:27:27,020 --> 00:27:36,059
 anti OB reference object by handle

470
00:27:32,760 --> 00:27:39,360
which is located at this address this

471
00:27:36,059 --> 00:27:41,640
function is called and after that as you

472
00:27:39,360 --> 00:27:43,559
can see we can follow it here this

473
00:27:41,640 --> 00:27:45,840
function is also returned at this

474
00:27:43,559 --> 00:27:48,120
address so this is the return address of

475
00:27:45,840 --> 00:27:50,960
this function called and this function

476
00:27:48,120 --> 00:27:54,360
is also called and in this function

477
00:27:50,960 --> 00:27:56,760
these functions are also called

478
00:27:54,360 --> 00:27:58,980
recursively like these these

479
00:27:56,760 --> 00:28:02,279
functions are called inside this

480
00:27:58,980 --> 00:28:04,919
function you can see after this function

481
00:28:02,279 --> 00:28:08,279
didn't execute any function

482
00:28:04,919 --> 00:28:11,400
it just Returns on this address and

483
00:28:08,279 --> 00:28:15,380
again this function didn't execute

484
00:28:11,400 --> 00:28:15,380
didn't call any function

485
00:28:15,419 --> 00:28:20,880
and yeah I think you've got the idea you

486
00:28:18,059 --> 00:28:24,120
can try to save these results for

487
00:28:20,880 --> 00:28:26,760
furthering investigation

488
00:28:24,120 --> 00:28:30,419
 high producer will continue to create

489
00:28:26,760 --> 00:28:34,679
some results for you so you can use them

490
00:28:30,419 --> 00:28:37,440
to examine the target and you you can

491
00:28:34,679 --> 00:28:40,080
start your reversing Journey from here

492
00:28:37,440 --> 00:28:44,600
right now we don't know that much about

493
00:28:40,080 --> 00:28:47,640
the HyperDbg debugger in the future

494
00:28:44,600 --> 00:28:49,799
sections we will know a lot about the

495
00:28:47,640 --> 00:28:53,640
advanced Techniques that are used by

496
00:28:49,799 --> 00:28:56,220
HyperDbg or how you can use main

497
00:28:53,640 --> 00:28:59,120
features of the HyperDbg but this is

498
00:28:56,220 --> 00:28:59,120
just a starting point

