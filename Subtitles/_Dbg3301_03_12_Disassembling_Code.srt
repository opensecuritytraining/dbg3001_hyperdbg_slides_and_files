1
00:00:00,199 --> 00:00:10,620
know about disassembling codes we can

2
00:00:04,860 --> 00:00:14,460
use a new command if we want to use if

3
00:00:10,620 --> 00:00:20,039
you want to disassemble the running

4
00:00:14,460 --> 00:00:23,880
codes HyperDbg uses the IDS as it's the

5
00:00:20,039 --> 00:00:23,880
main disassembler

6
00:00:24,000 --> 00:00:33,480
and there's also another command which

7
00:00:26,939 --> 00:00:37,980
is called U2 I will explain about its

8
00:00:33,480 --> 00:00:41,820
as you can see we can here see how what

9
00:00:37,980 --> 00:00:45,780
is the disassemble quotes for 

10
00:00:41,820 --> 00:00:48,059
aex allocated with tag and also I can

11
00:00:45,780 --> 00:00:50,160
specify the length for example I want to

12
00:00:48,059 --> 00:00:54,680
see

13
00:00:50,160 --> 00:00:56,699
 50 bytes of this

14
00:00:54,680 --> 00:01:00,420
data or

15
00:00:56,699 --> 00:01:04,519
maybe even bigger everything in

16
00:01:00,420 --> 00:01:09,000
hybrid uses in hex formats so

17
00:01:04,519 --> 00:01:12,659
this number means it's means in hex

18
00:01:09,000 --> 00:01:17,159
format interpreted as the hex and we can

19
00:01:12,659 --> 00:01:18,619
see that after this function which is

20
00:01:17,159 --> 00:01:20,580
called

21
00:01:18,619 --> 00:01:23,280
tag here

22
00:01:20,580 --> 00:01:26,640
 there is another function which is

23
00:01:23,280 --> 00:01:30,960
called exp whole flag to pull type

24
00:01:26,640 --> 00:01:33,060
another command is also U2 if you want

25
00:01:30,960 --> 00:01:34,560
to force hybrid user hybrid which by

26
00:01:33,060 --> 00:01:37,799
default detects whether you are running

27
00:01:34,560 --> 00:01:39,540
in a 32-bit environment or 64-bit

28
00:01:37,799 --> 00:01:42,840
environment but if you want to force

29
00:01:39,540 --> 00:01:45,360
hybrid activity to use a thirst qubit as

30
00:01:42,840 --> 00:01:47,720
a disassembler then you can use U to

31
00:01:45,360 --> 00:01:47,720
come up

