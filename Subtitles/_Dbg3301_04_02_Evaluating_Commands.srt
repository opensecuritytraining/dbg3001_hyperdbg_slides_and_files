1
00:00:00,000 --> 00:00:06,240
it sees the evaluating commands

2
00:00:03,179 --> 00:00:10,320
evaluation command in the hybrid WG like

3
00:00:06,240 --> 00:00:14,059
mini VGA HyperDbg uses the

4
00:00:10,320 --> 00:00:18,000
question mark as a command for

5
00:00:14,059 --> 00:00:20,820
evaluating script commands in the script

6
00:00:18,000 --> 00:00:24,000
engine for example if you use a question

7
00:00:20,820 --> 00:00:26,699
mark you can write your scripts you can

8
00:00:24,000 --> 00:00:30,840
write multiple scripts in the multiple

9
00:00:26,699 --> 00:00:33,780
lines and this is where we can use 

10
00:00:30,840 --> 00:00:35,940
the you can use some scripts that are

11
00:00:33,780 --> 00:00:39,540
evaluating in the SQ page I will explain

12
00:00:35,940 --> 00:00:42,719
it now I don't confuse it too much it's

13
00:00:39,540 --> 00:00:45,360
for example if you just want to print a

14
00:00:42,719 --> 00:00:48,600
simple hello world by using the printf

15
00:00:45,360 --> 00:00:52,559
function and you will use the question

16
00:00:48,600 --> 00:00:55,820
mark and a simple printf that that is

17
00:00:52,559 --> 00:00:59,219
exactly working like C language

18
00:00:55,820 --> 00:01:01,980
just the prints the hello world in the

19
00:00:59,219 --> 00:01:06,540
comment debugger or if you want to

20
00:01:01,980 --> 00:01:09,840
assign a value to a register for

21
00:01:06,540 --> 00:01:12,299
example if we want to change the Rex

22
00:01:09,840 --> 00:01:15,439
register will use this command which

23
00:01:12,299 --> 00:01:20,340
just assigns the

24
00:01:15,439 --> 00:01:22,740
0x1234 b e e f to the Rex register if

25
00:01:20,340 --> 00:01:24,360
there are any variables I will talk

26
00:01:22,740 --> 00:01:26,520
about the variables in a script engine

27
00:01:24,360 --> 00:01:29,520
but if there are any variables you can

28
00:01:26,520 --> 00:01:32,400
just simply assign them by using the

29
00:01:29,520 --> 00:01:36,119
name of the variable and then use the

30
00:01:32,400 --> 00:01:39,119
you simply assign it to an expression or

31
00:01:36,119 --> 00:01:42,119
on a static value there are also other

32
00:01:39,119 --> 00:01:46,439
possibilities to modify for example the

33
00:01:42,119 --> 00:01:50,640
flags if you want to simply modify the

34
00:01:46,439 --> 00:01:54,000
zero flag or R flax z f flag you can

35
00:01:50,640 --> 00:01:57,200
simply use it as you can simply assign a

36
00:01:54,000 --> 00:01:59,899
bit to it and whenever this is really

37
00:01:57,200 --> 00:02:04,079
important if you want to use a

38
00:01:59,899 --> 00:02:06,960
multi-line script so we want to use a

39
00:02:04,079 --> 00:02:08,700
description a multi-line format or some

40
00:02:06,960 --> 00:02:12,480
have some some multiple lines of a

41
00:02:08,700 --> 00:02:15,660
script then we use this curly brackets

42
00:02:12,480 --> 00:02:17,780
put or escape between these two purely

43
00:02:15,660 --> 00:02:17,780
back

