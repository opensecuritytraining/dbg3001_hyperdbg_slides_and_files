1
00:00:00,359 --> 00:00:07,280
let's talk about user process user debug

2
00:00:04,080 --> 00:00:10,320
and how we can use the user debugger

3
00:00:07,280 --> 00:00:12,660
user mode application debugger

4
00:00:10,320 --> 00:00:15,599
there are some commands like data start

5
00:00:12,660 --> 00:00:17,460
or Dutch restart these commands work on

6
00:00:15,599 --> 00:00:19,199
both me and my mode and the debugger

7
00:00:17,460 --> 00:00:21,779
mode and there are two scenarios to

8
00:00:19,199 --> 00:00:25,320
attach to a process first we can use the

9
00:00:21,779 --> 00:00:28,800
dot start command to start the process

10
00:00:25,320 --> 00:00:30,599
and second we can use the dot

11
00:00:28,800 --> 00:00:31,920
attach command to attach to a running

12
00:00:30,599 --> 00:00:34,680
process

13
00:00:31,920 --> 00:00:38,340
and there are also other commands like

14
00:00:34,680 --> 00:00:41,879
Dash restart and dash code which help us

15
00:00:38,340 --> 00:00:46,379
which will help us to run and rerun or

16
00:00:41,879 --> 00:00:50,879
terminate the target user mode program

17
00:00:46,379 --> 00:00:53,399
as the first example here we have and

18
00:00:50,879 --> 00:00:55,739
we want to start a new user mode

19
00:00:53,399 --> 00:00:59,340
application and we can use the data

20
00:00:55,739 --> 00:01:03,840
start command and this command gets the

21
00:00:59,340 --> 00:01:08,880
path of the target executable file

22
00:01:03,840 --> 00:01:12,180
after that we can pass the parameters to

23
00:01:08,880 --> 00:01:15,119
the target process we can specify the

24
00:01:12,180 --> 00:01:17,880
parameters and you should note that the

25
00:01:15,119 --> 00:01:20,040
target pass should be valid in the

26
00:01:17,880 --> 00:01:21,780
debuggies operating system not in the

27
00:01:20,040 --> 00:01:24,180
current house so the file should be

28
00:01:21,780 --> 00:01:25,560
located in the target VM or in the

29
00:01:24,180 --> 00:01:28,740
target device

30
00:01:25,560 --> 00:01:31,860
let's see some examples about it I

31
00:01:28,740 --> 00:01:34,680
already connected to this instance of

32
00:01:31,860 --> 00:01:37,680
her mirror

33
00:01:34,680 --> 00:01:37,680


34
00:01:38,700 --> 00:01:47,640
let's start for example a calculator the

35
00:01:43,259 --> 00:01:49,740
calculator is located at C Windows 

36
00:01:47,640 --> 00:01:52,740
system 32

37
00:01:49,740 --> 00:01:52,740
account.exe

38
00:01:53,520 --> 00:01:59,659
and as you can see we reached

39
00:01:56,159 --> 00:02:05,100
somewhere in the ntdl

40
00:01:59,659 --> 00:02:07,439
and once we press G we will end up in

41
00:02:05,100 --> 00:02:09,899
running the first instruction of the

42
00:02:07,439 --> 00:02:11,420
entry point no we are in the entry point

43
00:02:09,899 --> 00:02:14,760
as the module

44
00:02:11,420 --> 00:02:18,360
but the difference between this command

45
00:02:14,760 --> 00:02:20,459
and how is comma how this command works

46
00:02:18,360 --> 00:02:23,160
and the difference between

47
00:02:20,459 --> 00:02:25,440
implementation of this command in hybrid

48
00:02:23,160 --> 00:02:26,280
imagery

49
00:02:25,440 --> 00:02:29,420


50
00:02:26,280 --> 00:02:34,560
and other debuggers is that this command

51
00:02:29,420 --> 00:02:37,440
doesn't use any Windows API or any

52
00:02:34,560 --> 00:02:41,040
debugging flags for starting the process

53
00:02:37,440 --> 00:02:43,920
so the target process doesn't know

54
00:02:41,040 --> 00:02:46,319
that it's currently being debugged by

55
00:02:43,920 --> 00:02:49,140
the debugger because everything is run

56
00:02:46,319 --> 00:02:53,640
normally and it's purely implemented in

57
00:02:49,140 --> 00:02:57,300
the hypervisor so no Windows API is

58
00:02:53,640 --> 00:03:00,000
used for starting no Windows API is

59
00:02:57,300 --> 00:03:02,660
used for a start the process in the

60
00:03:00,000 --> 00:03:02,660
debug mode

61
00:03:02,700 --> 00:03:08,400
after that I continue running it and we

62
00:03:06,060 --> 00:03:12,540
can see here that the calculator is

63
00:03:08,400 --> 00:03:17,480
started Let's test another executable I

64
00:03:12,540 --> 00:03:17,480
copy the path of this executable

65
00:03:17,760 --> 00:03:23,300
again I use data start path 

66
00:03:24,959 --> 00:03:32,159
and we reach to the entry point of this

67
00:03:28,379 --> 00:03:33,060
process so here we are in in the entry

68
00:03:32,159 --> 00:03:35,819
point

69
00:03:33,060 --> 00:03:37,620
you can see that the name of the process

70
00:03:35,819 --> 00:03:40,319
is hello world

71
00:03:37,620 --> 00:03:45,000
 let's continue writing it

72
00:03:40,319 --> 00:03:49,319
no we can run some commands like dot

73
00:03:45,000 --> 00:03:53,099
restart command to again restart the

74
00:03:49,319 --> 00:03:55,560
target executable and again we reached

75
00:03:53,099 --> 00:03:59,459
the entry point of the measure after

76
00:03:55,560 --> 00:04:02,640
that it runs normally or we can also use

77
00:03:59,459 --> 00:04:05,299
the doctor command to kill the target

78
00:04:02,640 --> 00:04:05,299
process

