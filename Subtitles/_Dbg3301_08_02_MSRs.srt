1
00:00:00,000 --> 00:00:08,340
now let's talk a little bit about 

2
00:00:04,380 --> 00:00:11,580
control model about controlling model

3
00:00:08,340 --> 00:00:15,900
specific register or MSR registers how

4
00:00:11,580 --> 00:00:19,320
we can do that in hybrid imagery if

5
00:00:15,900 --> 00:00:22,740
you want to just simply change the some

6
00:00:19,320 --> 00:00:25,080
of the MSR registers there are some

7
00:00:22,740 --> 00:00:27,300
commands for it but first of all we have

8
00:00:25,080 --> 00:00:30,180
to know what are the MSR registers you

9
00:00:27,300 --> 00:00:33,180
probably know about it but in case you

10
00:00:30,180 --> 00:00:36,960
don't know MSR registers or alternative

11
00:00:33,180 --> 00:00:39,300
to control registers and there are

12
00:00:36,960 --> 00:00:42,000
somehow an extension to the control

13
00:00:39,300 --> 00:00:46,320
registers and you can configure the

14
00:00:42,000 --> 00:00:48,480
processor for example changing the

15
00:00:46,320 --> 00:00:50,879
configuration for debugging program

16
00:00:48,480 --> 00:00:52,860
execution tracing or computer

17
00:00:50,879 --> 00:00:55,579
performance monitoring for example

18
00:00:52,860 --> 00:00:58,800
changing a hardware debug

19
00:00:55,579 --> 00:01:01,440
hardware performance counters and

20
00:00:58,800 --> 00:01:04,680
toggling some of the certain CPU

21
00:01:01,440 --> 00:01:07,260
features you can use MSR register for

22
00:01:04,680 --> 00:01:10,560
these purposes and there are two

23
00:01:07,260 --> 00:01:15,720
commands in HyperDbg same as WinDbg

24
00:01:10,560 --> 00:01:18,240
that that that are able to modify MSR

25
00:01:15,720 --> 00:01:20,360
registers for example if we want to read

26
00:01:18,240 --> 00:01:24,060
this specific

27
00:01:20,360 --> 00:01:26,479
MSR registers we could use rdmsr

28
00:01:24,060 --> 00:01:31,619
command or if we want to

29
00:01:26,479 --> 00:01:34,880
read or if we want to write some MSR

30
00:01:31,619 --> 00:01:41,759
registers we could use W 

31
00:01:34,880 --> 00:01:46,439
srwr MSR command now let's try to read

32
00:01:41,759 --> 00:01:49,020
the same MSR register here and Hyper

33
00:01:46,439 --> 00:01:52,259
reduces shows the MSR register for all

34
00:01:49,020 --> 00:01:54,420
of the cores as you can see this MSO

35
00:01:52,259 --> 00:01:57,899
register which is I think relates to

36
00:01:54,420 --> 00:02:01,560
system call handling Alistair or I'm not

37
00:01:57,899 --> 00:02:05,880
sure this 

38
00:02:01,560 --> 00:02:08,280
 MSR register in all the cores are the

39
00:02:05,880 --> 00:02:11,340
same so Windows set all of them to a

40
00:02:08,280 --> 00:02:15,660
special location to a special address

41
00:02:11,340 --> 00:02:17,840
here let's see

42
00:02:15,660 --> 00:02:21,239
this probably the

43
00:02:17,840 --> 00:02:25,560
car the handler for the system call

44
00:02:21,239 --> 00:02:29,720
let's try to just 

45
00:02:25,560 --> 00:02:29,720
set the Sim symbol path

46
00:02:49,860 --> 00:02:58,080
 let's try to run the same command

47
00:02:53,700 --> 00:03:00,720
again but this time we should also be

48
00:02:58,080 --> 00:03:01,920
able to see the name of the actual

49
00:03:00,720 --> 00:03:05,099
function yeah

50
00:03:01,920 --> 00:03:08,780
 it's for handling system calls and

51
00:03:05,099 --> 00:03:13,620
also it start with a swapgs

52
00:03:08,780 --> 00:03:16,860
instructions so it's for handling 

53
00:03:13,620 --> 00:03:21,420
system calls and here also you can

54
00:03:16,860 --> 00:03:24,540
change the value for 

55
00:03:21,420 --> 00:03:27,840
MSR registers also if I

56
00:03:24,540 --> 00:03:29,819
I want to show you you can also specify

57
00:03:27,840 --> 00:03:33,720
the core for example if you don't want

58
00:03:29,819 --> 00:03:36,780
to modify all of the cores then you can

59
00:03:33,720 --> 00:03:41,580
specify the core and HyperDbg only

60
00:03:36,780 --> 00:03:43,860
modifies this MSR to this address in

61
00:03:41,580 --> 00:03:45,780
this specific core and by default it

62
00:03:43,860 --> 00:03:48,540
applies to all of the course but if you

63
00:03:45,780 --> 00:03:50,879
specify a special code then it only

64
00:03:48,540 --> 00:03:52,560
applies it to this to that special

65
00:03:50,879 --> 00:03:57,739
target code

66
00:03:52,560 --> 00:04:02,879
it is also possible in hyperd widgets to

67
00:03:57,739 --> 00:04:07,140
monitor which MSRs are trying to be

68
00:04:02,879 --> 00:04:11,939
changed in the Windows or which MSRs 

69
00:04:07,140 --> 00:04:14,040
are like some some of the drivers or the

70
00:04:11,939 --> 00:04:17,100
operating system tries to read from some

71
00:04:14,040 --> 00:04:22,260
MSRs we can monitor them

72
00:04:17,100 --> 00:04:24,900
 we can also change we can for

73
00:04:22,260 --> 00:04:27,120
example if we want to disable one

74
00:04:24,900 --> 00:04:30,540
feature in the processor by disabling

75
00:04:27,120 --> 00:04:32,699
bits in the MSR register this is

76
00:04:30,540 --> 00:04:34,919
really useful for example if you want to

77
00:04:32,699 --> 00:04:37,620
ignore some of the changes to some

78
00:04:34,919 --> 00:04:40,259
special MSRs now you could use hyper

79
00:04:37,620 --> 00:04:44,699
divisions monitor command basically

80
00:04:40,259 --> 00:04:47,520
change the value of the MSR and 

81
00:04:44,699 --> 00:04:49,800
hybrid which will apply the changed

82
00:04:47,520 --> 00:04:52,560
version of the values for example you

83
00:04:49,800 --> 00:04:57,000
can mask some of the bits from the MSRs

84
00:04:52,560 --> 00:05:02,460
and when it applies the real heart where

85
00:04:57,000 --> 00:05:05,280
the bits are still masked and the VA

86
00:05:02,460 --> 00:05:08,759
that HyperDbg tries to intercept these

87
00:05:05,280 --> 00:05:11,520
msos are based on intercepting all

88
00:05:08,759 --> 00:05:13,680
changes on a specific MSR so if you want

89
00:05:11,520 --> 00:05:15,720
to just monitor all of them it's still

90
00:05:13,680 --> 00:05:18,900
possible but in case you only want to

91
00:05:15,720 --> 00:05:21,240
find sure it if you want to Mentor all

92
00:05:18,900 --> 00:05:23,160
of them then definitely it will slow

93
00:05:21,240 --> 00:05:26,759
down your computer but in case if you

94
00:05:23,160 --> 00:05:30,900
want to just monitor one specific 

95
00:05:26,759 --> 00:05:33,900
MSR register then it won't try to 

96
00:05:30,900 --> 00:05:37,039
won't slow down your system because it

97
00:05:33,900 --> 00:05:39,419
only intercept that special MSR

98
00:05:37,039 --> 00:05:41,940
because because of the sum of the

99
00:05:39,419 --> 00:05:46,860
features that that are used in HyperDbg

100
00:05:41,940 --> 00:05:49,440
or MSR bitmaps so let's see let's see 

101
00:05:46,860 --> 00:05:53,780
some of the MSRs but before before that

102
00:05:49,440 --> 00:05:58,139
let's see the WMS are command the the

103
00:05:53,780 --> 00:06:01,259
command that monitors the WR WR MSR

104
00:05:58,139 --> 00:06:04,380
instruction if there's any change to

105
00:06:01,259 --> 00:06:09,900
these MSR registers then we can monitor

106
00:06:04,380 --> 00:06:13,320
it by using MSR right or bang Ms or

107
00:06:09,900 --> 00:06:15,780
right command and also of course as I

108
00:06:13,320 --> 00:06:16,460
mentioned before we can change the value

109
00:06:15,780 --> 00:06:19,699
of

110
00:06:16,460 --> 00:06:23,759
eax which will eventually

111
00:06:19,699 --> 00:06:26,940
be returned to the MSR and we can

112
00:06:23,759 --> 00:06:30,660
perform some something we can completely

113
00:06:26,940 --> 00:06:34,259
ignore the MSR for example if we want to

114
00:06:30,660 --> 00:06:37,199
just change the change it and we want

115
00:06:34,259 --> 00:06:39,720
not to apply it we can use the event 

116
00:06:37,199 --> 00:06:44,000
approach circuiting for this purpose but

117
00:06:39,720 --> 00:06:44,000
let's see let's see some of the examples

118
00:06:44,360 --> 00:06:51,440


119
00:06:46,319 --> 00:06:51,440
I try to also add

120
00:06:53,100 --> 00:06:58,440
previously we used command for

121
00:06:57,120 --> 00:07:02,400
the

122
00:06:58,440 --> 00:07:05,000
exceptions let's also edit

123
00:07:02,400 --> 00:07:05,000
here

124
00:07:11,360 --> 00:07:18,319
 I try to see what are what MSR

125
00:07:16,080 --> 00:07:21,720
registers are

126
00:07:18,319 --> 00:07:24,259
that the MSR register that Windows tries

127
00:07:21,720 --> 00:07:29,160
to read them

128
00:07:24,259 --> 00:07:29,160
 so basically

129
00:07:29,460 --> 00:07:37,280
the MSR index is located at

130
00:07:33,900 --> 00:07:37,280
ecx register

131
00:07:39,300 --> 00:07:47,310
instruction pointer Trail race

132
00:07:44,250 --> 00:07:47,310
[pause]

133
00:07:53,639 --> 00:07:57,020
this Ms or

134
00:07:59,880 --> 00:08:09,139
we can also specify the ecx

135
00:08:03,680 --> 00:08:09,139
and let's run this MSR

136
00:08:19,860 --> 00:08:29,759
yeah we can see that Windows tries to

137
00:08:24,979 --> 00:08:31,379
read these Ms authors I posted but let's

138
00:08:29,759 --> 00:08:35,539
see what our

139
00:08:31,379 --> 00:08:35,539
these MSRs here

140
00:08:36,839 --> 00:08:43,640
I try to put them in a

141
00:08:40,860 --> 00:08:43,640
comment

142
00:08:49,820 --> 00:08:57,380
so basically we have this MSR

143
00:08:54,240 --> 00:08:57,380
another one

144
00:08:57,480 --> 00:09:06,240
and yeah

145
00:09:00,000 --> 00:09:07,740
so I I I downloaded a list of

146
00:09:06,240 --> 00:09:11,640
Ms owners

147
00:09:07,740 --> 00:09:16,200
and their descriptions from a repository

148
00:09:11,640 --> 00:09:19,820
you can try to 

149
00:09:16,200 --> 00:09:19,820
read them from here

150
00:09:24,720 --> 00:09:29,180
you can search them these are copied

151
00:09:27,420 --> 00:09:32,880
from this

152
00:09:29,180 --> 00:09:36,060
repository I try to just understand what

153
00:09:32,880 --> 00:09:38,060
what is the what is this MSR and as

154
00:09:36,060 --> 00:09:42,899
you can see this is

155
00:09:38,060 --> 00:09:47,540
 for this MSR i32fs space it tries to

156
00:09:42,899 --> 00:09:47,540
change the fs space FS register

157
00:09:48,000 --> 00:09:53,459


158
00:09:48,720 --> 00:09:58,200
so yeah this is the actual 

159
00:09:53,459 --> 00:10:00,060
MSR register that Windows tries to read

160
00:09:58,200 --> 00:10:03,420
from

161
00:10:00,060 --> 00:10:07,040
this location from this RP address

162
00:10:03,420 --> 00:10:09,779
so basically if we

163
00:10:07,040 --> 00:10:13,440
unassemble this address we will see that

164
00:10:09,779 --> 00:10:16,860
there is a rdmsr instruction here

165
00:10:13,440 --> 00:10:20,459
but you know let's try to clear them all

166
00:10:16,860 --> 00:10:23,940
and see what what are the msrg stairs

167
00:10:20,459 --> 00:10:28,620
that are written by Windows so I try to

168
00:10:23,940 --> 00:10:33,680
write it change it to MSR right and it

169
00:10:28,620 --> 00:10:33,680
tries to write here

170
00:10:34,560 --> 00:10:37,279
foreign

171
00:10:48,260 --> 00:10:53,399
here are the MSR registers that Windows

172
00:10:51,959 --> 00:10:56,660
try to

173
00:10:53,399 --> 00:10:56,660
write on them

174
00:11:03,959 --> 00:11:07,940
let's copy some of them here

175
00:11:08,070 --> 00:11:09,300
[pause]

176
00:11:08,579 --> 00:11:12,740


177
00:11:09,300 --> 00:11:12,740
to check the

178
00:11:14,180 --> 00:11:21,480
documentation again this is a fsbase MSR

179
00:11:19,980 --> 00:11:23,779
but if

180
00:11:21,480 --> 00:11:26,399
you look at

181
00:11:23,779 --> 00:11:28,200
this address

182
00:11:26,399 --> 00:11:32,640
then yeah

183
00:11:28,200 --> 00:11:36,260
this is I attached to x 2 AP this is for

184
00:11:32,640 --> 00:11:39,360
end of interrupt MSR so basically

185
00:11:36,260 --> 00:11:42,420
Windows tries to 

186
00:11:39,360 --> 00:11:45,660
access these MSR Regis therefore 

187
00:11:42,420 --> 00:11:51,680
notifying and end of intro

188
00:11:45,660 --> 00:11:51,680
in this special MSR address so yeah

189
00:11:51,740 --> 00:11:58,339
and and if we take a look at one of

190
00:11:54,959 --> 00:11:58,339
these addresses then

191
00:11:58,500 --> 00:12:04,800
which shows we have to see some WMS

192
00:12:02,459 --> 00:12:08,459
artists actually yeah here it is this is

193
00:12:04,800 --> 00:12:12,320
the WR MSR that is responsible for

194
00:12:08,459 --> 00:12:12,320
changing this MSR

