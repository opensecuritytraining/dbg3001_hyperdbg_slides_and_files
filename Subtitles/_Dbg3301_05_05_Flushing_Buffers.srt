1
00:00:00,000 --> 00:00:06,660
anything is washing buffers as you can

2
00:00:03,780 --> 00:00:09,360
see HyperDbg creates thousands of

3
00:00:06,660 --> 00:00:12,420
messages and some of them are not be

4
00:00:09,360 --> 00:00:15,859
handled or you we cannot handle them

5
00:00:12,420 --> 00:00:19,740
this thing happens whenever you want to

6
00:00:15,859 --> 00:00:21,600
debug and you use their mood whenever

7
00:00:19,740 --> 00:00:24,240
you want to debug in local debugging

8
00:00:21,600 --> 00:00:27,180
mode because all of the messages are

9
00:00:24,240 --> 00:00:30,119
immediately passed or delivered in the

10
00:00:27,180 --> 00:00:32,579
debug account the thing about the

11
00:00:30,119 --> 00:00:36,000
flashing buffer is that sometimes the

12
00:00:32,579 --> 00:00:38,520
rate of the produced messages is more

13
00:00:36,000 --> 00:00:40,860
than a high produces predefined buffer

14
00:00:38,520 --> 00:00:43,440
for example I pretty visually defines

15
00:00:40,860 --> 00:00:45,840
some buffers you can change it in the

16
00:00:43,440 --> 00:00:49,520
source code if you want but these

17
00:00:45,840 --> 00:00:52,140
buffers might generate thousands or even

18
00:00:49,520 --> 00:00:54,239
hundreds of thousands of these messages

19
00:00:52,140 --> 00:00:58,379
and all of them are stored in the

20
00:00:54,239 --> 00:01:02,039
buffers to be delivered and whenever a

21
00:00:58,379 --> 00:01:05,280
new buffer a new thing is added to the

22
00:01:02,039 --> 00:01:07,619
buffer then there is no empty place to

23
00:01:05,280 --> 00:01:10,619
place the buffer or save the buffer so

24
00:01:07,619 --> 00:01:14,100
the oldest messages or the previous

25
00:01:10,619 --> 00:01:16,380
messages start to be replaced by the new

26
00:01:14,100 --> 00:01:18,780
buffers but if you want to just flash

27
00:01:16,380 --> 00:01:21,720
everything and just remove everything

28
00:01:18,780 --> 00:01:24,299
from the buffers you can use the

29
00:01:21,720 --> 00:01:26,640
flashing buffer commands in debugger

30
00:01:24,299 --> 00:01:28,500
mode this is really important the

31
00:01:26,640 --> 00:01:31,680
concept that you should remember in high

32
00:01:28,500 --> 00:01:33,960
HyperDbg buffers in debugger mode are

33
00:01:31,680 --> 00:01:36,240
delivered immediately which means that

34
00:01:33,960 --> 00:01:38,100
high producive won't buffer them if you

35
00:01:36,240 --> 00:01:41,880
are using a kernel multivather for

36
00:01:38,100 --> 00:01:44,700
example you can guest VM machine then

37
00:01:41,880 --> 00:01:48,360
every messages are delivered immediately

38
00:01:44,700 --> 00:01:50,759
but in the case of VMI mode or in the

39
00:01:48,360 --> 00:01:53,700
case of local debugging mode the buffers

40
00:01:50,759 --> 00:01:56,280
will be buffered and then delivered

41
00:01:53,700 --> 00:01:58,979
because simply we cannot deliver all of

42
00:01:56,280 --> 00:02:02,220
them immediately it's not possible so

43
00:01:58,979 --> 00:02:05,759
after filling the buffers you might just

44
00:02:02,220 --> 00:02:08,220
clear an event and after clearing events

45
00:02:05,759 --> 00:02:12,120
you just HyperDbg keeps delivering

46
00:02:08,220 --> 00:02:15,120
buffers onto all of them are received so

47
00:02:12,120 --> 00:02:17,640
if we are not interested in those

48
00:02:15,120 --> 00:02:21,060
buffers we will use the flash command to

49
00:02:17,640 --> 00:02:25,440
remove all of them I will give you an

50
00:02:21,060 --> 00:02:28,800
example of what I mean by this here is

51
00:02:25,440 --> 00:02:30,840
an example I just tried to revert to the

52
00:02:28,800 --> 00:02:33,720
previous estate because I want to show

53
00:02:30,840 --> 00:02:36,840
you something in the VMI mode of the

54
00:02:33,720 --> 00:02:40,200
HyperDbg and we can we can use the

55
00:02:36,840 --> 00:02:43,560
example that we previously use for the

56
00:02:40,200 --> 00:02:47,459
APK hook here it was just create

57
00:02:43,560 --> 00:02:52,620
books from x-allocate will be tag so

58
00:02:47,459 --> 00:02:55,459
let's just start the HyperDbg in VMI

59
00:02:52,620 --> 00:02:59,280
mode and say connect

60
00:02:55,459 --> 00:03:01,739
to the local machine and I will load the

61
00:02:59,280 --> 00:03:04,260
VMM I hope that

62
00:03:01,739 --> 00:03:06,739
screen is connected because we want to

63
00:03:04,260 --> 00:03:09,959
bypass the driver signature enforcement

64
00:03:06,739 --> 00:03:13,560
it's not connected yet yeah it's

65
00:03:09,959 --> 00:03:16,379
connected you know so yeah it's paid but

66
00:03:13,560 --> 00:03:20,879
if you want to if we run it again then

67
00:03:16,379 --> 00:03:26,519
we will see that it's connected and no I

68
00:03:20,879 --> 00:03:29,700
run this command from the slides as you

69
00:03:26,519 --> 00:03:32,159
know X allocate will be Tech has our

70
00:03:29,700 --> 00:03:35,640
high rate of execution

71
00:03:32,159 --> 00:03:38,060
so thousands of events are running each

72
00:03:35,640 --> 00:03:41,459
time so and and buffer are completely

73
00:03:38,060 --> 00:03:45,060
filled with these messages the thing is

74
00:03:41,459 --> 00:03:47,819
that it says mode is enabled and the

75
00:03:45,060 --> 00:03:51,060
debugger will automatically run in a new

76
00:03:47,819 --> 00:03:53,940
event command if you just click or if

77
00:03:51,060 --> 00:03:55,739
you just put a new event to the divided

78
00:03:53,940 --> 00:03:58,680
so I want to continue then I continue

79
00:03:55,739 --> 00:04:01,019
seeing these events and the thing that

80
00:03:58,680 --> 00:04:03,959
you should know is that some of the

81
00:04:01,019 --> 00:04:06,120
events might be ignored if you just try

82
00:04:03,959 --> 00:04:08,760
generally you are not it's not a good

83
00:04:06,120 --> 00:04:12,420
idea to just fill all the buffers of the

84
00:04:08,760 --> 00:04:15,480
HyperDbg but hyper-digit keeps removing

85
00:04:12,420 --> 00:04:18,900
the old buffers with the new buffers and

86
00:04:15,480 --> 00:04:21,120
in this some of the events might be

87
00:04:18,900 --> 00:04:23,400
ignored or something the event might be

88
00:04:21,120 --> 00:04:26,000
removed so you should put some checks

89
00:04:23,400 --> 00:04:28,740
like some if statements or else

90
00:04:26,000 --> 00:04:32,580
statements conditional statements to

91
00:04:28,740 --> 00:04:34,979
avoid creating thousands of messages is

92
00:04:32,580 --> 00:04:35,720
simultaneously it's not really coming I

93
00:04:34,979 --> 00:04:39,000
just

94
00:04:35,720 --> 00:04:41,280
clearing the event or disabling first

95
00:04:39,000 --> 00:04:44,180
I just want to see the email they when I

96
00:04:41,280 --> 00:04:47,220
use zero and I will

97
00:04:44,180 --> 00:04:49,560
disable this event so it's not supposed

98
00:04:47,220 --> 00:04:51,060
to show anything now but it keeps

99
00:04:49,560 --> 00:04:53,160
showing because the buffers aren't

100
00:04:51,060 --> 00:04:55,800
there's something buffers that are not

101
00:04:53,160 --> 00:04:58,820
limited I will use the flash command

102
00:04:55,800 --> 00:05:03,540
here which said that in total we remove

103
00:04:58,820 --> 00:05:06,120
600 72 messages and all of them are

104
00:05:03,540 --> 00:05:08,660
cleared now if I run the ga gain there

105
00:05:06,120 --> 00:05:12,060
is nothing and no messages are currently

106
00:05:08,660 --> 00:05:13,680
available in the debugger if I re-enable

107
00:05:12,060 --> 00:05:15,840
it again

108
00:05:13,680 --> 00:05:18,300
you can see that the messages are keep

109
00:05:15,840 --> 00:05:21,000
coming the hypers will just start

110
00:05:18,300 --> 00:05:24,500
producing new messages so I will just

111
00:05:21,000 --> 00:05:27,960
remove I will just disable it again

112
00:05:24,500 --> 00:05:30,500
this time watch the buffers and this

113
00:05:27,960 --> 00:05:35,660
time you can see that

114
00:05:30,500 --> 00:05:35,660
896 message were very clear

