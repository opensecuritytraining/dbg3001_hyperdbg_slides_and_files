1
00:00:00,000 --> 00:00:06,600
other tricks that I can also talk about

2
00:00:03,780 --> 00:00:09,840
is how you can customize different

3
00:00:06,600 --> 00:00:13,500
builds of HyperDbg

4
00:00:09,840 --> 00:00:16,440
HyperDbg has some constant values that

5
00:00:13,500 --> 00:00:19,500
that are like we see that based on

6
00:00:16,440 --> 00:00:22,680
regular needs these constants should be

7
00:00:19,500 --> 00:00:26,400
set to specify to a specific values

8
00:00:22,680 --> 00:00:28,980
for example the size of buffers for the

9
00:00:26,400 --> 00:00:32,160
message tracing is a constant number so

10
00:00:28,980 --> 00:00:34,500
you put for example increase the this

11
00:00:32,160 --> 00:00:36,719
value or you can also decrease this

12
00:00:34,500 --> 00:00:38,760
value based on your needs

13
00:00:36,719 --> 00:00:41,640
because there there might be some

14
00:00:38,760 --> 00:00:45,180
limitations for example we might need a

15
00:00:41,640 --> 00:00:48,379
special need for a special command and

16
00:00:45,180 --> 00:00:52,920
these limitations will be a barrier for

17
00:00:48,379 --> 00:00:56,039
or debugging so we try to just eliminate

18
00:00:52,920 --> 00:00:58,860
that by reconfiguring HyperDbg and

19
00:00:56,039 --> 00:01:00,680
rebuilding hyper DVD for this particular

20
00:00:58,860 --> 00:01:04,799
purposes

21
00:01:00,680 --> 00:01:08,280
in all of these examples we could use

22
00:01:04,799 --> 00:01:10,920
the following links I tried not to

23
00:01:08,280 --> 00:01:13,500
recompile it because it might the

24
00:01:10,920 --> 00:01:16,380
different customized build might 

25
00:01:13,500 --> 00:01:18,659
change in the future a variant but

26
00:01:16,380 --> 00:01:22,140
generally if we want to see the

27
00:01:18,659 --> 00:01:25,140
scenarios in which currently we are able

28
00:01:22,140 --> 00:01:27,540
to modify the source code and then

29
00:01:25,140 --> 00:01:31,020
recompile HyperDbg for example it

30
00:01:27,540 --> 00:01:34,439
allows you to just see the time and

31
00:01:31,020 --> 00:01:37,700
debug messages if you want to use wpp

32
00:01:34,439 --> 00:01:40,740
tracing instead of regular HyperDbg box

33
00:01:37,700 --> 00:01:43,920
and there are also other options that

34
00:01:40,740 --> 00:01:45,840
you can see and change the debugger

35
00:01:43,920 --> 00:01:49,200
based on your needs for example the

36
00:01:45,840 --> 00:01:52,619
maximum capacity is also here the chunk

37
00:01:49,200 --> 00:01:55,259
size of each packet is also here and

38
00:01:52,619 --> 00:01:58,979
also there are all the other options

39
00:01:55,259 --> 00:02:01,560
like before port for TCP connection

40
00:01:58,979 --> 00:02:05,240
you could read any this documentation

41
00:02:01,560 --> 00:02:05,240
might be changed over time

