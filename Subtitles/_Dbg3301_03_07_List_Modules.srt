1
00:00:02,399 --> 00:00:10,920
and I think that is whenever you use

2
00:00:06,120 --> 00:00:14,639
Windows a portion of the dlls or

3
00:00:10,920 --> 00:00:17,820
modules binary modules are loaded in the

4
00:00:14,639 --> 00:00:21,000
kernel mode and some of them are loaded

5
00:00:17,820 --> 00:00:24,539
in the user mode for each process we

6
00:00:21,000 --> 00:00:28,140
will see each of them and I will see how

7
00:00:24,539 --> 00:00:30,539
how HyperDbg can show both the camera

8
00:00:28,140 --> 00:00:33,780
mode and user mode modules along with

9
00:00:30,539 --> 00:00:36,840
their addresses their stats a start size

10
00:00:33,780 --> 00:00:40,079
and has those functions there's also

11
00:00:36,840 --> 00:00:43,160
another command which is called LM

12
00:00:40,079 --> 00:00:48,960
this command will 

13
00:00:43,160 --> 00:00:51,840
at least every single modules that are

14
00:00:48,960 --> 00:00:53,879
loaded in a Bose user mode and the

15
00:00:51,840 --> 00:00:56,879
kernel mode in the user mode part you

16
00:00:53,879 --> 00:00:59,239
can see that the following files are

17
00:00:56,879 --> 00:01:02,699
currently loaded as we are currently

18
00:00:59,239 --> 00:01:05,880
debugging the hype in we are currency in

19
00:01:02,699 --> 00:01:07,760
the HyperDbg process so we'll see

20
00:01:05,880 --> 00:01:10,799
this measurements than hybrid imagery

21
00:01:07,760 --> 00:01:13,320
these modules are called and their entry

22
00:01:10,799 --> 00:01:15,960
points are here they are located here

23
00:01:13,320 --> 00:01:18,000
and their start addresses here the same

24
00:01:15,960 --> 00:01:19,979
is also true for the Clarion mode you

25
00:01:18,000 --> 00:01:22,680
can see that we have the start address

26
00:01:19,979 --> 00:01:26,220
of this modules the size of these

27
00:01:22,680 --> 00:01:30,240
modules and the name of them and also

28
00:01:26,220 --> 00:01:32,700
the password these applications

29
00:01:30,240 --> 00:01:37,920
 there are also other parameters for

30
00:01:32,700 --> 00:01:40,439
this command like if I use let

31
00:01:37,920 --> 00:01:43,920
me just clear everything and let if I

32
00:01:40,439 --> 00:01:45,140
want to use help LM

33
00:01:43,920 --> 00:01:48,420


34
00:01:45,140 --> 00:01:49,979
lmkm only shows the kernel mode modules

35
00:01:48,420 --> 00:01:52,079
LM

36
00:01:49,979 --> 00:01:55,439
 only shows the user mode measures we

37
00:01:52,079 --> 00:01:59,220
can also try to filter those modules

38
00:01:55,439 --> 00:02:01,860
like for example let me show you the

39
00:01:59,220 --> 00:02:04,979
LM which shows that only the user mode

40
00:02:01,860 --> 00:02:09,259
or if I want to search everything I can

41
00:02:04,979 --> 00:02:12,840
use M and I want to search for the theme

42
00:02:09,259 --> 00:02:16,140
entry in our modules in both kernel mode

43
00:02:12,840 --> 00:02:20,160
and user mode as you can see that these

44
00:02:16,140 --> 00:02:24,060
modules can contain NT on their

45
00:02:20,160 --> 00:02:29,160
for example under name for example NT or

46
00:02:24,060 --> 00:02:32,160
their past ntos kernel has a NT on it or

47
00:02:29,160 --> 00:02:34,099
for example 

48
00:02:32,160 --> 00:02:41,400
I don't know maybe

49
00:02:34,099 --> 00:02:44,700
NTFS or Mount ER or Intel these 

50
00:02:41,400 --> 00:02:49,400
modules have NT or if I want to

51
00:02:44,700 --> 00:02:52,680
specifically specify the ntos then

52
00:02:49,400 --> 00:02:57,540
something like this will help which says

53
00:02:52,680 --> 00:03:02,959
that we have ntos EXT or and ntos card

54
00:02:57,540 --> 00:03:02,959
on car R and L in of system

