1
00:00:00,000 --> 00:00:05,520
can you imagine how powerful it would be

2
00:00:02,460 --> 00:00:07,919
if you want to share the state of other

3
00:00:05,520 --> 00:00:11,160
processors or the state of debugging

4
00:00:07,919 --> 00:00:14,400
between different cores now let's see

5
00:00:11,160 --> 00:00:17,039
how a script engine functions work and

6
00:00:14,400 --> 00:00:19,920
there are different functions in the

7
00:00:17,039 --> 00:00:21,600
script engine of the HyperDbg there are

8
00:00:19,920 --> 00:00:25,199
different the hybridges consists of

9
00:00:21,600 --> 00:00:27,960
different functions and in each in each

10
00:00:25,199 --> 00:00:30,539
version your functions are added to the

11
00:00:27,960 --> 00:00:32,099
hybrid using storage private clear that

12
00:00:30,539 --> 00:00:34,440
you should check the documentation

13
00:00:32,099 --> 00:00:37,079
because at the time that I'm

14
00:00:34,440 --> 00:00:40,800
recording this video then we have the

15
00:00:37,079 --> 00:00:42,540
current functions but other new

16
00:00:40,800 --> 00:00:45,239
functions might be added in the future

17
00:00:42,540 --> 00:00:47,700
actually will be added in the future so

18
00:00:45,239 --> 00:00:51,360
here here's the link that you should go

19
00:00:47,700 --> 00:00:54,000
and investigate you should see that how

20
00:00:51,360 --> 00:00:57,600
they implement it here if I want to just

21
00:00:54,000 --> 00:01:00,480
 make a separation between each

22
00:00:57,600 --> 00:01:04,440
function or just convert them to to

23
00:01:00,480 --> 00:01:06,479
different parts I will create these

24
00:01:04,440 --> 00:01:08,580
diagrams that shows that we have

25
00:01:06,479 --> 00:01:10,500
functions that are relating to the high

26
00:01:08,580 --> 00:01:12,479
housing the debugger or housing

27
00:01:10,500 --> 00:01:15,000
functions we have also some event

28
00:01:12,479 --> 00:01:17,100
functions that try to manage the events

29
00:01:15,000 --> 00:01:19,500
within the script Engine add other

30
00:01:17,100 --> 00:01:22,500
interlocked functions on memory

31
00:01:19,500 --> 00:01:25,439
functions now let's see a halting

32
00:01:22,500 --> 00:01:27,659
function here which is called pause this

33
00:01:25,439 --> 00:01:30,900
is a really cool feature of the hyper

34
00:01:27,659 --> 00:01:33,119
dvg it's just a function it's just a

35
00:01:30,900 --> 00:01:36,360
simple function that tries to halt

36
00:01:33,119 --> 00:01:39,180
everything of the kernel or the in the

37
00:01:36,360 --> 00:01:41,939
debugger mode just halt everything and

38
00:01:39,180 --> 00:01:44,840
it's it's the this is a command that's

39
00:01:41,939 --> 00:01:47,280
not supposed to be used with the

40
00:01:44,840 --> 00:01:48,900
question mark command it's not an

41
00:01:47,280 --> 00:01:53,340
evaluating command for example imagine

42
00:01:48,900 --> 00:01:56,579
that if you want to just halt the system

43
00:01:53,340 --> 00:01:59,399
or get the control after debugging

44
00:01:56,579 --> 00:02:02,100
whenever something happens in your

45
00:01:59,399 --> 00:02:05,759
system for example let's say a syscall we

46
00:02:02,100 --> 00:02:08,520
all we will create a hook for all the

47
00:02:05,759 --> 00:02:11,239
processes or a specific process then

48
00:02:08,520 --> 00:02:15,360
each system call will trigger an event

49
00:02:11,239 --> 00:02:18,360
in the event we will we will check for

50
00:02:15,360 --> 00:02:21,060
 different parameters that are housing

51
00:02:18,360 --> 00:02:23,700
the events and we'll check whether those

52
00:02:21,060 --> 00:02:25,980
parameters are from our interest or not

53
00:02:23,700 --> 00:02:29,040
for example let's say that we will we

54
00:02:25,980 --> 00:02:32,160
want to check and we will use this 

55
00:02:29,040 --> 00:02:35,160
simple s script in a syscall commands and

56
00:02:32,160 --> 00:02:37,980
for a special process and whenever the

57
00:02:35,160 --> 00:02:41,760
event is triggered will check whether

58
00:02:37,980 --> 00:02:45,540
the Rex or the system call number which

59
00:02:41,760 --> 00:02:52,260
is available in the RX is equal to 0x55

60
00:02:45,540 --> 00:02:55,260
or 55 and rdx is equal to 0 x 66. so if

61
00:02:52,260 --> 00:02:58,019
this condition is true then we will get

62
00:02:55,260 --> 00:02:59,640
the control and say yeah this is this is

63
00:02:58,019 --> 00:03:02,879
the parameters that I want to develop

64
00:02:59,640 --> 00:03:06,060
and this is based on my interest this

65
00:03:02,879 --> 00:03:09,120
way we can just ignore everything and

66
00:03:06,060 --> 00:03:12,480
continue our normal debugging scenario

67
00:03:09,120 --> 00:03:15,840
why if the condition for or debugging

68
00:03:12,480 --> 00:03:18,060
which is is triggered or the thing

69
00:03:15,840 --> 00:03:20,340
that we want is triggered in the target

70
00:03:18,060 --> 00:03:22,500
debuggee then we can pause and get the

71
00:03:20,340 --> 00:03:25,140
control of the debugger there are also

72
00:03:22,500 --> 00:03:29,340
other events like event enable and event

73
00:03:25,140 --> 00:03:33,420
disabled these functions simply get the

74
00:03:29,340 --> 00:03:36,300
event ID as their parameters and you

75
00:03:33,420 --> 00:03:40,500
can just see the different event ideas

76
00:03:36,300 --> 00:03:43,799
but using the events command and this

77
00:03:40,500 --> 00:03:46,739
way you can enable an event or disable

78
00:03:43,799 --> 00:03:49,260
an event logically in the hybrid region

79
00:03:46,739 --> 00:03:52,140
and we will discuss we'll discuss about

80
00:03:49,260 --> 00:03:54,900
these events more truly truly on the

81
00:03:52,140 --> 00:03:57,060
next sections and see here's an example

82
00:03:54,900 --> 00:03:59,760
there's a simple example that how you

83
00:03:57,060 --> 00:04:02,700
can use this number also you can pass a

84
00:03:59,760 --> 00:04:05,760
Studio register or you can pass the

85
00:04:02,700 --> 00:04:08,220
result of a function or variable as the

86
00:04:05,760 --> 00:04:10,019
parameter to the events enable and yeah

87
00:04:08,220 --> 00:04:12,299
even disabled form there are also

88
00:04:10,019 --> 00:04:14,040
interlocked functions I'm not going to

89
00:04:12,299 --> 00:04:16,079
talk about the interlock function as

90
00:04:14,040 --> 00:04:18,000
it's the operating system function you

91
00:04:16,079 --> 00:04:20,040
can pretty if you don't have any idea

92
00:04:18,000 --> 00:04:23,040
about interlock function you can search

93
00:04:20,040 --> 00:04:26,100
the Google is pretty simple but hybrid

94
00:04:23,040 --> 00:04:28,320
images supports different interlock

95
00:04:26,100 --> 00:04:30,360
functions for like for example like

96
00:04:28,320 --> 00:04:33,600
interlock the Exchange interlock

97
00:04:30,360 --> 00:04:36,060
exchange ad and other functions this 

98
00:04:33,600 --> 00:04:38,340
if just want to say have a simple

99
00:04:36,060 --> 00:04:41,880
explanation these functions guarantee

100
00:04:38,340 --> 00:04:44,580
that only one core applied change or

101
00:04:41,880 --> 00:04:46,560
decrement or change a special variable

102
00:04:44,580 --> 00:04:49,919
for example this is well I don't know

103
00:04:46,560 --> 00:04:53,520
yeah we've ever seen the like button in

104
00:04:49,919 --> 00:04:56,940
Twitter for example if all of the users

105
00:04:53,520 --> 00:05:01,020
around the world just try to put their

106
00:04:56,940 --> 00:05:03,900
like button simultaneously it then and

107
00:05:01,020 --> 00:05:08,520
you definitely end up in a result that

108
00:05:03,900 --> 00:05:12,419
this user tries to increment the value

109
00:05:08,520 --> 00:05:16,020
of the count of likes and other user

110
00:05:12,419 --> 00:05:19,160
just tries to add something to it so it

111
00:05:16,020 --> 00:05:21,360
just you cannot do everything

112
00:05:19,160 --> 00:05:23,820
simultaneously and that's why you need

113
00:05:21,360 --> 00:05:25,500
to interlock functions if you use a

114
00:05:23,820 --> 00:05:28,199
simple interlock function then it's

115
00:05:25,500 --> 00:05:31,620
guaranteed that each like is computed

116
00:05:28,199 --> 00:05:34,740
and each leg is added to the target

117
00:05:31,620 --> 00:05:37,020
value so this that's why interlock

118
00:05:34,740 --> 00:05:38,639
functions are here in HyperDbg for

119
00:05:37,020 --> 00:05:41,100
example if you want to make some

120
00:05:38,639 --> 00:05:43,500
counters out of the script engine then

121
00:05:41,100 --> 00:05:46,220
you can use the interlock increment

122
00:05:43,500 --> 00:05:49,740
function there are also other

123
00:05:46,220 --> 00:05:52,620
functions for modifying the memory from

124
00:05:49,740 --> 00:05:56,039
within the script engine or checking

125
00:05:52,620 --> 00:05:58,320
whether an address is valid or not these

126
00:05:56,039 --> 00:06:01,680
are pretty useful functions for example

127
00:05:58,320 --> 00:06:05,820
where we want to use if we want to

128
00:06:01,680 --> 00:06:09,419
change a value which is pointed by r11

129
00:06:05,820 --> 00:06:14,220
register to this value then we can use

130
00:06:09,419 --> 00:06:17,460
EQ function which gets 64-bit value and

131
00:06:14,220 --> 00:06:19,680
we will and it will put it to the value

132
00:06:17,460 --> 00:06:22,259
that are pointed by R in the event which

133
00:06:19,680 --> 00:06:24,840
is there so and if it was okay then the

134
00:06:22,259 --> 00:06:28,680
changes were applied and check it with a

135
00:06:24,840 --> 00:06:31,139
simple if a statement if the and if

136
00:06:28,680 --> 00:06:34,560
the address was not available or

137
00:06:31,139 --> 00:06:37,560
address was wrong then we will see that

138
00:06:34,560 --> 00:06:40,680
changes are not applied okay before just

139
00:06:37,560 --> 00:06:44,160
like going to the summary let me give

140
00:06:40,680 --> 00:06:48,740
you some examples of how you can use the

141
00:06:44,160 --> 00:06:48,740
escape engine of HyperDbg

