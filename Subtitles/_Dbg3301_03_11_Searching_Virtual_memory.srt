1
00:00:00,000 --> 00:00:05,759
we have another section called searching

2
00:00:02,460 --> 00:00:08,160
virtual memory several formats for

3
00:00:05,759 --> 00:00:12,599
searching the virtual memory we could

4
00:00:08,160 --> 00:00:16,020
use SP SB and SQ commands the first

5
00:00:12,599 --> 00:00:18,480
command search searches in byte format

6
00:00:16,020 --> 00:00:21,900
the second command searches in keyword

7
00:00:18,480 --> 00:00:24,840
format or two bit format the third one

8
00:00:21,900 --> 00:00:29,640
searches for keyword search perform

9
00:00:24,840 --> 00:00:32,820
64-bit search so we can use these

10
00:00:29,640 --> 00:00:36,660
commands in both VMI mode and debugger

11
00:00:32,820 --> 00:00:40,020
mode for example this is an example

12
00:00:36,660 --> 00:00:44,579
after we want to search for some byte

13
00:00:40,020 --> 00:00:46,040
patterns so I try to start from this

14
00:00:44,579 --> 00:00:49,800
virtual address

15
00:00:46,040 --> 00:00:53,219
and use SP command to search it in byte

16
00:00:49,800 --> 00:00:55,100
format then I specify the range of

17
00:00:53,219 --> 00:00:58,039
search or

18
00:00:55,100 --> 00:01:01,260
specify the number of

19
00:00:58,039 --> 00:01:03,539
bytes that actually will be searched

20
00:01:01,260 --> 00:01:05,820
this is the number of bytes that will be

21
00:01:03,539 --> 00:01:08,460
searched and after that I try to specify

22
00:01:05,820 --> 00:01:12,180
each byte of the pattern so high

23
00:01:08,460 --> 00:01:15,119
producer tries to find this address to

24
00:01:12,180 --> 00:01:19,100
this address plus this amount of bytes

25
00:01:15,119 --> 00:01:22,680
and searches for this pattern of 41

26
00:01:19,100 --> 00:01:25,619
5641 I did this pattern in hexadecimal

27
00:01:22,680 --> 00:01:28,680
format again this is another example

28
00:01:25,619 --> 00:01:29,880
that you will try to search in keyword

29
00:01:28,680 --> 00:01:33,320
format

30
00:01:29,880 --> 00:01:37,320
we specify the address we specified the

31
00:01:33,320 --> 00:01:39,840
size and after that we will skip the

32
00:01:37,320 --> 00:01:43,979
debugger or value to be searched so

33
00:01:39,840 --> 00:01:46,280
these values should be continuous be

34
00:01:43,979 --> 00:01:49,619
after each other for example first

35
00:01:46,280 --> 00:01:51,659
whenever first this value is on the

36
00:01:49,619 --> 00:01:55,140
memory and after that this value is

37
00:01:51,659 --> 00:01:57,540
again under memory hybrid resources

38
00:01:55,140 --> 00:02:01,560
like this you should just separate them

39
00:01:57,540 --> 00:02:05,060
by by a space delimiter we will see some

40
00:02:01,560 --> 00:02:05,060
examples don't worry about it

