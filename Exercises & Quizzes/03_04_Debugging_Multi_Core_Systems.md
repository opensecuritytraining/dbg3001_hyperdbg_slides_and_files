## Debugging Multi-core Systems

### Exercise List:

1. **Switch to different cores**
   Switch to different cores using the '~' command, and examine the instructions that are currently executing in other cores (e.g., whether the target core is in the user-mode or the kernel-mode).

### Links:

* [~ (display and change the current operating core)](https://docs.hyperdbg.org/commands/debugging-commands/core)