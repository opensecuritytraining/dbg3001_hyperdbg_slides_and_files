## Batch Scripts

### Exercise List:

1. **Write a DS script**
   Write a DS file script that connects to the VMI mode, intercepts system-calls using the '!syscall' command. Saves the output of the debugger using the '.logopen' command, and then close it by using the '.logclose' command. After that clear the event. 

2. **Run the DS file 1**
   Run the above DS file using the '.script' command.

3. **Run the DS file 2**
   Run the above DS file, and pass the '--script' argument to the 'hyperdbg-cli.exe'.

### Links:

* [.logopen (open log file)](https://docs.hyperdbg.org/commands/meta-commands/.logopen)

* [.logclose (close log file)](https://docs.hyperdbg.org/commands/meta-commands/.logclose)

* [.script (run batch script commands)](https://docs.hyperdbg.org/commands/meta-commands/.script)

* [Debugger Script (DS)](https://docs.hyperdbg.org/commands/scripting-language/debugger-script)

* [!syscall, !syscall2 (hook system-calls)](https://docs.hyperdbg.org/commands/extension-commands/syscall)

* [.logclose (close log file)](https://docs.hyperdbg.org/commands/meta-commands/.logclose)