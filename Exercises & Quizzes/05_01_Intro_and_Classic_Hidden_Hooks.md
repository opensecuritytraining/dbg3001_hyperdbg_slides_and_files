## Intro & Classic Hidden Hooks

### Exercise List:

1. **Use the '!epthook' as hidden breakpoint**
   As EPT hooks are hidden, use the the '!epthook' like a breakpoint on the 'nt!NtCreateFile'.

2. **Print arguments**
   Print the arguments to the 'nt!NtCreateFile' by using the 'printf' function in the script engine.

### Links:

* [!epthook (hidden hook with EPT - stealth breakpoints)](https://docs.hyperdbg.org/commands/extension-commands/epthook)

* [printf](https://docs.hyperdbg.org/commands/scripting-language/functions/exports/printf)