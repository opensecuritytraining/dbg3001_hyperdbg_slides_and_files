## HyperDbg Lab Setup

### Exercise List:

1. **Attach KDNET**
   Attach KDNET and WinDbg to Windows and bypass DSE. (As shown in the video)

2. **Use EfiGuard**
   Use the EfiGuard to attach HyperDbg. (As shown in the video)

### Links:

* [Build & Install](https://docs.hyperdbg.org/getting-started/build-and-install)

* [Attach to a remote machine](https://docs.hyperdbg.org/getting-started/attach-to-hyperdbg/debug)

* [Attach to local machine](https://docs.hyperdbg.org/getting-started/attach-to-hyperdbg/local-debugging)