## Debugging WinDbg with HyperDbg

### Exercise List:

1. **Change the MSR results**
   Change the result of MSR\_LSTAR (0x80000082) using the '!msrread' command.
   
2. **Ignore breakpoints**
   Ignore breakpoints using the '!exception' command.
   
3. **Sniff the WinDbg outputs**
   Sniff the traffic of WinDbg using the '!epthook' command.
   
4. **View module function**
   Examine different functions in WinDbg.

5. **Track connection**
   Track the connection (the way WinDbg sends data) using the '!track' command.

6. **Find and modify the process name**
   Find and modify the location where WinDbg reads the process name and modify it and again check the results in WinDbg.

### Links:

* [Files](https://url.hyperdbg.org/ost2/part-11-files)

* [!epthook (hidden hook with EPT - stealth breakpoints)](https://docs.hyperdbg.org/commands/extension-commands/epthook)

* [!monitor (monitor read/write/execute to a range of memory)](https://docs.hyperdbg.org/commands/extension-commands/monitor)

* [!msrread (hook RDMSR instruction execution)](https://docs.hyperdbg.org/commands/extension-commands/msrread)

* [!exception (hook first 32 entries of IDT)](https://docs.hyperdbg.org/commands/extension-commands/exception)

* [!track (track and map function calls and returns to the symbols)](https://docs.hyperdbg.org/commands/extension-commands/track)

* [db, dc, dd, dq (read virtual memory)](https://docs.hyperdbg.org/commands/debugging-commands/d)

* [test (test functionalities)](https://docs.hyperdbg.org/commands/debugging-commands/test)