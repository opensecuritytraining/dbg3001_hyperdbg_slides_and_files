## Tracking Function Calls

### Exercise List:

1. **Set a breakpoint**
   Set a breakpoint on the 'nt!ExAllocatePoolWithTag'. Then, run the 'g' command to continue the debuggee. Once triggered, remove the breakpoint using the 'bc' command.

2. **Track functions**
   Use the '!track' command to see the list of functions that are called in the system.

### Links:

* [!track (track and map function calls and returns to the symbols)](https://docs.hyperdbg.org/commands/extension-commands/track)

* [bp (set breakpoint)](https://docs.hyperdbg.org/commands/debugging-commands/bp)

* [g (continue debuggee or processing kernel packets)](https://docs.hyperdbg.org/commands/debugging-commands/g)

* [bc (clear and remove breakpoints)](https://docs.hyperdbg.org/commands/debugging-commands/bc)