## Intro, Scripting, and Preliminaries

### Exercise List:

1. **Use keywords**
   Use the 'print' command to view different registers, and combine them with keywords.

2. **View pseudo-registers**
   Get the current process id, by using the '$pid' pseudo-register (using the 'print' command).

### Links:

* [print (evaluate and print expression in debuggee)](https://docs.hyperdbg.org/commands/debugging-commands/print)

* [Assumptions & Evaluations](https://docs.hyperdbg.org/commands/scripting-language/assumptions-and-evaluations)