## Kernel Debugging

### Exercise List:

1. **Connect to the kernel debugger**
   Connect to the kernel debugger using the '.debug' command.

2. **Load symbols**
   Load the symbols based on kHyperDbg.

### Links:

* [.debug (prepare and connect to debugger)](https://docs.hyperdbg.org/commands/meta-commands/.debug)

* [.sympath (set the symbol server)](https://docs.hyperdbg.org/commands/meta-commands/.sympath)

* [.sym (load pdb symbols)](https://docs.hyperdbg.org/commands/meta-commands/.sym)